package com.captain.crew.admin.api.entity.domain;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 定时任务执行日志表
 *
 * @author PC
 */
@Data
@TableName(value = "sys_job_log")
public class SysJobLog implements Serializable {
    /**
     * 任务日志ID
     */
    @TableId(value = "job_log_id", type = IdType.AUTO)
    private Integer jobLogId;

    /**
     * 任务id
     */
    @TableField(value = "job_id")
    private Integer jobId;

    /**
     * 任务名称
     */
    @TableField(value = "job_name")
    private String jobName;

    /**
     * 任务组名
     */
    @TableField(value = "job_group")
    private String jobGroup;

    /**
     * 组内执行顺利，值越大执行优先级越高，最大值9，最小值1
     */
    @TableField(value = "job_order")
    private Integer jobOrder;

    /**
     * 1、java类;2、spring bean名称;3、rest调用;4、jar调用;9其他
     */
    @TableField(value = "job_type")
    private Integer jobType;

    /**
     * job_type=3时，rest调用地址，仅支持post协议;job_type=4时，jar路径;其它值为空
     */
    @TableField(value = "execute_path")
    private String executePath;

    /**
     * job_type=1时，类完整路径;job_type=2时，spring bean名称;其它值为空
     */
    @TableField(value = "class_name")
    private String className;

    /**
     * 任务方法
     */
    @TableField(value = "method_name")
    private String methodName;

    /**
     * 参数值
     */
    @TableField(value = "method_params_value")
    private String methodParamsValue;

    /**
     * cron执行表达式
     */
    @TableField(value = "cron_expression")
    private String cronExpression;

    /**
     * 日志信息
     */
    @TableField(value = "job_message")
    private String jobMessage;

    /**
     * 执行状态（0正常 1失败）
     */
    @TableField(value = "job_log_status")
    private Integer jobLogStatus;

    /**
     * 执行时间
     */
    @TableField(value = "execute_time")
    private String executeTime;

    /**
     * 异常信息
     */
    @TableField(value = "exception_info")
    private String exceptionInfo;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private LocalDateTime createTime;

    /**
     * 租户id
     */
    @TableField(value = "tenant_id")
    private Integer tenantId;

    private static final long serialVersionUID = 1L;
}
