package com.captain.crew.admin.api.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/17 18:01
 */
@Data
public class EditPasswordInfo {

    /**
     * 用户名
     */
    private String username;

    /**
     * 旧密码
     */
    @NotBlank(message = "旧密码不能为空")
    private String oldPwd;

    /**
     * 新密码
     */
    @NotBlank(message = "新密码不能为空")
    private String newPwd;

    /**
     * 重复新密码
     */
    @NotBlank(message = "重复密码不能为空")
    private String confirmPwd;
}
