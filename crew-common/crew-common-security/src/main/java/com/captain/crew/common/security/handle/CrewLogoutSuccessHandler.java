package com.captain.crew.common.security.handle;

import com.alibaba.fastjson.JSON;
import com.captain.crew.common.core.dto.R;
import com.captain.crew.common.security.service.SysUserAdmin;
import com.captain.crew.common.security.service.TokenService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author CAP_Crew
 * @Description: 退出登录处理器，把用户信息从redis中清除
 * @date 2021-01-22
 **/
@Component
@Slf4j
@AllArgsConstructor
public class CrewLogoutSuccessHandler implements LogoutSuccessHandler {

    private final TokenService tokenService;

    /**
     * 把用户信息从redis中清除
     *
     * @param request
     * @param response
     * @param authentication
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        SysUserAdmin loginUser = tokenService.getLoginUser(request);
        if (!ObjectUtils.isEmpty(loginUser)) {
            // 删除用户缓存记录
            tokenService.delLoginUser(loginUser.getToken());
        }
        response.setStatus(HttpStatus.OK.value());
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        String json = JSON.toJSONString(R.ok("退出成功", null));
        PrintWriter writer = response.getWriter();
        writer.write(json);
        writer.flush();
    }
}
