/*
 Navicat Premium Data Transfer

 Source Server         : LocaHost
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : unroad

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 04/08/2022 18:15:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for persistent_logins
-- ----------------------------
DROP TABLE IF EXISTS `persistent_logins`;
CREATE TABLE `persistent_logins`  (
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `series` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `last_used` timestamp(0) NOT NULL,
  PRIMARY KEY (`series`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of persistent_logins
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `BLOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `SCHED_NAME`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `QRTZ_BLOB_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `CALENDAR_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `CRON_EXPRESSION` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TIME_ZONE_ID` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `QRTZ_CRON_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('clusteredScheduler', 'restJob', 'restGroup', '1/10 * * * * ? *', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ENTRY_ID` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `FIRED_TIME` bigint(0) NOT NULL,
  `SCHED_TIME` bigint(0) NOT NULL,
  `PRIORITY` int(0) NOT NULL,
  `STATE` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `JOB_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `JOB_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TRIG_INST_NAME`(`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE,
  INDEX `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY`(`SCHED_NAME`, `INSTANCE_NAME`, `REQUESTS_RECOVERY`) USING BTREE,
  INDEX `IDX_QRTZ_FT_J_G`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_JG`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_T_G`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TG`(`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `JOB_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `JOB_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `IS_DURABLE` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `IS_UPDATE_DATA` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_J_REQ_RECOVERY`(`SCHED_NAME`, `REQUESTS_RECOVERY`) USING BTREE,
  INDEX `IDX_QRTZ_J_GRP`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('clusteredScheduler', 'restJob', 'restGroup', NULL, 'com.captain.crew.common.quartz.job.CrewTaskFactory', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C77080000001000000001740004393532377372002F636F6D2E6361707461696E2E637265772E61646D696E2E6170692E656E746974792E646F6D61696E2E5379734A6F62E565B75C4E13BAA80200174C0009636C6173734E616D657400124C6A6176612F6C616E672F537472696E673B4C0008637265617465427971007E00094C000A63726561746554696D657400194C6A6176612F74696D652F4C6F63616C4461746554696D653B4C000E63726F6E45787072657373696F6E71007E00094C000B657865637574655061746871007E00094C00106A6F62457865637574655374617475737400134C6A6176612F6C616E672F496E74656765723B4C00086A6F6247726F757071007E00094C00056A6F62496471007E000B4C00076A6F624E616D6571007E00094C00086A6F624F7264657271007E000B4C00096A6F6253746174757371007E000B4C000D6A6F6254656E616E745479706571007E000B4C00076A6F625479706571007E000B4C000A6D6574686F644E616D6571007E00094C00116D6574686F64506172616D7356616C756571007E00094C000D6D697366697265506F6C69637971007E000B4C00086E65787454696D6571007E000A4C000C70726576696F757354696D6571007E000A4C000672656D61726B71007E00094C0009737461727454696D6571007E000A4C000874656E616E74496471007E000B4C0008757064617465427971007E00094C000A75706461746554696D6571007E000A787070707372000D6A6176612E74696D652E536572955D84BA1B2248B20C00007870770A05000007E50A15061ACE78740010312F3130202A202A202A202A203F202A740014687474703A2F2F7777772E62616964752E636F6D737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000007400097265737447726F75707371007E001100000002740007726573744A6F627371007E00110000000171007E001771007E00177371007E001100000003707071007E001870707400007071007E00177400007371007E000D770A05000007E50A15061ACE787800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `LOCK_NAME` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('clusteredScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------
INSERT INTO `qrtz_paused_trigger_grps` VALUES ('clusteredScheduler', 'restGroup');
INSERT INTO `qrtz_paused_trigger_grps` VALUES ('clusteredScheduler', '_$_ALL_GROUPS_PAUSED_$_');

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `LAST_CHECKIN_TIME` bigint(0) NOT NULL,
  `CHECKIN_INTERVAL` bigint(0) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `REPEAT_COUNT` bigint(0) NOT NULL,
  `REPEAT_INTERVAL` bigint(0) NOT NULL,
  `TIMES_TRIGGERED` bigint(0) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `QRTZ_SIMPLE_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `STR_PROP_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STR_PROP_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STR_PROP_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `INT_PROP_1` int(0) NULL DEFAULT NULL,
  `INT_PROP_2` int(0) NULL DEFAULT NULL,
  `LONG_PROP_1` bigint(0) NULL DEFAULT NULL,
  `LONG_PROP_2` bigint(0) NULL DEFAULT NULL,
  `DEC_PROP_1` decimal(13, 4) NULL DEFAULT NULL,
  `DEC_PROP_2` decimal(13, 4) NULL DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `QRTZ_SIMPROP_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `JOB_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `JOB_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(0) NULL DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(0) NULL DEFAULT NULL,
  `PRIORITY` int(0) NULL DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TRIGGER_TYPE` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `START_TIME` bigint(0) NOT NULL,
  `END_TIME` bigint(0) NULL DEFAULT NULL,
  `CALENDAR_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `MISFIRE_INSTR` smallint(0) NULL DEFAULT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_J`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_JG`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_C`(`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE,
  INDEX `IDX_QRTZ_T_G`(`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_STATE`(`SCHED_NAME`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_N_STATE`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_N_G_STATE`(`SCHED_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_NEXT_FIRE_TIME`(`SCHED_NAME`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST`(`SCHED_NAME`, `TRIGGER_STATE`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_MISFIRE`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  CONSTRAINT `QRTZ_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('clusteredScheduler', 'restJob', 'restGroup', 'restJob', 'restGroup', NULL, 1634800181000, 1634800131000, 5, 'PAUSED', 'CRON', 1634798434000, 0, NULL, 2, 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C77080000001000000001740004393532377372002F636F6D2E6361707461696E2E637265772E61646D696E2E6170692E656E746974792E646F6D61696E2E5379734A6F62E565B75C4E13BAA80200174C0009636C6173734E616D657400124C6A6176612F6C616E672F537472696E673B4C0008637265617465427971007E00094C000A63726561746554696D657400194C6A6176612F74696D652F4C6F63616C4461746554696D653B4C000E63726F6E45787072657373696F6E71007E00094C000B657865637574655061746871007E00094C00106A6F62457865637574655374617475737400134C6A6176612F6C616E672F496E74656765723B4C00086A6F6247726F757071007E00094C00056A6F62496471007E000B4C00076A6F624E616D6571007E00094C00086A6F624F7264657271007E000B4C00096A6F6253746174757371007E000B4C000D6A6F6254656E616E745479706571007E000B4C00076A6F625479706571007E000B4C000A6D6574686F644E616D6571007E00094C00116D6574686F64506172616D7356616C756571007E00094C000D6D697366697265506F6C69637971007E000B4C00086E65787454696D6571007E000A4C000C70726576696F757354696D6571007E000A4C000672656D61726B71007E00094C0009737461727454696D6571007E000A4C000874656E616E74496471007E000B4C0008757064617465427971007E00094C000A75706461746554696D6571007E000A787070707372000D6A6176612E74696D652E536572955D84BA1B2248B20C00007870770A05000007E50A14061ACE78740010312F3130202A202A202A202A203F202A74001668747470733A2F2F7777772E676F6F676C652E636F6D737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000007400097265737447726F75707371007E001100000002740007726573744A6F627371007E00110000000171007E001571007E00177371007E001100000003707071007E00187371007E000D770A05000007E50A14162BCC787371007E000D770A05000007E50A14162BD6787400007371007E000D770A05000007E50A141628DD7871007E001771007E001B7371007E000D770A05000007E50A14061ACE787800);

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '部门ID',
  `parent_id` int(0) NOT NULL COMMENT '父级部门ID',
  `dept_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门名称',
  `tenant_id` int(0) NULL DEFAULT 1 COMMENT '租户ID',
  `sort` int(0) NULL DEFAULT NULL COMMENT '排序',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modify_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `del_flag` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除标记',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (18, -1, '董事会', 1, 1, '2021-09-09 09:45:06', NULL, 0);
INSERT INTO `sys_dept` VALUES (20, -1, '股东大会', 1, 2, '2021-09-09 09:46:05', NULL, 0);
INSERT INTO `sys_dept` VALUES (21, 20, '财务中心', 1, 1, '2021-09-09 01:52:40', NULL, 0);

-- ----------------------------
-- Table structure for sys_dept_relation
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept_relation`;
CREATE TABLE `sys_dept_relation`  (
  `ancestor` int(0) NOT NULL COMMENT '祖先节点',
  `descendant` int(0) NOT NULL COMMENT '后代节点',
  PRIMARY KEY (`ancestor`, `descendant`) USING BTREE,
  INDEX `idx1`(`ancestor`) USING BTREE,
  INDEX `idx2`(`descendant`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '部门关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept_relation
-- ----------------------------
INSERT INTO `sys_dept_relation` VALUES (18, 18);
INSERT INTO `sys_dept_relation` VALUES (20, 20);
INSERT INTO `sys_dept_relation` VALUES (20, 21);
INSERT INTO `sys_dept_relation` VALUES (21, 21);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键自增',
  `dict_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型',
  `description` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典描述',
  `remark` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `tenant_id` int(0) NULL DEFAULT 1 COMMENT '租户ID',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `del_flag` int(0) NULL DEFAULT 0 COMMENT '删除标记 0-正常 1-删除',
  `system` int(0) NULL DEFAULT NULL COMMENT '是否为系统字典 0-是 1-不是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1, 'del_flag', '删除标记', '0-正常；1-删除', 1, NULL, '2021-09-27 03:09:38', 0, 0);
INSERT INTO `sys_dict` VALUES (2, 'ds_scope', '角色权限范围', '角色权限范围 0-全部；1-本级及子级；2-本级；3-自定义', 1, '2020-10-16 16:49:56', NULL, 0, 0);
INSERT INTO `sys_dict` VALUES (3, 'menu_type', '菜单类型', '0-菜单；1-按钮', 1, '2020-10-16 16:52:42', NULL, 0, 0);
INSERT INTO `sys_dict` VALUES (4, 'menu_type', '菜单类型', '0-菜单；1-按钮；', 1, '2020-10-16 17:10:13', '2020-10-16 17:30:07', 1, 0);
INSERT INTO `sys_dict` VALUES (5, 'menu_type', '菜单类型', '0-菜单；1-按钮；', 1, '2020-10-16 17:14:48', '2020-10-16 17:30:04', 1, 0);
INSERT INTO `sys_dict` VALUES (6, 'dict_type', '字典类型', '0-系统级；1-业务级', 1, '2020-10-16 17:58:50', NULL, 0, 0);
INSERT INTO `sys_dict` VALUES (7, 'del_flag', '删除标记', '0-正常；1-删除', 1, '2021-09-27 02:11:28', '2021-09-27 03:07:01', 1, 1);
INSERT INTO `sys_dict` VALUES (8, 'del_flag', '删除标记', '0-正常；1-删除', 1, '2021-09-27 02:12:29', '2021-09-27 03:06:57', 1, 1);
INSERT INTO `sys_dict` VALUES (9, 'log_type', '日志类型', '0-正常；1-异常', 1, '2021-09-28 02:03:00', NULL, 0, 0);
INSERT INTO `sys_dict` VALUES (10, 'job_type', '定时任务类型', '任务类型', 1, '2021-10-21 02:00:30', NULL, 0, 0);
INSERT INTO `sys_dict` VALUES (11, 'job_status', '任务状态', '任务状态', 1, '2021-10-21 02:08:08', NULL, 0, 0);
INSERT INTO `sys_dict` VALUES (12, 'job_execute_type', '任务执行状态', '成功，失败', 1, '2021-10-21 02:09:58', NULL, 0, 0);
INSERT INTO `sys_dict` VALUES (13, 'misfire_type', '定时任务错失执行策略', '定时任务错失执行策略', 1, '2021-10-21 02:12:06', NULL, 0, 0);

-- ----------------------------
-- Table structure for sys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE `sys_dict_item`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键自增',
  `dict_id` bigint(0) NOT NULL COMMENT '字典ID关联字典表',
  `dict_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典类型',
  `label` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典项标签名',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典项描述',
  `value` int(0) NOT NULL COMMENT '字典项值',
  `create_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '最后修改时间',
  `del_flag` int(0) NULL DEFAULT 0 COMMENT '删除标记',
  `tenant_id` int(0) NULL DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_item
-- ----------------------------
INSERT INTO `sys_dict_item` VALUES (1, 1, 'del_flag', '快乐', '试试水', 0, '2021-09-27 03:25:48', '2020-10-16 15:56:42', 0, 1);
INSERT INTO `sys_dict_item` VALUES (2, 1, 'del_flag', '删除', '逻辑删除，无法查询', 1, '2021-09-26 09:11:01', '2020-10-16 16:39:13', 0, 1);
INSERT INTO `sys_dict_item` VALUES (3, 3, 'menu_type', '菜单', '菜单标识', 0, '2021-09-26 09:11:02', '2020-10-16 17:45:21', 0, 1);
INSERT INTO `sys_dict_item` VALUES (4, 3, 'menu_type', '按钮', '按钮标识', 1, '2021-09-26 09:11:03', '2020-10-16 17:45:28', 0, 1);
INSERT INTO `sys_dict_item` VALUES (5, 2, 'ds_scope', '全部', '全部的数据权限', 0, '2021-09-26 09:11:04', '2020-10-16 17:45:54', 0, 1);
INSERT INTO `sys_dict_item` VALUES (6, 2, 'ds_scope', '本级', '只拥有本级部门的数据权限', 1, '2021-09-26 09:11:05', '2020-10-16 17:46:02', 0, 1);
INSERT INTO `sys_dict_item` VALUES (7, 2, 'ds_scope', '本级及子级', '拥有本级和子级部门的数据权限', 2, '2021-09-26 09:11:06', '2020-10-16 17:46:11', 0, 1);
INSERT INTO `sys_dict_item` VALUES (8, 2, 'ds_scope', '自定义', '自定义部门权限', 3, '2021-09-26 09:11:07', '2020-10-16 17:46:19', 0, 1);
INSERT INTO `sys_dict_item` VALUES (9, 6, 'dict_type', '系统级', '系统级别', 0, '2021-09-26 09:11:07', '2020-10-16 17:59:09', 0, 1);
INSERT INTO `sys_dict_item` VALUES (10, 6, 'dict_type', '业务级', '业务级别', 1, '2021-09-26 09:11:10', '2020-10-16 17:59:20', 0, 1);
INSERT INTO `sys_dict_item` VALUES (11, 1, 'del_flag', 'jjj', '冷凉了', 4, '2021-09-27 03:25:42', '2021-09-27 03:25:28', 1, 1);
INSERT INTO `sys_dict_item` VALUES (12, 9, 'log_type', '正常', '正常', 0, NULL, '2021-09-28 02:03:13', 0, 1);
INSERT INTO `sys_dict_item` VALUES (13, 9, 'log_type', '异常', '异常', 1, NULL, '2021-09-28 02:03:22', 0, 1);
INSERT INTO `sys_dict_item` VALUES (14, 10, 'job_type', 'Rest 调用任务', '支持 HTTP GET 请求', 3, NULL, '2021-10-21 02:01:25', 0, 1);
INSERT INTO `sys_dict_item` VALUES (15, 10, 'job_type', '反射 Java 类', '调用本地 Java 方法', 1, NULL, '2021-10-21 02:02:07', 0, 1);
INSERT INTO `sys_dict_item` VALUES (16, 10, 'job_type', 'Spring Bean 调用', '调用 Spring 容器 Bean 方法', 2, NULL, '2021-10-21 02:03:47', 0, 1);
INSERT INTO `sys_dict_item` VALUES (17, 11, 'job_status', '已发布', '已发布', 1, NULL, '2021-10-21 02:08:36', 0, 1);
INSERT INTO `sys_dict_item` VALUES (18, 11, 'job_status', '运行中', '运行中', 2, NULL, '2021-10-21 02:08:47', 0, 1);
INSERT INTO `sys_dict_item` VALUES (19, 11, 'job_status', '暂停', '暂停', 3, NULL, '2021-10-21 02:08:57', 0, 1);
INSERT INTO `sys_dict_item` VALUES (20, 11, 'job_status', '删除', '删除', 4, NULL, '2021-10-21 02:09:06', 0, 1);
INSERT INTO `sys_dict_item` VALUES (21, 12, 'job_execute_type', '执行成功', '执行成功', 0, NULL, '2021-10-21 02:10:11', 0, 1);
INSERT INTO `sys_dict_item` VALUES (22, 12, 'job_execute_type', '执行失败', '执行失败', 1, '2021-10-21 02:10:30', '2021-10-21 02:10:19', 0, 1);
INSERT INTO `sys_dict_item` VALUES (23, 13, 'misfire_type', '默认', '默认', 0, NULL, '2021-10-21 02:12:30', 0, 1);
INSERT INTO `sys_dict_item` VALUES (24, 13, 'misfire_type', '立即执行错失任务', '立即执行错失任务', 1, NULL, '2021-10-21 02:12:40', 0, 1);
INSERT INTO `sys_dict_item` VALUES (25, 13, 'misfire_type', '触发一次执行周期执行', '触发一次执行周期执行', 2, NULL, '2021-10-21 02:12:51', 0, 1);
INSERT INTO `sys_dict_item` VALUES (26, 13, 'misfire_type', '不触发执行周期执行', '不触发执行周期执行', 3, NULL, '2021-10-21 02:13:04', 0, 1);

-- ----------------------------
-- Table structure for sys_files
-- ----------------------------
DROP TABLE IF EXISTS `sys_files`;
CREATE TABLE `sys_files`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键自增',
  `bucket_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '桶名称',
  `file_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件名',
  `modifier` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `size` double NULL DEFAULT NULL COMMENT '文件大小',
  `gmt_create` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `gmt_modify` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后修改时间',
  `tenant_id` int(0) NULL DEFAULT NULL COMMENT '租户ID',
  `del_flag` int(0) NULL DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_files
-- ----------------------------
INSERT INTO `sys_files` VALUES (1, 'demo', '123.png', NULL, 25.2, '2020-12-10 19:13:19', '2020-12-17 21:03:29', 1, 1);
INSERT INTO `sys_files` VALUES (2, 'picture', 'qioeo2oedvz4iuf4', NULL, 109242, '2020-12-16 22:11:29', '2021-01-11 09:17:36', 1, 0);
INSERT INTO `sys_files` VALUES (3, 'picture', 'xxtw9c7wub91ru42', NULL, 58185, '2020-12-16 22:11:27', '2021-01-11 09:17:37', 1, 0);
INSERT INTO `sys_files` VALUES (4, 'picture', 'dzyorvtvefikxput', NULL, 52977, '2020-12-16 22:11:25', '2021-01-26 09:29:55', 1, 1);
INSERT INTO `sys_files` VALUES (5, 'picture', 'ha2y5cx7dyovcucb.jpg', NULL, 168740, '2020-12-16 12:16:56', '2021-01-11 09:17:40', 1, 0);
INSERT INTO `sys_files` VALUES (6, 'picture', 'tufjstye5wiwsbn9.jpg', NULL, 279775, '2020-12-16 12:18:18', '2021-01-11 09:17:41', 1, 0);
INSERT INTO `sys_files` VALUES (7, 'picture', 'szfryduelt48k1rb.jpg', NULL, 279235, '2020-12-16 12:23:07', '2021-01-11 09:17:43', 1, 0);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `job_order` int(0) NULL DEFAULT 1 COMMENT '组内执行顺利，值越大执行优先级越高，最大值9，最小值1',
  `job_type` int(0) NOT NULL DEFAULT 1 COMMENT '1、java类;2、spring bean名称;3、rest调用;4、jar调用;9其他',
  `execute_path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'job_type=3时，rest调用地址，仅支持rest get协议,需要增加String返回值，0成功，1失败;job_type=4时，jar路径;其它值为空',
  `class_name` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'job_type=1时，类完整路径;job_type=2时，spring bean名称;其它值为空',
  `method_name` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务方法',
  `method_params_value` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数值',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '3' COMMENT '错失执行策略（1错失周期立即执行 2错失周期执行一次 3下周期执行）',
  `job_tenant_type` int(0) NULL DEFAULT 1 COMMENT '1、多租户任务;2、非多租户任务',
  `job_status` int(0) NULL DEFAULT 0 COMMENT '状态（1、未发布;2、运行中;3、暂停;4、删除;）',
  `job_execute_status` int(0) NULL DEFAULT 0 COMMENT '状态（0正常 1异常）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `start_time` timestamp(0) NULL DEFAULT NULL COMMENT '初次执行时间',
  `previous_time` timestamp(0) NULL DEFAULT NULL COMMENT '上次执行时间',
  `next_time` timestamp(0) NULL DEFAULT NULL COMMENT '下次执行时间',
  `tenant_id` int(0) NULL DEFAULT 1 COMMENT '租户',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (2, 'restJob', 'restGroup', 1, 3, 'https://www.baidu.com', NULL, NULL, NULL, '1/10 * * * * ? *', '3', 1, 3, 1, NULL, '2021-10-19 14:26:49', '', '2021-10-19 14:26:49', '2021-10-21 06:40:34', '2021-10-21 07:08:51', '2021-10-21 07:09:01', 1, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_id` int(0) NOT NULL COMMENT '任务id',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务组名',
  `job_order` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组内执行顺利，值越大执行优先级越高，最大值9，最小值1',
  `job_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '1、java类;2、spring bean名称;3、rest调用;4、jar调用;9其他',
  `execute_path` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'job_type=3时，rest调用地址，仅支持post协议;job_type=4时，jar路径;其它值为空',
  `class_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'job_type=1时，类完整路径;job_type=2时，spring bean名称;其它值为空',
  `method_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务方法',
  `method_params_value` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数值',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'cron执行表达式',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `job_log_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `execute_time` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '执行时间',
  `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `tenant_id` int(0) NOT NULL DEFAULT 1 COMMENT '租户id',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务执行日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `method_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `params` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` int(0) NULL DEFAULT NULL COMMENT '日志类型',
  `exec_time` bigint(0) NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '菜单名',
  `permission` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限字符串',
  `path` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '访问路径',
  `parent_id` int(0) NULL DEFAULT NULL COMMENT '父菜单ID',
  `icon` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图标',
  `sort` int(0) NULL DEFAULT 1 COMMENT '排序值',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '组件类型， 0菜单、1按钮',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `del_flag` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 'Dashboard', 'dashboard', 'Layout', -1, 'dashboard', 0, '0', '2021-02-05 09:19:26', '2021-05-12 13:12:33', 1);
INSERT INTO `sys_menu` VALUES (2, '工作台', 'workplace', '', 1, NULL, 1, '0', '2021-02-05 09:19:44', '2021-05-12 13:12:34', 1);
INSERT INTO `sys_menu` VALUES (3, '分析页', 'analysis', '', 1, NULL, 2, '0', '2021-02-05 09:19:54', '2021-05-12 13:12:35', 1);
INSERT INTO `sys_menu` VALUES (4, '权限管理', 'permission', 'Layout', -1, 'icon-quanxianguanli', 1, '0', '2020-09-30 16:54:46', '2021-05-12 13:13:10', 0);
INSERT INTO `sys_menu` VALUES (5, '菜单管理', 'sys_menu', '/menu/index', 4, 'icon-caidan', 1, '0', '2020-09-30 16:54:46', '2021-05-14 12:31:21', 0);
INSERT INTO `sys_menu` VALUES (6, '角色管理', 'sys_role', '/unroad/user_role', 4, 'icon-jiaose', 2, '0', '2020-09-30 16:54:46', '2021-07-25 02:33:21', 0);
INSERT INTO `sys_menu` VALUES (7, '用户管理', 'sys_user', '/user/index', 4, 'icon-yonghuguanli', 3, '0', '2020-09-30 16:54:46', '2021-05-14 11:54:42', 0);
INSERT INTO `sys_menu` VALUES (8, '部门管理', 'sys_dept', '/unroad/dept', 4, 'icon-bumenguanli', 4, '0', '2020-10-12 16:37:01', '2021-09-09 03:01:50', 0);
INSERT INTO `sys_menu` VALUES (9, '系统管理', 'system', 'Layout', -1, 'icon-xitong', 2, '0', '2020-10-14 14:06:46', '2021-09-28 06:30:27', 0);
INSERT INTO `sys_menu` VALUES (10, '字典管理', 'sys_dict', '/views/dict/index', 9, 'icon-zidianbiaoguanli', 1, '0', '2020-10-14 14:11:57', '2021-09-25 01:52:55', 0);
INSERT INTO `sys_menu` VALUES (11, '文件管理', 'sys_file', '/unroad/files', 9, 'icon-wenjianguanli', 2, '0', '2020-12-10 18:12:01', '2021-05-12 13:12:46', 1);
INSERT INTO `sys_menu` VALUES (12, 'MinIo 监控', 'minio', 'http://crew-minio:9000', 9, 'icon-shangpin', 4, '0', '2020-12-10 20:04:35', '2021-05-12 13:12:47', 1);
INSERT INTO `sys_menu` VALUES (13, '租户管理', 'sys_tenant', '/pages/tenant/index', 4, NULL, 5, '0', '2021-02-05 07:23:02', '2021-09-09 03:02:00', 0);
INSERT INTO `sys_menu` VALUES (14, '菜单新增', 'sys_menu_add', NULL, 5, NULL, 1, '1', '2021-01-21 02:51:07', '2021-02-05 09:47:55', 0);
INSERT INTO `sys_menu` VALUES (15, '菜单修改', 'sys_menu_edit', NULL, 5, NULL, 2, '1', '2021-01-26 07:06:52', '2021-02-05 09:47:56', 0);
INSERT INTO `sys_menu` VALUES (16, '菜单删除', 'sys_menu_remove', '', 5, '', 3, '1', '2021-01-26 08:13:53', '2021-02-05 09:47:59', 0);
INSERT INTO `sys_menu` VALUES (17, '角色新增', 'sys_role_add', '', 6, '', 1, '1', '2021-01-26 08:24:17', '2021-02-05 09:48:05', 0);
INSERT INTO `sys_menu` VALUES (18, '角色修改', 'sys_role_edit', '', 6, '', 2, '1', '2021-01-26 08:24:47', '2021-02-05 09:48:06', 0);
INSERT INTO `sys_menu` VALUES (19, '角色删除', 'sys_role_remove', '', 6, '', 3, '1', '2021-01-26 08:25:09', '2021-02-05 09:48:10', 0);
INSERT INTO `sys_menu` VALUES (20, '分配权限', 'sys_role_perm', '', 6, '', 4, '1', '2021-01-26 08:25:42', '2021-02-05 09:48:12', 0);
INSERT INTO `sys_menu` VALUES (21, '用户新增', 'sys_user_add', '', 7, '', 1, '1', '2021-01-26 08:35:43', '2021-02-05 09:48:19', 0);
INSERT INTO `sys_menu` VALUES (22, '用户修改', 'sys_user_edit', '', 7, '', 2, '1', '2021-01-26 08:36:06', '2021-02-05 09:48:20', 0);
INSERT INTO `sys_menu` VALUES (23, '用户删除', 'sys_user_remove', '', 7, '', 3, '1', '2021-01-26 08:36:38', '2021-02-05 09:48:22', 0);
INSERT INTO `sys_menu` VALUES (24, '部门新增', 'sys_dept_add', '', 8, '', 1, '1', '2021-01-26 08:37:07', '2021-09-09 09:04:57', 0);
INSERT INTO `sys_menu` VALUES (25, '部门修改', 'sys_dept_edit', '', 8, '', 2, '1', '2021-01-26 08:37:27', '2021-02-05 09:48:29', 0);
INSERT INTO `sys_menu` VALUES (26, '部门删除', 'sys_dept_remove', '', 8, '', 3, '1', '2021-01-26 08:37:52', '2021-02-05 09:48:31', 0);
INSERT INTO `sys_menu` VALUES (27, '租户新增', 'sys_tenant_add', '', 13, '', 1, '1', '2021-01-26 08:38:24', '2021-02-05 09:48:35', 0);
INSERT INTO `sys_menu` VALUES (28, '租户修改', 'sys_tenant_edit', '', 13, '', 2, '1', '2021-01-26 08:38:44', '2021-02-05 09:48:36', 0);
INSERT INTO `sys_menu` VALUES (29, '租户删除', 'sys_tenant_remove', '', 13, '', 3, '1', '2021-01-26 08:39:07', '2021-02-05 09:48:38', 0);
INSERT INTO `sys_menu` VALUES (30, '字典新增', 'sys_dict_add', '', 10, '', 1, '1', '2021-01-26 08:40:15', '2021-02-05 09:49:00', 0);
INSERT INTO `sys_menu` VALUES (31, '字典修改', 'sys_dict_edit', '', 10, '', 2, '1', '2021-01-26 08:40:42', '2021-09-27 02:14:46', 0);
INSERT INTO `sys_menu` VALUES (32, '字典删除', 'sys_dict_remove', '', 10, '', 3, '1', '2021-01-26 08:41:08', '2021-02-05 09:49:03', 0);
INSERT INTO `sys_menu` VALUES (33, '文件删除', 'sys_file_remove', '', 11, '', 1, '1', '2021-01-26 08:41:35', '2021-02-05 09:49:07', 0);
INSERT INTO `sys_menu` VALUES (34, '查看部门', 'dept_view', NULL, 8, NULL, 4, '1', '2021-09-09 07:11:02', '2021-09-09 09:05:03', 1);
INSERT INTO `sys_menu` VALUES (35, '文件管理', 'sys_file', '/views/file/index', 9, NULL, 2, '0', '2021-09-25 01:55:42', NULL, 0);
INSERT INTO `sys_menu` VALUES (36, '添加字典项', 'sys_dict_item_save', NULL, 10, NULL, 4, '1', '2021-09-27 03:12:25', NULL, 0);
INSERT INTO `sys_menu` VALUES (37, '修改字典项', 'sys_dict_item_edit', NULL, 36, NULL, 5, '1', '2021-09-27 03:12:49', '2021-09-27 03:12:55', 1);
INSERT INTO `sys_menu` VALUES (38, '修改字典项', 'sys_dict_item_edit', NULL, 10, NULL, 5, '1', '2021-09-27 03:13:18', NULL, 0);
INSERT INTO `sys_menu` VALUES (39, '删除字典项', 'sys_dict_item_remove', NULL, 10, NULL, 6, NULL, '2021-09-27 03:13:41', NULL, 0);
INSERT INTO `sys_menu` VALUES (40, '日志管理', 'sys_log', '/views/log/index', 9, NULL, 3, '0', '2021-09-27 07:45:08', NULL, 0);
INSERT INTO `sys_menu` VALUES (41, '系统监控', 'monitor', 'Layout', -1, NULL, 3, '0', '2021-09-28 07:28:54', NULL, 0);
INSERT INTO `sys_menu` VALUES (42, '文件管理', 'monitor-minio', 'http://crew-minio:9000', 41, NULL, 1, '0', '2021-09-28 07:29:27', '2021-09-28 07:32:52', 0);
INSERT INTO `sys_menu` VALUES (43, '接口文档', 'monitor-swagger', 'http://localhost:8090/doc.html', 41, NULL, 2, '0', '2021-09-28 07:30:24', '2021-09-28 07:32:42', 0);
INSERT INTO `sys_menu` VALUES (44, '定时任务', 'sys_job', '/views/job/index', 9, NULL, 4, '0', '2021-10-20 03:13:54', '2021-10-20 03:15:02', 0);
INSERT INTO `sys_menu` VALUES (45, '暂停任务', 'sys_job_pause', NULL, 44, NULL, 1, '1', '2021-10-20 06:47:17', NULL, 0);
INSERT INTO `sys_menu` VALUES (46, '启动任务', 'sys_job_start', NULL, 44, NULL, 2, '1', '2021-10-20 06:47:38', NULL, 0);
INSERT INTO `sys_menu` VALUES (47, '发布任务', 'sys_job_save', NULL, 44, NULL, 3, NULL, '2021-10-20 06:47:59', NULL, 0);
INSERT INTO `sys_menu` VALUES (48, '修改任务', 'sys_job_update', NULL, 44, NULL, 4, NULL, '2021-10-20 06:48:33', NULL, 0);
INSERT INTO `sys_menu` VALUES (49, '删除任务', 'sys_job_delete', NULL, 44, NULL, 5, NULL, '2021-10-20 06:48:58', NULL, 0);
INSERT INTO `sys_menu` VALUES (50, '暂停全部定时任务', 'sys_jobs_shutdown', NULL, 44, NULL, 6, NULL, '2021-10-20 06:49:20', NULL, 0);
INSERT INTO `sys_menu` VALUES (51, '启动全部定时任务', 'sys_jobs_start', NULL, 44, NULL, 7, NULL, '2021-10-20 06:49:37', NULL, 0);
INSERT INTO `sys_menu` VALUES (52, 'Quartz 日志', 'sys_job_log', '/views/job/jobLog', 41, NULL, 3, '0', '2021-10-21 07:21:18', NULL, 0);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `role_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色名',
  `role_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色标识',
  `role_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色的中文描述',
  `ds_scope` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限范围',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0',
  `tenant_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`) USING BTREE,
  INDEX `role_idx1_role_code`(`role_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', 'ROLE_ADMIN', '管理员', '0', '2020-09-30 16:40:57', NULL, '0', 1);
INSERT INTO `sys_role` VALUES (2, '用户', 'ROLE_USER', '用户', '2', '2020-09-30 16:41:20', NULL, '0', 1);
INSERT INTO `sys_role` VALUES (3, '领导', 'ROLE_LEADER', '部门经理', '1', '2020-10-12 15:47:27', '2021-09-06 09:07:10', '1', 2);
INSERT INTO `sys_role` VALUES (4, '员工', 'ROLE_EMPL', '员工', '0', '2021-01-11 10:56:55', '2021-09-06 08:32:19', '1', 2);
INSERT INTO `sys_role` VALUES (5, '小跟班', 'ROLE_LITTLE', '小跟班，狗腿子', '1', '2021-09-06 08:51:32', '2021-09-10 09:46:28', '0', 2);
INSERT INTO `sys_role` VALUES (6, '扫地僧', 'ROLR_CLEAN', '不可小觑的扫地僧', '3', '2021-09-06 09:07:48', '2021-09-10 09:46:30', '0', 2);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` int(0) NOT NULL COMMENT '角色ID',
  `menu_id` int(0) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 4);
INSERT INTO `sys_role_menu` VALUES (1, 5);
INSERT INTO `sys_role_menu` VALUES (1, 6);
INSERT INTO `sys_role_menu` VALUES (1, 7);
INSERT INTO `sys_role_menu` VALUES (1, 8);
INSERT INTO `sys_role_menu` VALUES (1, 9);
INSERT INTO `sys_role_menu` VALUES (1, 10);
INSERT INTO `sys_role_menu` VALUES (1, 13);
INSERT INTO `sys_role_menu` VALUES (1, 14);
INSERT INTO `sys_role_menu` VALUES (1, 15);
INSERT INTO `sys_role_menu` VALUES (1, 16);
INSERT INTO `sys_role_menu` VALUES (1, 17);
INSERT INTO `sys_role_menu` VALUES (1, 18);
INSERT INTO `sys_role_menu` VALUES (1, 19);
INSERT INTO `sys_role_menu` VALUES (1, 20);
INSERT INTO `sys_role_menu` VALUES (1, 21);
INSERT INTO `sys_role_menu` VALUES (1, 22);
INSERT INTO `sys_role_menu` VALUES (1, 23);
INSERT INTO `sys_role_menu` VALUES (1, 24);
INSERT INTO `sys_role_menu` VALUES (1, 25);
INSERT INTO `sys_role_menu` VALUES (1, 26);
INSERT INTO `sys_role_menu` VALUES (1, 27);
INSERT INTO `sys_role_menu` VALUES (1, 28);
INSERT INTO `sys_role_menu` VALUES (1, 29);
INSERT INTO `sys_role_menu` VALUES (1, 30);
INSERT INTO `sys_role_menu` VALUES (1, 31);
INSERT INTO `sys_role_menu` VALUES (1, 32);
INSERT INTO `sys_role_menu` VALUES (1, 35);
INSERT INTO `sys_role_menu` VALUES (1, 36);
INSERT INTO `sys_role_menu` VALUES (1, 38);
INSERT INTO `sys_role_menu` VALUES (1, 39);
INSERT INTO `sys_role_menu` VALUES (1, 40);
INSERT INTO `sys_role_menu` VALUES (1, 41);
INSERT INTO `sys_role_menu` VALUES (1, 42);
INSERT INTO `sys_role_menu` VALUES (1, 43);
INSERT INTO `sys_role_menu` VALUES (1, 44);
INSERT INTO `sys_role_menu` VALUES (1, 45);
INSERT INTO `sys_role_menu` VALUES (1, 46);
INSERT INTO `sys_role_menu` VALUES (1, 47);
INSERT INTO `sys_role_menu` VALUES (1, 48);
INSERT INTO `sys_role_menu` VALUES (1, 49);
INSERT INTO `sys_role_menu` VALUES (1, 50);
INSERT INTO `sys_role_menu` VALUES (1, 51);
INSERT INTO `sys_role_menu` VALUES (1, 52);
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 5);
INSERT INTO `sys_role_menu` VALUES (2, 6);
INSERT INTO `sys_role_menu` VALUES (2, 7);
INSERT INTO `sys_role_menu` VALUES (2, 8);
INSERT INTO `sys_role_menu` VALUES (2, 14);
INSERT INTO `sys_role_menu` VALUES (2, 15);
INSERT INTO `sys_role_menu` VALUES (2, 16);
INSERT INTO `sys_role_menu` VALUES (2, 17);
INSERT INTO `sys_role_menu` VALUES (2, 18);
INSERT INTO `sys_role_menu` VALUES (2, 19);
INSERT INTO `sys_role_menu` VALUES (2, 20);
INSERT INTO `sys_role_menu` VALUES (2, 21);
INSERT INTO `sys_role_menu` VALUES (2, 22);
INSERT INTO `sys_role_menu` VALUES (2, 23);
INSERT INTO `sys_role_menu` VALUES (2, 24);
INSERT INTO `sys_role_menu` VALUES (2, 25);
INSERT INTO `sys_role_menu` VALUES (2, 26);
INSERT INTO `sys_role_menu` VALUES (2, 27);
INSERT INTO `sys_role_menu` VALUES (2, 28);
INSERT INTO `sys_role_menu` VALUES (2, 29);
INSERT INTO `sys_role_menu` VALUES (5, 4);
INSERT INTO `sys_role_menu` VALUES (5, 5);
INSERT INTO `sys_role_menu` VALUES (5, 6);
INSERT INTO `sys_role_menu` VALUES (5, 7);
INSERT INTO `sys_role_menu` VALUES (5, 14);
INSERT INTO `sys_role_menu` VALUES (5, 17);
INSERT INTO `sys_role_menu` VALUES (5, 21);
INSERT INTO `sys_role_menu` VALUES (6, 4);
INSERT INTO `sys_role_menu` VALUES (6, 7);
INSERT INTO `sys_role_menu` VALUES (6, 22);
INSERT INTO `sys_role_menu` VALUES (6, 23);

-- ----------------------------
-- Table structure for sys_tenant
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant`;
CREATE TABLE `sys_tenant`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键Id',
  `tenant_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户名称',
  `status` int(0) NOT NULL COMMENT '状态 0 正常 1 锁定',
  `begin_time` timestamp(0) NOT NULL COMMENT '开始时间',
  `end_time` timestamp(0) NOT NULL COMMENT '结束时间',
  `del_flag` int(0) NOT NULL DEFAULT 0 COMMENT '删除标识',
  `tenant_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户码',
  `gmt_create` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `gmt_modify` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后修改时间',
  `modifier` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_tenant
-- ----------------------------
INSERT INTO `sys_tenant` VALUES (1, '笨熊家族', 0, '2020-12-24 09:41:44', '2030-07-02 09:41:47', 0, '1', '2020-12-24 09:42:24', '2020-12-24 22:28:41', NULL);
INSERT INTO `sys_tenant` VALUES (2, '复仇者联盟', 0, '2020-12-23 16:00:00', '2021-12-24 16:00:00', 0, '2', '2020-12-24 14:53:52', NULL, 'user');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户名',
  `nickname` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '昵称',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  `salt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码加盐',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
  `dept_id` int(0) NULL DEFAULT NULL COMMENT '部门ID',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `lock_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0',
  `tenant_id` int(0) NULL DEFAULT 1 COMMENT '租户ID',
  PRIMARY KEY (`user_id`) USING BTREE,
  INDEX `user_idx1_username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'user', '平头老大哥', '$2a$10$HDmkjxTtyOpo07gPYVTf8etjhRrwuS2G5KCdHNDoMZEv1V6eaOmKS', NULL, '15090371157', 'http://192.168.126.130:9000/picture/szfryduelt48k1rb.jpg', 18, '超级管理员', '2020-09-30 16:35:35', '2021-09-14 02:35:25', '0', '0', 1);
INSERT INTO `sys_user` VALUES (16, 'admin', '涂山蓉蓉', '$2a$10$qoOGg8EWO4Lrk4LNM1Mpmu2TYeOivwvAtX9cpsHHPq16leb3t1Wyq', NULL, '13599855659', NULL, 21, '？？', '2021-06-27 03:35:27', '2021-09-25 01:20:04', '0', '0', 1);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` int(0) NOT NULL COMMENT '用户ID',
  `role_id` int(0) NOT NULL COMMENT '角色ID',
  `tenant_id` int(0) NULL DEFAULT 1 COMMENT '租户ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1, 1);
INSERT INTO `sys_user_role` VALUES (1, 2, 1);
INSERT INTO `sys_user_role` VALUES (2, 2, 1);
INSERT INTO `sys_user_role` VALUES (3, 4, 2);
INSERT INTO `sys_user_role` VALUES (4, 3, 2);
INSERT INTO `sys_user_role` VALUES (16, 1, 1);
INSERT INTO `sys_user_role` VALUES (16, 2, 1);
INSERT INTO `sys_user_role` VALUES (17, 1, 1);
INSERT INTO `sys_user_role` VALUES (17, 2, 1);
INSERT INTO `sys_user_role` VALUES (17, 3, 1);
INSERT INTO `sys_user_role` VALUES (17, 4, 1);

SET FOREIGN_KEY_CHECKS = 1;
