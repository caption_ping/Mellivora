package com.captain.crew.common.quartz.exception;

/**
 * @author CAP_Crew
 * @date 2021-10-19
 **/
public class TaskException extends Exception{

    public TaskException() {
    }

    public TaskException(String message) {
        super(message);
    }
}
