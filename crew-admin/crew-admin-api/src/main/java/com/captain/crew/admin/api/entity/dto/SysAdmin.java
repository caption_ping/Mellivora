package com.captain.crew.admin.api.entity.dto;

import lombok.Data;

import java.util.List;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-02-03
 **/
@Data
public class SysAdmin {

    /**
     * token
     */
    private java.lang.String token;
    /**
     * 过期时间
     */
    private long expireAt;

    /**
     * 用户信息
     */
    private User user;

    private List<Permission> permissions;

    private List<Permission> roles;

    private Integer tenantId;

    @Data
    public static class User {
        /**
         * 用户名（昵称）
         */
        private java.lang.String name;
        /**
         * 头像
         */
        private java.lang.String avatar;
        /**
         * 地址
         */
        private java.lang.String address;
        /**
         * 位置
         */
        private java.lang.String position;
    }
}
