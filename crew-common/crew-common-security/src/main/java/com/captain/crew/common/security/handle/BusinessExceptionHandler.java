package com.captain.crew.common.security.handle;

import com.captain.crew.common.core.enums.ResponseCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: crewer
 * @Description: 自定义异常处理 Handler
 * @Date: 2020/10/21 13:42
 */
@Slf4j
@ControllerAdvice
public class BusinessExceptionHandler {

    // =========================================================================================== //
    // security

    /**
     * BadCredentialsException 异常处理
     *
     * @param e
     * @return
     */
    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<?> handBadCredentialsException(BadCredentialsException e) {
        log.error(e.getMessage());
        Map<String, Object> error = new HashMap<>(2);
        error.put("code", ResponseCode.BAD_CREDENTIALS.getCode());
        error.put("msg", ResponseCode.BAD_CREDENTIALS.getMsg());
        return new ResponseEntity<>(error, HttpStatus.OK);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> handAccessDeniedException(AccessDeniedException e) {
        log.error(e.getMessage());
        Map<String, Object> error = new HashMap<>(2);
        error.put("code", ResponseCode.ACCESS_DENIED.getCode());
        error.put("msg", ResponseCode.ACCESS_DENIED.getMsg());
        return new ResponseEntity<>(error, HttpStatus.OK);
    }

    @ExceptionHandler(AccountExpiredException.class)
    public ResponseEntity<?> handleAccountExpiredException(AccountExpiredException e) {
        log.error(e.getMessage());
        Map<String, Object> error = new HashMap<>(2);
        error.put("code", ResponseCode.FAILED.getCode());
        error.put("msg", e.getMessage());
        return new ResponseEntity<>(error, HttpStatus.OK);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<?> handleUsernameNotFoundException(UsernameNotFoundException e) {
        log.error(e.getMessage(), e);
        Map<String, Object> error = new HashMap<>(2);
        error.put("code", ResponseCode.BAD_CREDENTIALS.getCode());
        error.put("msg", ResponseCode.BAD_CREDENTIALS.getMsg());
        return new ResponseEntity<>(error, HttpStatus.OK);
    }

    // =========================================================================================== //
    // 业务异常

    @ExceptionHandler({BusinessException.class, Exception.class})
    public ResponseEntity<?> handlerException(HttpServletRequest request, Exception ex) {
        Map<String, Object> error = new HashMap<>(2);

        // 业务异常
        if (ex instanceof BusinessException) {
            error.put("code", ((BusinessException) ex).getCode());
            error.put("msg", ex.getMessage());
            log.warn("[全局业务异常]业务编码：{}异常记录：{}", error.get("code"), error.get("msg"));
            // TODO 业务异常的处理
        }

        // 未知错误
        else {
            error.put("code", ResponseCode.UNKNOWN.getCode());
            error.put("msg", ResponseCode.UNKNOWN.getMsg());
            log.error(ex.getMessage());
            // TODO 未知错误异常处理
        }
        ex.printStackTrace();

        return new ResponseEntity<>(error, HttpStatus.OK);
    }
}
