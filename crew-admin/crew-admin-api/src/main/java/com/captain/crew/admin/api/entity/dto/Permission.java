package com.captain.crew.admin.api.entity.dto;

import lombok.Data;

import java.util.List;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-02-04
 **/
@Data
public class Permission {

    private SysAdmin id;

    private List<SysAdmin> operation;
}
