package com.captain.crew.admin.upms.service.auth.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.captain.crew.admin.api.entity.domain.SysDeptRelation;
import com.captain.crew.admin.api.entity.domain.SysRole;
import com.captain.crew.admin.api.service.auth.DataScopeService;
import com.captain.crew.admin.upms.service.auth.SysDeptRelationService;
import com.captain.crew.admin.upms.service.auth.SysRoleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author CAP_Crew
 * @Description
 * @date 2021-09-10
 **/
@Service
@AllArgsConstructor
public class DataScopeServiceImpl implements DataScopeService {

    private final SysRoleService roleService;
    private final SysDeptRelationService relationService;

    @Override
    public List<SysRole> getRoleList(List<String> roleId) {
        return roleService.listByIds(roleId);
    }

    @Override
    public List<SysDeptRelation> getDescendantList(Integer deptId) {
        return relationService.list(Wrappers.<SysDeptRelation>lambdaQuery()
                .eq(SysDeptRelation::getAncestor, deptId));
    }
}
