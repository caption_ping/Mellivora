package com.captain.crew.common.core.vo;

import com.captain.crew.common.core.dto.TreeNode;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2020-12-01
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class TreeNodeView extends TreeNode implements Serializable {
    /**
     * 节点标题
     */
    private String label;

    /**
     * 排序编号
     */
    private Integer sort;

    /**
     * 是否可编辑
     */
    private boolean disabled;

    /**
     * 是否展开该节点
     */
    private Boolean spread = true;
}
