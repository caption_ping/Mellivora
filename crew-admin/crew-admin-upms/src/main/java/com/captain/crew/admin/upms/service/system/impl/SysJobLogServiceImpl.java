package com.captain.crew.admin.upms.service.system.impl;

import com.captain.crew.admin.api.entity.domain.SysJobLog;
import com.captain.crew.admin.api.service.system.SysJobLogService;
import com.captain.crew.admin.upms.mapper.SysJobLogMapper;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @author PC
 */
@Service
public class SysJobLogServiceImpl extends ServiceImpl<SysJobLogMapper, SysJobLog> implements SysJobLogService {

}
