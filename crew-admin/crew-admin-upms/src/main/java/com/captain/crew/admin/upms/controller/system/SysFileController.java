package com.captain.crew.admin.upms.controller.system;

import cn.hutool.core.io.IoUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.captain.crew.admin.api.entity.domain.SysFiles;
import com.captain.crew.admin.upms.service.system.FilesService;
import com.captain.crew.common.core.dto.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2020-12-07
 **/
@RestController
@RequestMapping("/file")
@AllArgsConstructor
@Api(value = "file", tags = "文件管理模块")
public class SysFileController {

    private final FilesService filesService;

    /**
     * 上传图片
     *
     * @param file
     * @return
     */
    @ApiOperation(value = "上传图片", notes = "上传成功后返回照片的url")
    @PostMapping("/picture")
    public R<String> uploadPicture(MultipartFile file) {
        return R.ok(filesService.uploadPicture(file));
    }

    /**
     * 分页查询
     *
     * @param page
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public R<List<SysFiles>> list(Page<SysFiles> page) {
        IPage<SysFiles> filePage = filesService.queryPage(page);
        long total = filePage.getTotal();
        return R.ok((int) total, filePage.getRecords());
    }

    /**
     * 删除文件
     *
     * @param id
     * @return
     */
    @ApiOperation("删除文件")
    @DeleteMapping("/{id}")
    @PreAuthorize("@ums.hasPermission('sys_file_remove')")
    public R<Void> remove(@PathVariable("id") Integer id) {
        filesService.remove(id);
        return R.ok();
    }

    /**
     * 下载
     *
     * @param id
     * @param response
     */
    @ApiOperation("下载")
    @GetMapping("/download")
    public void download(@RequestParam("id") Integer id,
                         HttpServletResponse response) {
        InputStream inputStream = filesService.downLoad(id);
        SysFiles file = filesService.getById(id);
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        response.addHeader("Content-Disposition", "attchement;filename=" + file.getFileName());
        byte[] body = new byte[1024];
        BufferedInputStream bf = new BufferedInputStream(inputStream);
        ServletOutputStream outputStream = null;
        try {
            outputStream = response.getOutputStream();
            int i = bf.read(body);
            while (i != -1) {
                outputStream.write(body, 0, i);
                i = bf.read(body);
            }
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IoUtil.close(inputStream);
            IoUtil.close(outputStream);
            IoUtil.close(bf);
        }
    }
}
