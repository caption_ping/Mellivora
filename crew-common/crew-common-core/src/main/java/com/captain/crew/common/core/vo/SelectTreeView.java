package com.captain.crew.common.core.vo;

import com.captain.crew.common.core.dto.TreeNode;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author CAP_Crew
 * @Description: 下拉框选择树视图传输对象
 * @date 2020-12-03
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class SelectTreeView extends TreeNode {
    /**
     * 节点标题
     */
    private String name;

    /**
     * 节点值
     */
    private Integer value;

    /**
     * 是否可编辑
     */
    private Boolean disabled;

    /**
     * 是否默认被选中
     */
    private Boolean selected;
}
