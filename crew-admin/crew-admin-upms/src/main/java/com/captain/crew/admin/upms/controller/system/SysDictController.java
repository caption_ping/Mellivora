package com.captain.crew.admin.upms.controller.system;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.captain.crew.admin.api.entity.domain.SysDict;
import com.captain.crew.admin.upms.service.system.SysDictService;
import com.captain.crew.common.core.dto.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @author CAP_Crew
 * @date 2020-10-14
 */
@Slf4j
@RequestMapping("/dict")
@RestController
@AllArgsConstructor
@Api(value = "dict", tags = "字典管理模块")
public class SysDictController {

    private final SysDictService dictService;

    /**
     * 分页查询所有字典类型
     *
     * @param page 分页参数
     * @return 字典项列表
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public R<?> list(Page<SysDict> page) {
        return R.ok(dictService.queryList(page));
    }

    /**
     * 保存字典信息
     *
     * @param sysDict 字典实体类
     * @return 处理成功与否
     */
    @ApiOperation("新增")
    @PostMapping
    @PreAuthorize("@ums.hasPermission('sys_dict_add')")
    public R<Boolean> save(@RequestBody SysDict sysDict) {
        return R.ok(dictService.save(sysDict));
    }

    @ApiOperation("修改字典")
    @PutMapping
    @PreAuthorize("@ums.hasPermission('sys_dict_edit')")
    public R<?> edit(@RequestBody SysDict sysDict) {
        return R.ok(dictService.edit(sysDict));
    }

    /**
     * 删除字典类型
     *
     * @param dictId 字典类型ID
     * @return 处理结果与否
     */
    @ApiOperation("删除")
    @DeleteMapping("/{dictId}")
    @PreAuthorize("@ums.hasPermission('sys_dict_remove')")
    public R<Boolean> removeDict(@PathVariable Integer dictId) {
        log.info("dict 的值为: {}", dictId);
        if (dictId != null) {
            return R.ok(dictService.removeById(dictId));
        } else {
            return R.failed();
        }
    }
}
