package com.captain.crew.admin.upms;

import com.captain.crew.common.swagger.annotation.EnableCrewSwaggerConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * spring boot
 *
 * @author crewer
 */
@SpringBootApplication
@EnableCrewSwaggerConfig
@EnableCaching
@EnableAsync
@MapperScan({"com.captain.crew.admin.upms.mapper"})
public class CrewAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(CrewAdminApplication.class, args);
    }
}
