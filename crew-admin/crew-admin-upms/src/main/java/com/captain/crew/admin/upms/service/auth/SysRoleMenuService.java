package com.captain.crew.admin.upms.service.auth;

import com.baomidou.mybatisplus.extension.service.IService;
import com.captain.crew.admin.api.entity.domain.SysMenu;
import com.captain.crew.admin.api.entity.domain.SysRoleMenu;
import com.captain.crew.admin.api.entity.dto.MenuRouter;
import com.captain.crew.common.core.dto.MenuTreeNode;

import java.util.List;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/28 17:52
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

    /**
     * 更新角色菜单
     *
     * @param roleId  角色
     * @param menuIds 菜单ID拼成的字符串，每个id之间根据逗号分隔
     * @return boolean
     */
    Boolean saveRoleMenus(Integer roleId, String menuIds);

    /**
     * 根据角色名返回菜单集合对象
     *
     * @param roleId 角色ID
     * @return 菜单集合对象
     */
    List<Integer> queryMenuByRoleId(Integer roleId);
}
