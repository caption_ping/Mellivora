package com.captain.crew.common.core.vo;

import com.captain.crew.common.core.dto.TreeNode;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author CAP_Crew
 * @Description
 * @date 2021-09-09
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class DeptTreeNode extends TreeNode implements Serializable {

    /**
     * 节点标题
     */
    private String label;

    /**
     * 排序编号
     */
    private Integer sort;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
}
