package com.captain.crew.admin.api.entity.domain;

import lombok.Data;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/10/9 9:27
 */
@Data
public class SysRoleMenu {
    /**
     * 角色ID
     */
    private Integer roleId;

    /**
     * 菜单ID
     */
    private Integer menuId;
}
