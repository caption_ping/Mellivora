package com.captain.crew.common.mybatis.dataScope;

import java.util.List;

/**
 * @author CAP_Crew
 * @Description
 * @date 2021-09-10
 **/
public interface DataScopeHandle {

    /**
     * 获取用户所拥有的数据权限范围
     * 如果有全部数据的权限 或者 不需要进行权限过滤返回 true
     * 如果有部分部门的权限直接在入参 deptId 中添加并返回 false
     * 如果没有任何权限，返回 false
     *
     * @param deptId
     * @return
     */
    Boolean getScope(List<Integer> deptId);
}
