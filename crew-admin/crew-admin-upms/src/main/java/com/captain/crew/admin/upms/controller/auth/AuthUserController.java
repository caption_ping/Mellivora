package com.captain.crew.admin.upms.controller.auth;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.captain.crew.admin.api.entity.domain.SysUser;
import com.captain.crew.admin.api.entity.dto.EditPasswordInfo;
import com.captain.crew.admin.api.service.auth.SysUserService;
import com.captain.crew.admin.upms.service.auth.AuthUerService;
import com.captain.crew.common.core.dto.R;
import com.captain.crew.common.security.util.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/17 17:37
 */
@Slf4j
@RestController
@RequestMapping("/user")
@AllArgsConstructor
@Api(value = "user", tags = "用户管理模块")
public class AuthUserController {

    private final SysUserService userService;
    private final AuthUerService authUerService;
    private final BCryptPasswordEncoder passwordEncoder;

    /**
     * 分页查询
     *
     * @param page
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/list")
    public R<?> list(Page<SysUser> page, SysUser user) {
        return R.ok(userService.queryList(page, user));
    }

    /**
     * 修改用户密码
     *
     * @param userInfo
     * @param bindingResult
     * @return
     */
    @ApiOperation("修改用户信息")
    @PutMapping("/pwd")
    public R<Void> editPassword(@RequestBody @Valid EditPasswordInfo userInfo, BindingResult bindingResult) {
        // 验证参数是否存在问题，如果存在直接返回错误信息
        List<FieldError> errors = bindingResult.getFieldErrors();
        if (errors.size() != 0) {
            return R.failed("表单信息不完整", null);
        }

        String username = SecurityUtils.getUser().getUsername();
        userInfo.setUsername(username);
        return userService.editPassword(userInfo);
    }

    /**
     * 根据用户名查看用户信息
     *
     * @return
     */
    @ApiOperation("根据用户名查看用户信息")
    @GetMapping("/info")
    public R<SysUser> getUserInfo() {
        String username = SecurityUtils.getUser().getUsername();
        // 如果cookie中查询不到用户信息，返回错误信息跳转重新登录
        if (StringUtils.isBlank(username)) {
            return R.failed("登录信息已过期，请重新登录", null);
        }
        SysUser entity = userService.getOne(Wrappers.<SysUser>lambdaQuery().eq(SysUser::getUsername, username));
        return R.ok(entity);
    }

    /**
     * 修改用户基本信息，仅限于昵称，备注
     * 适用于个人中心
     *
     * @param user
     * @return
     */
    @ApiOperation("修改用户基本信息")
    @PutMapping(value = "/info")
    public R<?> editInfo(@RequestBody SysUser user) {
        return R.ok(userService.update(Wrappers.<SysUser>lambdaUpdate()
                .set(SysUser::getRemark, user.getRemark())
                .set(SysUser::getNickname, user.getNickname())
                .eq(SysUser::getUsername, user.getUsername())));
    }

    /**
     * 删除用户信息
     *
     * @param userId
     * @return
     */
    @ApiOperation("删除用户")
    @DeleteMapping
    @PreAuthorize("@ums.hasPermission('sys_user_remove')")
    public R<?> removeUser(@RequestParam("userId") Integer userId) {
        return R.ok(userService.remove(Wrappers.<SysUser>lambdaQuery().eq(SysUser::getUserId, userId)));
    }

    /**
     * 新添加用户
     *
     * @param user 用户实体类
     * @return 操作结果
     */
    @ApiOperation("新添加用户")
    @PostMapping
    @PreAuthorize("@ums.hasPermission('sys_user_add')")
    public R<Boolean> save(@RequestBody SysUser user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return R.ok(userService.saveUser(user));
    }

    /**
     * 修改用户和角色对应关系及全部用户信息
     *
     * @param user 用户实体类
     * @return 操作结果
     */
    @ApiOperation("修改用户和角色对应关系及全部用户信息")
    @PostMapping("/edit")
    @PreAuthorize("@ums.hasPermission('sys_user_edit')")
    public R<Boolean> edit(@RequestBody SysUser user) {
        return R.ok(userService.editUserInfo(user));
    }

    /**
     * 修改头像
     *
     * @param avatar 用户头像
     * @return 操作结果
     */
    @ApiOperation("修改头像")
    @RequestMapping(value = "/avatar", method = RequestMethod.POST)
    public R<String> editAvatar(MultipartFile avatar, @RequestParam("username") String username) {
        return R.ok(authUerService.editAvatar(avatar, username));
    }
}
