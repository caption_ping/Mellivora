package com.captain.crew.common.core.config;

import cn.hutool.core.date.DatePattern;
import com.captain.crew.common.core.function.StringToLocalDateConverter;
import com.captain.crew.common.core.function.StringToLocalDateTimeConverter;
import com.captain.crew.common.core.function.StringToLocalTimeConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-04-23
 **/
@Configuration
@ConditionalOnClass(ObjectMapper.class)
@AutoConfigureBefore(JacksonAutoConfiguration.class)
public class JacksonConfig {

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer customizer() {
        return builder -> {
            builder.locale(Locale.CHINA);
            builder.timeZone(TimeZone.getTimeZone(ZoneId.systemDefault()));
            builder.simpleDateFormat(DatePattern.NORM_DATETIME_PATTERN);
            builder.modules(new CollectorJavaTimeModule());
        };
    }

    /**
     * LocalDate 转换器，用于转换 GET 请求时 RequestParam和 PathVariable 参数
     * 适用于 yyyy-MM-dd -> LocalDate
     */
    @Bean
    public StringToLocalDateConverter localDateConverter() {
        return source -> LocalDate.parse(source, DateTimeFormatter.ofPattern(DatePattern.NORM_DATE_PATTERN));
    }

    /**
     * LocalTime 转换器，用于转换 GET 请求时 RequestParam 和 PathVariable 参数
     * 适用于 HH:mm:ss -> LocalTime
     */
    @Bean
    public StringToLocalTimeConverter localTimeConverter() {
        return source -> LocalTime.parse(source, DateTimeFormatter.ofPattern(DatePattern.NORM_TIME_PATTERN));
    }

    /**
     * LocalDateTime 转换器，用于转换 GET 请求时 RequestParam 和 PathVariable 参数
     * 适用于 yyyy-MM-dd HH:mm:ss -> LocalDateTime
     */
    @Bean
    public StringToLocalDateTimeConverter localDateTimeConverter() {
        return source -> LocalDateTime.parse(source, DateTimeFormatter.ofPattern(DatePattern.NORM_DATETIME_PATTERN));
    }
}
