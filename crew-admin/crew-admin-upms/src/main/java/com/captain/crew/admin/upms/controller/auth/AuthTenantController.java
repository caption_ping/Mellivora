package com.captain.crew.admin.upms.controller.auth;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.captain.crew.admin.api.entity.domain.SysTenant;
import com.captain.crew.admin.upms.service.auth.SysTenantService;
import com.captain.crew.common.core.dto.R;
import com.captain.crew.common.security.annotation.PermitUrl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2020-12-24
 **/
@RestController
@RequestMapping("/tenant")
@AllArgsConstructor
@Api(value = "tenant", tags = "租户管理模块")
public class AuthTenantController {

    private final SysTenantService tenantService;

    /**
     * 分页查询
     *
     * @param page
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public R page(Page<SysTenant> page, SysTenant sysTenant) {
        return R.ok(tenantService.page(page, Wrappers.query(sysTenant)));
    }

    /**
     * 新增
     *
     * @param tenant
     * @return
     */
    @ApiOperation("新增")
    @PostMapping
    @PreAuthorize("@ums.hasPermission('sys_tenant_add')")
    public R<Void> save(@RequestBody @Valid SysTenant tenant, BindingResult result) {
        if (result.hasErrors()) {
            return R.failed(Objects.requireNonNull(result.getFieldError()).getDefaultMessage());
        }
        tenantService.saveTenant(tenant);
        return R.ok();
    }

    /**
     * 修改
     *
     * @param tenant
     * @return
     */
    @ApiOperation("修改")
    @PutMapping
    @PreAuthorize("@ums.hasPermission('sys_tenant_edit')")
    public R<Boolean> edit(@RequestBody @Valid SysTenant tenant, BindingResult result) {
        if (result.hasErrors()) {
            return R.failed(Objects.requireNonNull(result.getFieldError()).getDefaultMessage());
        }
        return R.ok(tenantService.update(Wrappers.<SysTenant>lambdaUpdate()
                .set(SysTenant::getBeginTime, tenant.getBeginTime())
                .set(SysTenant::getEndTime, tenant.getEndTime())
                .set(SysTenant::getStatus, tenant.getStatus())
                .set(SysTenant::getTenantName, tenant.getTenantName())
                .eq(SysTenant::getTenantCode, tenant.getTenantCode())));
    }

    /**
     * 获取所有的租户列表
     *
     * @return
     */
    @PermitUrl
    @ApiOperation("获取所有的租户列表")
    @GetMapping
    public R<List<SysTenant>> getList() {
        return R.ok(tenantService.list());
    }

    /**
     * 删除
     *
     * @param code
     * @return
     */
    @ApiOperation("删除")
    @DeleteMapping("/{code}")
    @PreAuthorize("@ums.hasPermission('sys_tenant_remove')")
    public R<Boolean> remove(@PathVariable String code) {
        return R.ok(tenantService.remove(Wrappers.<SysTenant>lambdaQuery()
                .eq(SysTenant::getTenantCode, code)));
    }
}
