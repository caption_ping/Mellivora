package com.captain.crew.common.mybatis.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-01-20
 **/
@Data
@Component
@ConfigurationProperties(prefix = "tenant")
public class CrewTenantTableProperty {

    /**
     * 是否启用多租户
     */
    private Boolean enable = Boolean.FALSE;

    /**
     * 多租户字段名称
     */
    private String fieldName = "tenant_id";

    /**
     * 多租户管理的表名
     */
    private List<String> tables;
}
