package com.captain.crew.admin.upms.controller.system;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.captain.crew.admin.api.entity.domain.SysLog;
import com.captain.crew.admin.upms.service.system.SysLogService;
import com.captain.crew.common.core.dto.R;
import com.captain.crew.common.log.annotation.SysLogger;
import com.captain.crew.common.security.annotation.PermitUrl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CAP_Crew
 * @date 2021-09-27
 **/
@RestController
@RequestMapping("/log")
@AllArgsConstructor
@Api(value = "log", tags = "日志管理模块")
public class SysLogController {

    private final SysLogService sysLogService;

    /**
     * 分页查询
     *
     * @param page 分页
     * @param log  查询条件
     * @return r
     */
    @PermitUrl
    @SysLogger("分页查询日志")
    @ApiOperation(value = "分页查询", notes = "分页查询系统日志")
    @GetMapping("/page")
    public R<?> page(Page<SysLog> page, SysLog log) {
        return R.ok(sysLogService.page(page, Wrappers.query(log).orderByDesc("id")));
    }

    @ApiOperation(value = "操作次数统计", notes = "获取日志中各种操作描述出现的次数")
    @GetMapping("/op_count")
    public R<?> operateCount() {
        return R.ok(sysLogService.operateCount());
    }

    @ApiOperation(value = "操作执行平均耗时", notes = "各接口执行的平均耗时")
    @GetMapping("/op_aver")
    public R<?> averageOpExecuteTime(){
        return R.ok(sysLogService.averageOfEachOpConsuming());
    }
}
