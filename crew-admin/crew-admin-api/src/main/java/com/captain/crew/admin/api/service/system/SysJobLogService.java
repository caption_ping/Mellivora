package com.captain.crew.admin.api.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.captain.crew.admin.api.entity.domain.SysJob;
import com.captain.crew.admin.api.entity.domain.SysJobLog;

/**
 * @author CAP_Crew
 * @date 2021-10-19
 **/
public interface SysJobLogService extends IService<SysJobLog> {
}
