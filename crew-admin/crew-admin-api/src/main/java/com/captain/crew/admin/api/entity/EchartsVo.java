package com.captain.crew.admin.api.entity;

import lombok.Data;

/**
 * @author CAP_Crew
 * @date 2021-12-04
 **/
@Data
public class EchartsVo {

    /**
     * 属性名
     */
    private String name;

    /**
     * 值
     */
    private String value;
}
