package com.captain.crew.admin.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.captain.crew.admin.api.entity.domain.SysJobLog;

/**
 * @author PC
 */
public interface SysJobLogMapper extends BaseMapper<SysJobLog> {
}
