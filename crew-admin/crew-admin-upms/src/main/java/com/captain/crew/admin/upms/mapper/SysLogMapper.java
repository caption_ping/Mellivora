package com.captain.crew.admin.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.captain.crew.admin.api.entity.EchartsVo;
import com.captain.crew.admin.api.entity.domain.SysLog;

import java.util.List;

/**
 * @author CAP_Crew
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

    /**
     * 各种操作次数的统计
     *
     * @return
     */
    List<EchartsVo> operateCount();

    /**
     * 指定天数至今的操作日志
     *
     * @param day
     * @return
     */
    List<SysLog> listInDays(int day);
}
