package com.captain.crew.common.log.service;

import com.captain.crew.common.log.core.CrewLog;

/**
 * @author renjunqi
 * @date 2023/2/3
 */
public interface CrewLogService {

    /**
     * 报存日志信息
     *
     * @param log
     */
    void saveLog(CrewLog log);
}
