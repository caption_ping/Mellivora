package com.captain.crew.common.quartz.job;

import com.captain.crew.admin.api.entity.domain.SysJob;
import com.captain.crew.common.core.util.UnroadSpringContext;
import com.captain.crew.common.quartz.component.QuartzInvokeFactory;
import com.captain.crew.common.quartz.constant.CrewQuartzConstant;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

/**
 * @author CAP_Crew
 * @date 2021-10-19
 **/
public class CrewTaskFactory implements Job {

    private final QuartzInvokeFactory quartzInvokeFactory;

    public CrewTaskFactory() {
        this.quartzInvokeFactory = UnroadSpringContext.getBean(QuartzInvokeFactory.class);
    }

    @Override
    public void execute(JobExecutionContext context) {
        SysJob sysJob = (SysJob) context.getMergedJobDataMap().get(CrewQuartzConstant.TASK_PARAM_KEY.getType().toString());
        quartzInvokeFactory.init(sysJob, context.getTrigger());
    }
}
