package com.captain.crew.admin.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.captain.crew.admin.api.entity.domain.SysJob;

/**
 * @author CAP_Crew
 */
public interface SysJobMapper extends BaseMapper<SysJob> {
}
