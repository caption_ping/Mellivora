package com.captain.crew.common.mybatis;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.captain.crew.common.security.util.SecurityUtils;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2020-11-30
 **/
@Component
public class CrewMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, "createTime", LocalDateTime.class, LocalDateTime.now());
        this.strictInsertFill(metaObject, "gmtCreate", LocalDateTime.class, LocalDateTime.now());
        // 最后修改人
        this.strictInsertFill(metaObject, "modifier", String.class, SecurityUtils.getUser().getUsername());

    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictUpdateFill(metaObject, "updateTime", LocalDateTime.class, LocalDateTime.now());
        this.strictUpdateFill(metaObject, "gmtModify", LocalDateTime.class, LocalDateTime.now());
        // 最后修改人
        this.strictUpdateFill(metaObject, "modifier", String.class, SecurityUtils.getUser().getUsername());
    }
}
