package com.captain.crew.common.mybatis.dataScope;

import cn.hutool.core.util.StrUtil;
import com.captain.crew.admin.api.entity.domain.SysDeptRelation;
import com.captain.crew.admin.api.entity.domain.SysRole;
import com.captain.crew.admin.api.service.auth.DataScopeService;
import com.captain.crew.common.core.constant.CommonConstant;
import com.captain.crew.common.core.util.UnroadSpringContext;
import com.captain.crew.common.mybatis.enums.DataScopeTypeEnum;
import com.captain.crew.common.security.service.SysUserAdmin;
import com.captain.crew.common.security.util.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author CAP_Crew
 * @Description
 * @date 2021-09-10
 **/
@Slf4j
public class CrewDefaultDataScopeHandle implements DataScopeHandle {

    @Override
    public Boolean getScope(List<Integer> deptId) {
        DataScopeService dataScopeService = UnroadSpringContext.getBean(DataScopeService.class);
        SysUserAdmin user = SecurityUtils.getUser();
        List<String> roleList = user.getPermissions().stream()
                .filter(permission -> StringUtils.startsWith(permission, CommonConstant.ROLE))
                .map(permission -> permission.split(StrUtil.UNDERLINE)[1])
                .collect(Collectors.toList());
        log.info("当前用户的角色ID有：{}", roleList);

        SysRole sysRole = dataScopeService.getRoleList(roleList).stream()
                .min(Comparator.comparing(SysRole::getDsScope)).get();
        Integer dsScope = sysRole.getDsScope();

        // 全部
        if (DataScopeTypeEnum.ALL.getType() == dsScope){
            return true;
        }
        //本级及子级
        if (DataScopeTypeEnum.OWN_CHILD_LEVEL.getType() == dsScope){
            List<Integer> deptIdList = dataScopeService.getDescendantList(user.getDeptId())
                    .stream().map(SysDeptRelation::getDescendant)
                    .collect(Collectors.toList());
            deptId.addAll(deptIdList);
        }
        // 只查询本级
        if (DataScopeTypeEnum.OWN_LEVEL.getType() == dsScope) {
            deptId.add(user.getDeptId());
        }
        return false;
    }
}
