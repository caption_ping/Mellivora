package com.captain.crew.admin.upms.service;

import com.captain.crew.common.security.service.SysUserAdmin;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/12/6 14:57
 */
public interface IndexService {
    /**
     * 处理认证通过后的登录业务
     * 返回用户的基本信息和权限信息
     *
     * @param adminUser
     * @return
     */
    String login(SysUserAdmin adminUser);
}
