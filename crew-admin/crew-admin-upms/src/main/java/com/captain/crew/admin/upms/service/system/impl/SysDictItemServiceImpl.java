package com.captain.crew.admin.upms.service.system.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.captain.crew.admin.api.entity.domain.SysDictItem;
import com.captain.crew.admin.upms.mapper.SysDictItemMapper;
import com.captain.crew.admin.upms.service.system.SysDictItemService;
import com.captain.crew.common.core.constant.CacheConstants;
import com.captain.crew.common.security.handle.BusinessException;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

/**
 * @author crewer
 */
@Service
public class SysDictItemServiceImpl extends ServiceImpl<SysDictItemMapper, SysDictItem> implements SysDictItemService {

    @Override
    @CacheEvict(value = CacheConstants.DICT_DETAILS, allEntries = true)
    public Boolean edit(SysDictItem dictItem) {
        SysDictItem item = this.getById(dictItem.getId());
        if (item == null) {
            throw new BusinessException("查询不到字典项信息");
        }
        return this.update(Wrappers.<SysDictItem>lambdaUpdate()
                .set(SysDictItem::getLabel, dictItem.getLabel())
                .set(SysDictItem::getValue, dictItem.getValue())
                .set(SysDictItem::getDescription, dictItem.getDescription())
                .eq(SysDictItem::getId, dictItem.getId()));
    }
}
