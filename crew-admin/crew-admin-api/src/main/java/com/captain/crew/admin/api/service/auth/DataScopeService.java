package com.captain.crew.admin.api.service.auth;

import com.captain.crew.admin.api.entity.domain.SysDeptRelation;
import com.captain.crew.admin.api.entity.domain.SysRole;

import java.util.List;

/**
 * @author CAP_Crew
 * @Description
 * @date 2021-09-10
 **/
public interface DataScopeService {

    /**
     * 给定包含 roleId 的集合，返回他们的详细信息
     *
     * @param roleId
     * @return
     */
    List<SysRole> getRoleList(List<String> roleId);

    /**
     * 给定一个部门ID，返回以它为父节点的所有的部门关系
     *
     * @param deptId
     * @return
     */
    List<SysDeptRelation> getDescendantList(Integer deptId);
}
