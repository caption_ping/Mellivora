package com.captain.crew.admin.api.service.auth;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.captain.crew.admin.api.entity.UserInfo;
import com.captain.crew.admin.api.entity.domain.SysUser;
import com.captain.crew.admin.api.entity.dto.EditPasswordInfo;
import com.captain.crew.common.core.dto.R;

/**
 * @Author: crewer
 * @Description:
 * @Date: 2020/8/28 10:36
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * 修改用户密码
     *
     * @param userInfo 用户密码信息
     * @return 通用的JSON响应对象
     */
    R<Void> editPassword(EditPasswordInfo userInfo);


    /**
     * 分页查询用户
     *
     * @param page 分页插件
     * @return 分页用户
     */
    IPage<SysUser> queryList(Page<SysUser> page, SysUser user);

    /**
     * 修改用户信息 根据提交的信息重置用户角色表
     *
     * @param sysUser 用户实体信息
     * @return bool
     */
    Boolean editUserInfo(SysUser sysUser);


    /**
     * 根据用户名检索用户信息
     *
     * @param username
     * @return
     */
    UserInfo findUserInfo(String username);

    /**
     * 添加用户
     *
     * @param user
     * @return
     */
    Boolean saveUser(SysUser user);
}
