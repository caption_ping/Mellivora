package com.captain.crew.common.quartz.event;

import com.captain.crew.admin.api.entity.domain.SysJob;
import com.captain.crew.common.quartz.util.TaskInvokeUtil;
import lombok.AllArgsConstructor;
import org.quartz.Trigger;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;

/**
 * @author PC
 * @date 2021/10/19
 */
@AllArgsConstructor
public class SysJobEventListener {

    private final TaskInvokeUtil taskInvokeUtil;

    @Async
    @Order
    @EventListener(SysJobEvent.class)
    public void doExecuteJobEvent(SysJobEvent sysJobEvent){
        SysJob sysJob = sysJobEvent.getSysJob();
        Trigger trigger = sysJobEvent.getTrigger();
        taskInvokeUtil.invokeMethod(sysJob, trigger);
    }
}
