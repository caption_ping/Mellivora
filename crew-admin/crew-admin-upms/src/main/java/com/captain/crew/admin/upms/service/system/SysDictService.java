package com.captain.crew.admin.upms.service.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.captain.crew.admin.api.entity.domain.SysDict;

/**
 * @author crewer
 */
public interface SysDictService extends IService<SysDict> {

    /**
     * 保存或者更新字典记录
     *
     * @param sysDict 字典实体类
     * @return 布尔
     */
    Boolean edit(SysDict sysDict);

    /**
     * 分页查询所有的字典信息
     *
     * @param page 分页
     * @return 所有的用户信息
     */
    IPage<SysDict> queryList(Page<SysDict> page);
}
