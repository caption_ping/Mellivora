package com.captain.crew.admin.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.captain.crew.admin.api.entity.domain.SysDept;
import com.captain.crew.common.core.vo.DeptTreeNode;
import com.captain.crew.common.core.vo.SelectTreeView;

import java.util.List;

/**
 * @author CAP_Crew
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

    /**
     * 查询部门信息
     *
     * @return TreeNodeView
     */
    List<DeptTreeNode> selectForTree();

    /**
     * 查询部门信息作为下拉选择树
     *
     * @return
     */
    List<SelectTreeView> selTreeVo();
}
