package com.captain.crew.common.quartz.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author CAP_Crew
 * @date 2021-10-19
 **/
@AllArgsConstructor
public enum CrewQuartzConstant {
    /**
     * 错失执行策略默认
     */
    MISFIRE_DEFAULT(0, "默认"),

    /**
     * 错失执行策略-立即执行错失任务
     */
    MISFIRE_IGNORE_MISFIRES(1, "立即执行错失任务"),

    /**
     * 错失执行策略-触发一次执行周期执行
     */
    MISFIRE_FIRE_AND_PROCEED(2, "触发一次执行周期执行"),

    /**
     * 错失执行策略-不触发执行周期执行
     */
    MISFIRE_DO_NOTHING(3, "不触发周期执行"),

    /**
     * JOB执行状态：0执行成功
     */
    JOB_LOG_STATUS_SUCCESS(0, "执行成功"),
    /**
     * JOB执行状态：1执行失败
     */
    JOB_LOG_STATUS_FAIL(1, "执行失败"),


    /**
     * JOB状态：1已发布
     */
    JOB_STATUS_RELEASE(1, "已发布"),
    /**
     * JOB状态：2运行中
     */
    JOB_STATUS_RUNNING(2, "运行中"),
    /**
     * JOB状态：3暂停
     */
    JOB_STATUS_NOT_RUNNING(3, "暂停"),
    /**
     * JOB状态：4删除
     */
    JOB_STATUS_DEL(4, "删除"),
    /**
     *
     */
    TASK_PARAM_KEY(9527, "任务参数标识");

    @Getter
    private final Integer type;

    @Getter
    private final String desc;
}
