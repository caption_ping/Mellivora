package com.captain.crew.admin.upms.controller.auth;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.captain.crew.admin.api.entity.domain.SysRole;
import com.captain.crew.admin.upms.service.auth.SysRoleMenuService;
import com.captain.crew.admin.upms.service.auth.SysRoleService;
import com.captain.crew.common.core.dto.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/22 11:36
 */
@Slf4j
@RestController
@RequestMapping("/role")
@AllArgsConstructor
@Api(value = "role", tags = "角色管理模块")
public class AuthRoleController {

    private final SysRoleService sysRoleService;
    private final SysRoleMenuService roleMenuService;

    /**
     * 返回所有的角色信息
     *
     * @return 所有的角色信息
     */
    @ApiOperation("分页查询所有角色信息")
    @GetMapping("/list")
    public R<List<SysRole>> list(Page<SysRole> page) {
        IPage<SysRole> list = sysRoleService.queryList(page);
        return R.ok((int) list.getTotal(), list.getRecords());
    }

    /**
     * 添加角色信息
     *
     * @param role 角色信息
     * @return 操作结果
     */
    @ApiOperation("添加角色信息")
    @PostMapping
    @PreAuthorize("@ums.hasPermission('sys_role_add')")
    public R add(@RequestBody SysRole role) {
        if (StringUtils.isBlank(role.getRoleName())) {
            return R.failed("请完整填写表单信息");
        }
        return R.ok(sysRoleService.add(role));
    }

    /**
     * 修改角色信息
     *
     * @param role 角色信息
     * @return 操作结果
     */
    @ApiOperation("修改角色信息")
    @PutMapping
    @PreAuthorize("@ums.hasPermission('sys_role_edit')")
    public R<Void> edit(@RequestBody SysRole role) {
        if (StringUtils.isBlank(role.getRoleName())) {
            return R.failed("请完整填写表单信息");
        }

        if (sysRoleService.edit(role)) {
            return R.ok();
        }
        return R.failed();
    }

    /**
     * 根据角色ID删除
     *
     * @param id 角色ID
     * @return 执行结果
     */
    @ApiOperation("根据角色ID删除")
    @ApiImplicitParam(name = "id", value = "角色ID")
    @DeleteMapping
    @PreAuthorize("@ums.hasPermission('sys_role_remove')")
    public R<?> remove(@RequestParam("id") Integer id) {
        return R.ok(sysRoleService.remove(Wrappers.<SysRole>lambdaQuery().eq(SysRole::getRoleId, id)));
    }

    /**
     * 更新角色菜单
     *
     * @param roleId  角色ID
     * @param menuIds 菜单ID拼成的字符串，每个id之间根据逗号分隔
     * @return success、false
     */
    @ApiOperation("更新角色菜单")
    @PutMapping("/{roleId}/menus")
    @PreAuthorize("@ums.hasPermission('sys_role_perm')")
    public R<Boolean> savePermission(@PathVariable("roleId") Integer roleId,
                                     @RequestParam(value = "menuId", required = false) String menuIds) {
        return R.ok(roleMenuService.saveRoleMenus(roleId, menuIds));
    }

    /**
     * 获取角色 ID 和 name 作为字典项
     *
     * @return
     */
    @ApiOperation("获取角色 ID 和 name 作为字典项")
    @GetMapping(value = "/dict")
    public R<List<Map<String, Object>>> getForDict() {
        List<Map<String, Object>> maps = sysRoleService.list()
                .stream()
                .map(role -> {
                    Map<String, Object> map = new HashMap<>(2);
                    map.put("label", role.getRoleName());
                    map.put("value", role.getRoleId());
                    return map;
                })
                .collect(Collectors.toList());
        return R.ok(maps);
    }
}
