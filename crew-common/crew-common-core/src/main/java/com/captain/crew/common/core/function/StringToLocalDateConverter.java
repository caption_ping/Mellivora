package com.captain.crew.common.core.function;

import org.springframework.core.convert.converter.Converter;

import java.time.LocalDate;

/**
 * 解决自定义 Converter<String, LocalDate> 时使用 lambda 表达式启动报错的问题
 * <p>
 * 参照：https://segmentfault.com/q/1010000021237008/a-1020000021240862
 *
 * @author CAP_Crew
 * @date 2021-07-06
 **/
public interface StringToLocalDateConverter extends Converter<String, LocalDate> {
}
