package com.captain.crew.admin.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.captain.crew.admin.api.entity.domain.SysUserRole;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2020-12-03
 **/
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
