package com.captain.crew.common.core.constant;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/25 19:07
 */
public interface CommonConstant {

    String TENANT_ID = "tenant_id";

    /**
     * 菜单根节点父辈ID
     */
    Integer MENU_TREE_ROOT_ID = -1;
    Integer ROUTER_MENU = 0;
    Integer ROUTER_BUTTON = 1;

    String ROLE = "ROLE_";

    /**
     * 用户账号 0正常/ 9锁定
     */
    String STATUS_NORMAL = "0";
    String STATUS_LOCK = "9";

    String BCRYPT = "{bcrypt}";

    String COMMA = ",";
    String SENSITIVE = "******";

    /**
     * redis 哈希键值
     * 用户菜单
     */
    String REDIS_ROLE_MENU = "role_menu";

    /**
     * minio bucket 名称
     * 图片：picture
     */
    String MINIO_BUCKET_PICTURE = "picture";

    /**
     * 0 菜单 1按钮
     */
    Integer PERMISSION_MENU = 0;
    Integer PERMISSION_BUTTON = 1;
}
