package com.captain.crew.admin.upms.service.system.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.captain.crew.admin.api.entity.domain.SysJob;
import com.captain.crew.admin.api.service.system.SysJobService;
import com.captain.crew.admin.upms.mapper.SysJobMapper;
import org.springframework.stereotype.Service;

/**
 * @author CAP_Crew
 */
@Service
public class SysJobServiceImpl extends ServiceImpl<SysJobMapper, SysJob> implements SysJobService {

}
