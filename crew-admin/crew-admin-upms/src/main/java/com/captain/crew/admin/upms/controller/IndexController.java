package com.captain.crew.admin.upms.controller;

import cn.hutool.core.lang.UUID;
import com.captain.crew.admin.api.entity.UserInfo;
import com.captain.crew.admin.api.entity.dto.LoginForm;
import com.captain.crew.admin.api.service.auth.SysUserService;
import com.captain.crew.admin.upms.service.IndexService;
import com.captain.crew.common.core.cache.RedisCache;
import com.captain.crew.common.core.dto.R;
import com.captain.crew.common.security.annotation.PermitUrl;
import com.captain.crew.common.security.service.SysUserAdmin;
import com.captain.crew.common.security.util.SecurityUtils;
import com.wf.captcha.SpecCaptcha;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/8/27 9:44
 */
@Slf4j
@RestController
@AllArgsConstructor
@Api(value = "index", tags = "认证管理模块")
public class IndexController {

    private final IndexService indexService;
    private final SysUserService userService;
    private final AuthenticationManager authenticationManager;
    private final RedisCache redisCache;

    /**
     * 请求验证码图片
     *
     * @return
     */
    @PermitUrl
    @ApiOperation("请求验证码图片")
    @GetMapping("/captcha")
    public R<Map<String, String>> captcha() {
        SpecCaptcha specCaptcha = new SpecCaptcha(130, 48, 5);
        String verCode = specCaptcha.text().toLowerCase();
        log.info("请求验证码: {}", verCode);
        String key = UUID.randomUUID().toString();
        // 存入redis并设置过期时间为 5 分钟
        redisCache.setCacheObject(key, verCode, 5, TimeUnit.MINUTES);
        // 将key和base64返回给前端
        Map<String, String> map = new HashMap<>(2);
        map.put("key", key);
        map.put("image", specCaptcha.toBase64());
        return R.ok(map);
    }

    /**
     * 登录认证
     *
     * @return 登录
     */
    @PermitUrl
    @ApiOperation("登录认证")
    @PostMapping(value = "/login")
    public R<String> login(@RequestBody LoginForm loginForm) {
        String verCode = redisCache.getCacheObject(loginForm.getKey());
        redisCache.deleteObject(loginForm.getKey());
        if (StringUtils.isBlank(verCode)) {
            return R.failed("验证码已过期");
        }

        if (!verCode.equals(loginForm.getCaptcha().trim().toLowerCase())) {
            return R.failed("验证码输入错误");
        }
        // 用户验证
        Authentication authentication;
        // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
        // 如果认证失败 会抛出 BadCredentialsException 异常
        // 该异常会被自定义异常处理器拦截处理
        authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginForm.getUsername(), loginForm.getPassword()));
        SysUserAdmin adminUser = (SysUserAdmin) authentication.getPrincipal();

        return R.ok(indexService.login(adminUser));
    }

    /**
     * 获取当前登陆人信息
     *
     * @return
     */
    @GetMapping("/user")
    public R<UserInfo> getUserInfo() {
        return R.ok(userService.findUserInfo(SecurityUtils.getUser().getUsername()));
    }
}
