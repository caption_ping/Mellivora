package com.captain.crew.admin.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.captain.crew.admin.api.entity.domain.SysRoleMenu;
import com.captain.crew.admin.api.entity.dto.MenuRouter;

import java.util.List;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/28 17:50
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

    /**
     * 查询指定角色的菜单
     *
     * @param roleId 角色ID
     * @return 菜单节点
     */
    List<MenuRouter> queryMenuByRoleId(Integer roleId);
}
