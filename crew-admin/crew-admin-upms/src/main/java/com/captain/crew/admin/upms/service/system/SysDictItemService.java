package com.captain.crew.admin.upms.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.captain.crew.admin.api.entity.domain.SysDictItem;
import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 * @author crewer
 */
public interface SysDictItemService extends IService<SysDictItem> {

    /**
     * 保存字典项信息，根据ID查询，若存在就更新；不存在直接新增
     *
     * @param dictItem 字典项实体类
     * @return 布尔值
     */
    Boolean edit(SysDictItem dictItem);
}
