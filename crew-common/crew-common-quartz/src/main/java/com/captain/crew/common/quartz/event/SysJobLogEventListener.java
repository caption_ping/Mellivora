package com.captain.crew.common.quartz.event;

import com.captain.crew.admin.api.entity.domain.SysJobLog;
import com.captain.crew.admin.api.service.system.SysJobLogService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;

/**
 * @author PC
 * @date 2021/10/16
 */
@AllArgsConstructor
public class SysJobLogEventListener {

    private final SysJobLogService sysJobLogService;

    @Async
    @Order
    @EventListener(SysJobLogEvent.class)
    public void doTaskLog(SysJobLogEvent sysJobLogEvent) {
        SysJobLog sysJobLog = sysJobLogEvent.getSysJobLog();
        sysJobLogService.save(sysJobLog);
    }
}
