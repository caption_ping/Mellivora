package com.captain.crew.common.log.core;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author renjunqi
 * @date 2023/2/3
 */
@Data
public class CrewLog implements Serializable {

    private static final long serialVersionUID = 2129082709025495317L;
    
    /**
     * 日志类型
     */
    private Integer type;

    /**
     * 类名
     */
    private String className;

    /**
     * 方法名
     */
    private String methodName;

    /**
     * 参数
     */
    private String params;

    /**
     * 执行时间
     */
    private Long execTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建人（访问者）
     */
    private String createBy;

    /**
     * 差UN关键时间爱你
     */
    private LocalDateTime createDate;
}
