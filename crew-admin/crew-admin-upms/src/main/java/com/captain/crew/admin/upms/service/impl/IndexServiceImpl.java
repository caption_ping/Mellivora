package com.captain.crew.admin.upms.service.impl;

import com.captain.crew.admin.upms.service.IndexService;
import com.captain.crew.common.security.service.SysUserAdmin;
import com.captain.crew.common.security.service.TokenService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/12/6 14:57
 */
@Service
@Slf4j
@AllArgsConstructor
public class IndexServiceImpl implements IndexService {

    private final TokenService tokenService;

    @Override
    public String login(SysUserAdmin adminUser) {
        return tokenService.createToken(adminUser);
    }
}
