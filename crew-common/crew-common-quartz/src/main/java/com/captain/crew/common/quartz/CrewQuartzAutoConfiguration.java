package com.captain.crew.common.quartz;

import com.captain.crew.admin.api.service.system.SysJobLogService;
import com.captain.crew.admin.api.service.system.SysJobService;
import com.captain.crew.common.quartz.component.*;
import com.captain.crew.common.quartz.event.SysJobEventListener;
import com.captain.crew.common.quartz.event.SysJobLogEventListener;
import com.captain.crew.common.quartz.util.TaskInvokeUtil;
import com.captain.crew.common.quartz.util.SysJobManage;
import org.quartz.Scheduler;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

/**
 * @author PC
 * @date 2021/10/19
 */
public class CrewQuartzAutoConfiguration {

    @Bean
    public SysJobEventListener sysJobEventListener(TaskInvokeUtil taskInvokeUtil) {
        return new SysJobEventListener(taskInvokeUtil);
    }

    @Bean
    public SysJobLogEventListener sysJobLogEventListener(SysJobLogService sysJobLogService) {
        return new SysJobLogEventListener(sysJobLogService);
    }

    @Bean
    public TaskInvokeHandler javaClassInvokeHandler() {
        return new JavaClassTaskInvokeHandler();
    }

    @Bean
    public TaskInvokeHandler restInvokeHandler() {
        return new RestTaskInvokeHandler();
    }

    @Bean
    public TaskInvokeHandler springBeanInvokeHandler() {
        return new SpringBeanTaskInvokeHandler();
    }

    @Bean
    public TaskInvokeUtil taskInvokeUtil(SysJobService sysJobService, ApplicationEventPublisher applicationEventPublisher) {
        return new TaskInvokeUtil(sysJobService, applicationEventPublisher);
    }

    @Bean
    public SysJobManage sysJobManage(Scheduler scheduler) {
        return new SysJobManage(scheduler);
    }

    @Bean
    public QuartzInvokeFactory quartzInvokeFactory(ApplicationEventPublisher eventPublisher) {
        return new QuartzInvokeFactory(eventPublisher);
    }

    @Bean
    @ConditionalOnClass(SchedulerFactoryBean.class)
    public Scheduler scheduler(SchedulerFactoryBean schedulerFactoryBean) {
        return schedulerFactoryBean.getScheduler();
    }
}
