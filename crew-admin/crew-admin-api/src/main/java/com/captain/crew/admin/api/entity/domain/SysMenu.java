package com.captain.crew.admin.api.entity.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

/**
 * 菜单权限表
 *
 * @author crewer
 */
@Data
public class SysMenu {
    /**
     * 菜单ID
     */
    @TableId
    private Integer menuId;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 权限
     */
    private String permission;

    /**
     * 访问路径
     */
    private String path;

    /**
     * 父菜单ID
     */
    private Integer parentId;

    /**
     * 图标
     */
    private String icon;

    /**
     * 排序值
     */
    private Integer sort;

    /**
     * 组件类型， 0菜单、1按钮
     */
    private String type;

    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer delFlag;
}
