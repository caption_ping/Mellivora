package com.captain.crew.admin.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.captain.crew.admin.api.entity.domain.SysUser;
import com.captain.crew.common.mybatis.dataScope.DataScope;
import org.apache.ibatis.annotations.Param;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/8/28 10:56
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 通过用户名查询用户信息
     *
     * @param username 用户名
     * @return 用户实体类
     */
    SysUser getByUsername(String username);

    /**
     * 分页查询所有用户
     *
     * @param user      动态查询
     * @param dataScope 进行权限过滤
     * @param page      分页插件
     * @return 分页用户
     */
    IPage<SysUser> queryList(Page<?> page, @Param("user") SysUser user, DataScope dataScope);
}
