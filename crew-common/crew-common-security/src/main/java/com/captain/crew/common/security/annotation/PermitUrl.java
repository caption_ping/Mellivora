package com.captain.crew.common.security.annotation;

import java.lang.annotation.*;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-05-22
 **/
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PermitUrl {
}
