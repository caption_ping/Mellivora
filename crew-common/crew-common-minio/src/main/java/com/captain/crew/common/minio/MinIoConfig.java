package com.captain.crew.common.minio;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2020-12-09
 **/
@Data
@Component
@ConfigurationProperties(prefix = "minio")
public class MinIoConfig {

    /**
     * minio 服务的 URL
     * 服务地址 http://ip:port
     */
    private String url;
    /**
     * 用户名
     */
    private String accessKey;
    /**
     * 密码
     */
    private String secretKey;
    /**
     * 是否启用 https
     */
    private Boolean secure = Boolean.TRUE;
}
