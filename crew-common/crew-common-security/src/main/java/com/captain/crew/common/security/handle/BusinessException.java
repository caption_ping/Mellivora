package com.captain.crew.common.security.handle;

import com.captain.crew.common.core.enums.ResponseCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author: crewer
 * @Description: 全局业务异常
 * @Date: 2020/10/21 13:40
 */
public class BusinessException extends RuntimeException{

    private static final long serialVersionUID = -7737873757475120488L;

    @Getter
    @Setter
    private Integer code;

    public BusinessException() {}

    public BusinessException(String message) {
        super(message);
        this.code = -1;
    }

    public BusinessException(ResponseCode status) {
        super(status.getMsg());
        this.code = status.getCode();
    }
}
