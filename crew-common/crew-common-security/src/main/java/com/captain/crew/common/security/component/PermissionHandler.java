package com.captain.crew.common.security.component;

import com.captain.crew.common.security.service.SysUserAdmin;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-01-21
 **/
@Slf4j
@Component("ums")
public class PermissionHandler {

    /**
     * 判断接口调用者是否有某个指定的角色角色
     *
     * @param roles 角色集合
     * @return 是否允许访问
     */
    public Boolean hasRoles(List<String> roles) {
        log.info(roles.toString());
        return false;
    }

    /**
     * 判断接口调用者是否具有指定的角色权限
     *
     * @param role 角色名
     * @return 是否允许访问
     */
    public Boolean hasRole(String role) {
        return false;
    }

    /**
     * 判断接口调用者是否具有该权限字符串
     * 从 Security 容器里拿到当前登陆人信息，从中拿到我们自己维护的 permissions
     * 从中判断权限字符串集合中是否包含所需要的权限
     *
     * @param premStr 权限字符串
     * @return 是否允许访问
     */
    public Boolean hasPermission(String premStr) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return false;
        }
        SysUserAdmin userAdmin = (SysUserAdmin) authentication.getPrincipal();
        List<String> permissions = userAdmin.getPermissions();
        return permissions.contains(premStr);
    }
}
