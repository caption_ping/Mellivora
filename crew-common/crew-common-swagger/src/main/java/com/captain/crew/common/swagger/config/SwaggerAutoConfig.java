package com.captain.crew.common.swagger.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-03-15
 **/
@Configuration
@EnableSwagger2WebMvc
public class SwaggerAutoConfig {

    @Bean
    @ConditionalOnMissingBean
    public SwaggerProperties swaggerProperties() {
        return new SwaggerProperties();
    }

    @Bean
    public Docket defaultApi(SwaggerProperties swaggerProperties) {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .description(swaggerProperties.getDescription())
                        .termsOfServiceUrl(swaggerProperties.getTermsOfServiceUrl())
                        .contact(new Contact(swaggerProperties.getContact().getName(),swaggerProperties.getContact().getUrl(), swaggerProperties.getContact().getEmail()))
                        .version(swaggerProperties.getVersion())
                        .license(swaggerProperties.getLicense())
                        .licenseUrl(swaggerProperties.getLicenseUrl())
                        .title(swaggerProperties.getTitle())
                        .build())
                .select()
                // 指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage(swaggerProperties.getBasePackage()))
                .paths(PathSelectors.any())
                .build();
    }
}
