package com.captain.crew.admin.api.entity.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author crewer
 */
@Data
@TableName(value = "sys_dict_item")
public class SysDictItem implements Serializable {

    private static final long serialVersionUID = 6825871736132242668L;

    /**
     * 主键自增
     */
    @TableId
    private Long id;

    /**
     * 字典ID关联字典表
     */
    @TableField(value = "dict_id")
    private Long dictId;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 字典项标签名
     */
    @TableField(value = "label")
    private String label;

    /**
     * 字典项值
     */
    @TableField(value = "`value`")
    private Integer value;

    /**
     * 描述
     */
    @TableField(value = "description")
    private String description;

    /**
     * 删除标记
     */
    @TableField(value = "del_flag")
    @TableLogic
    private Integer delFlag;
}
