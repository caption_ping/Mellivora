package com.captain.crew.common.quartz.event;

import com.captain.crew.admin.api.entity.domain.SysJob;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.quartz.Trigger;

/**
 * @author PC
 * @date 2021/10/19
 */
@Data
@AllArgsConstructor
public class SysJobEvent {

    private SysJob sysJob;

    private Trigger trigger;
}
