package com.captain.crew.admin.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.captain.crew.admin.api.entity.domain.SysFiles;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2020-12-10
 **/
public interface SysFilesMapper extends BaseMapper<SysFiles> {

    /**
     * 分页查询
     *
     * @param page
     * @return
     */
    IPage<SysFiles> getPage(Page<?> page);
}
