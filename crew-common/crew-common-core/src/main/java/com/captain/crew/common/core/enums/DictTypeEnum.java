package com.captain.crew.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author CAP_Crew
 * @Description
 * @date 2021-09-10
 **/
@AllArgsConstructor
public enum DictTypeEnum {

    /**
     * 系统内置，不允许修改删除
     */
    SYSTEM(0, "系统内置"),
    /**
     * 业务类型
     */
    BIZ(1, "业务类型");

    @Getter
    private final Integer type;

    @Getter
    private final String description;
}
