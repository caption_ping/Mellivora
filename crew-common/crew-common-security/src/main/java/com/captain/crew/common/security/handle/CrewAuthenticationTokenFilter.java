package com.captain.crew.common.security.handle;

import com.captain.crew.common.security.service.SysUserAdmin;
import com.captain.crew.common.security.service.TokenService;
import com.captain.crew.common.security.util.SecurityUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-01-22
 **/
@Component
@Slf4j
@AllArgsConstructor
public class CrewAuthenticationTokenFilter extends OncePerRequestFilter {

    private final TokenService tokenService;

    /**
     * 自定义请求拦截器， 从请求头中获取 token
     * 用token换取redis 中的信息构造 securityContext
     * securityContext 构造失败会自动进入认证失败处理器
     *
     * @param request
     * @param response
     * @param chain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        SysUserAdmin loginUser = tokenService.getLoginUser(request);
        if (!ObjectUtils.isEmpty(loginUser) && ObjectUtils.isEmpty(SecurityContextHolder.getContext().getAuthentication())) {
            tokenService.verifyToken(loginUser);
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUser, null, loginUser.getAuthorities());
            authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }
        chain.doFilter(request, response);
    }
}
