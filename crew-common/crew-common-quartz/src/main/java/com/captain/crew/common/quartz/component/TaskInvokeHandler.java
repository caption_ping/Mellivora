package com.captain.crew.common.quartz.component;

import com.captain.crew.admin.api.entity.domain.SysJob;
import com.captain.crew.common.quartz.exception.TaskException;

/**
 * @author CAP_Crew
 * @date 2021-10-19
 **/
public interface TaskInvokeHandler {

    /**
     * 任务的实际执行方法，由各个不同类型的任务处理器实现
     *
     * @param sysJob 任务详情
     * @throws TaskException 任务处理异常
     */
    void execute(SysJob sysJob) throws TaskException;

    /**
     * 该处理器是否支持
     * @param jobType 任务类型
     * @return 是否支持
     */
    boolean support(String jobType);
}
