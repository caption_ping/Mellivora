package com.captain.crew.common.core.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-01-20
 **/
@Getter
@RequiredArgsConstructor
public enum LogTypeEnum {

    /**
     * 正常日志类型
     */
    NORMAL(0, "正常日志"),

    /**
     * 错误日志类型
     */
    ERROR(9, "错误日志");

    /**
     * 类型
     */
    private final Integer type;

    /**
     * 描述
     */
    private final String description;
}
