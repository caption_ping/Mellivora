package com.captain.crew.common.security.util;

import cn.hutool.core.util.StrUtil;

import com.captain.crew.common.core.constant.CommonConstant;
import com.captain.crew.common.security.service.SysUserAdmin;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/10/20 16:19
 */
@UtilityClass
public class SecurityUtils {

    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * 获取用户
     *
     * @param authentication
     * @return PigxUser
     * <p>
     */
    public SysUserAdmin getUser(Authentication authentication) {
        Object principal = authentication.getPrincipal();
        if (principal instanceof SysUserAdmin) {
            return (SysUserAdmin) principal;
        }
        return null;
    }

    /**
     * 获取用户
     */
    public SysUserAdmin getUser() {
        Authentication authentication = getAuthentication();
        return getUser(authentication);
    }

    /**
     * 获取用户角色信息
     *
     * @return 角色集合
     */
    public List<Integer> getRoles() {
        List<String> permissions = getUser().getPermissions();
        List<Integer> roleIds = new ArrayList<>();
        permissions.stream()
                .filter(granted -> StrUtil.startWith(granted, CommonConstant.ROLE))
                .forEach(granted -> {
                    String id = StrUtil.removePrefix(granted, CommonConstant.ROLE);
                    roleIds.add(Integer.parseInt(id));
                });
        return roleIds;
    }
}
