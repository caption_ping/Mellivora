package com.captain.crew.admin.api.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.captain.crew.admin.api.entity.domain.SysJob;

/**
 * @author CAP_Crew
 * @date 2021-10-19
 **/
public interface SysJobService extends IService<SysJob> {
}
