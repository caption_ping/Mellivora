package com.captain.crew.common.log.annotation;

import java.lang.annotation.*;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-01-20
 **/
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SysLogger {

    String value() default "";
}
