package com.captain.crew.admin.upms.service.auth;

import com.baomidou.mybatisplus.extension.service.IService;
import com.captain.crew.admin.api.entity.domain.SysUserRole;

/**
 * @author CAP_Crew
 * @Description
 * @date 2021-09-10
 **/
public interface SysUserRoleService extends IService<SysUserRole> {
}
