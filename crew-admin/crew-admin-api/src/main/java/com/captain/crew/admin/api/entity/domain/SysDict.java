package com.captain.crew.admin.api.entity.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author crewer
 */
@Data
@TableName(value = "sys_dict")
public class SysDict {
    /**
     * 主键自增
     */
    @TableId
    private Long id;

    /**
     * 字典类型
     */
    @TableField(value = "dict_type")
    private String dictType;

    /**
     * 字典描述
     */
    @TableField(value = "description")
    private String description;

    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 删除标记 0-正常 1-删除
     */
    @TableField(value = "del_flag")
    @TableLogic
    private Integer delFlag;

    /**
     * 是否为系统字典 0-是 1-不是
     */
    @TableField(value = "`system`")
    private Integer system;
}
