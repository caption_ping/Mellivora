package com.captain.crew.admin.upms.service.auth.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.captain.crew.admin.api.entity.domain.SysRole;
import com.captain.crew.admin.upms.mapper.SysRoleMapper;
import com.captain.crew.admin.upms.service.auth.SysRoleService;
import com.captain.crew.common.security.handle.BusinessException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/22 11:55
 */
@Service
@AllArgsConstructor
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    private final SysRoleMapper roleMapper;

    @Override
    public IPage<SysRole> queryList(Page<SysRole> page) {
        return roleMapper.queryList(page);
    }

    @Override
    public boolean add(SysRole role) {
        if (isExist(role)) {
            throw new BusinessException("角色或角色标识已存在");
        }
        return this.save(role);
    }

    @Override
    public boolean edit(SysRole role) {
        if (isExist(role)) {
            return this.update(Wrappers.<SysRole>lambdaUpdate()
                    .eq(SysRole::getRoleId, role.getRoleId())
                    .set(SysRole::getRoleName, role.getRoleName())
                    .set(SysRole::getDsScope, role.getDsScope())
                    .set(SysRole::getRoleDesc, role.getRoleDesc())
                    .set(SysRole::getRoleCode, role.getRoleCode()));
        }
        return false;
    }

    @Override
    public List<SysRole> findRolesByUserId(Integer userId) {
        return baseMapper.findRoleByUserId(userId);
    }

    /**
     * 判断角色是否存在
     *
     * @param role
     * @return
     */
    private boolean isExist(SysRole role) {
        SysRole entity = this.getOne(Wrappers
                .<SysRole>lambdaQuery()
                .eq(SysRole::getRoleId, role.getRoleId())
                .or()
                .eq(SysRole::getRoleCode, role.getRoleCode()));
        return entity != null;
    }
}
