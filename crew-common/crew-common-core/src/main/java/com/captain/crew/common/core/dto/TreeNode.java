package com.captain.crew.common.core.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/25 19:20
 */
@Data
public class TreeNode {

    /**
     * 节点ID
     */
    protected Integer id;

    /**
     * 父级节点Id
     */
    protected Integer parentId;

    /**
     * 孩子节点列表
     */
    protected List<TreeNode> children = new ArrayList<>();

    public void add(TreeNode node){
        children.add(node);
    }
}
