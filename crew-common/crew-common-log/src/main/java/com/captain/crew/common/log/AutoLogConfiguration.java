package com.captain.crew.common.log;

import com.captain.crew.common.log.event.SysLogEventListener;
import com.captain.crew.common.log.service.CrewLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-04-26
 **/
@Slf4j
public class AutoLogConfiguration {

    @Bean
    public SysLogEventListener getSysLogEvent(CrewLogService logService) {
        return new SysLogEventListener(logService);
    }

    @Bean
    public CrewLogHandler getLogHandle(ApplicationEventPublisher publisher) {
        return new CrewLogHandler(publisher);
    }

    @Bean
    @ConditionalOnMissingBean
    public CrewLogService logService() {
        return opsLog -> log.info("【{}】", opsLog);
    }
}
