package com.captain.crew.admin.api.entity.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2020-12-10
 **/
@Data
public class SysFiles {

    @TableId
    private Integer id;
    /**
     * 主键ID
     */
    private String bucketName;
    /**
     * 桶名称
     */
    private String fileName;
    /**
     * 文件名
     */
    private Long size;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime gmtCreate;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime gmtModify;
    /**
     * 租户ID
     */
    private Integer tenantId;
    /**
     * 修改人
     */
    @TableField(fill = FieldFill.UPDATE)
    private String modifier;

    @TableLogic
    private Integer delFlag;
}
