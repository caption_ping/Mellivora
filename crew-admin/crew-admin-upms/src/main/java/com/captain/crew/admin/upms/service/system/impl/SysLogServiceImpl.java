package com.captain.crew.admin.upms.service.system.impl;

import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.captain.crew.admin.api.entity.EchartsVo;
import com.captain.crew.admin.api.entity.domain.SysLog;
import com.captain.crew.admin.upms.service.system.SysLogService;
import com.captain.crew.admin.upms.mapper.SysLogMapper;
import com.captain.crew.common.log.core.CrewLog;
import com.captain.crew.common.log.service.CrewLogService;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author CAP_Crew
 */
@Service
@AllArgsConstructor
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService, CrewLogService {

    private final SysLogMapper sysLogMapper;

    @Override
    public List<EchartsVo> operateCount() {
        return sysLogMapper.operateCount();
    }

    @Override
    public List<EchartsVo> averageOfEachOpConsuming() {
        List<SysLog> logs = sysLogMapper.listInDays(30);
        Set<String> remarks = logs.stream().map(SysLog::getRemark).collect(Collectors.toSet());
        Map<String, Long> tempMap = new HashMap<>(remarks.size() * 2);
        logs.forEach(log -> {
            Long ms = tempMap.get(log.getRemark());
            if (ms == null) {
                tempMap.put(log.getRemark(), log.getExecTime());
                tempMap.put(log.getRemark() + "times", 1L);
            } else {
                ms += log.getExecTime();
                tempMap.put(log.getRemark(), ms);
                Long times = tempMap.get(log.getRemark() + "times");
                tempMap.put(log.getRemark() + "times", times + 1);
            }
        });
        return remarks.stream().map(remark -> {
            EchartsVo vo = new EchartsVo();
            Long msTotal = tempMap.get(remark);
            Long times = tempMap.get(remark + "times");
            vo.setName(remark);
            vo.setValue(NumberUtil.div(msTotal, times, 2).toString());
            return vo;
        }).collect(Collectors.toList());
    }

    @Override
    public void saveLog(CrewLog log) {
        SysLog opsLog = mapToSysLog(log);
        save(opsLog);
    }

    private SysLog mapToSysLog(CrewLog log) {
        SysLog opsLog = new SysLog();
        BeanUtils.copyProperties(log, opsLog);
        return opsLog;
    }
}
