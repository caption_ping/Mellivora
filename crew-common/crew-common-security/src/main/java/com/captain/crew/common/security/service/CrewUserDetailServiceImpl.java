package com.captain.crew.common.security.service;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.captain.crew.admin.api.entity.UserInfo;
import com.captain.crew.admin.api.entity.domain.SysUser;
import com.captain.crew.admin.api.service.auth.SysUserService;
import com.captain.crew.common.core.constant.CommonConstant;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/21 10:38
 */
@Service
@AllArgsConstructor
public class CrewUserDetailServiceImpl implements UserDetailsService {

    private final SysUserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserInfo info = userService.findUserInfo(username);
        if (ObjectUtils.isEmpty(info)) {
            throw new UsernameNotFoundException("unknown user");
        }

        Set<String> dbAuthsSet = new HashSet<>();
        if (ArrayUtil.isNotEmpty(info.getRoles())) {
            // 获取角色
            Arrays.stream(info.getRoles()).forEach(roleId -> dbAuthsSet.add(CommonConstant.ROLE + roleId));
            // 获取资源
            dbAuthsSet.addAll(Arrays.asList(info.getPermissions()));

        }
        List<String> authorities = Stream.of(dbAuthsSet.toArray(new String[0])).collect(Collectors.toList());
        SysUser user = info.getUser();
        boolean enabled = StrUtil.equals(user.getLockFlag(), CommonConstant.STATUS_NORMAL);
        // 构造security用户
        return new SysUserAdmin(user.getUserId(), user.getDeptId(), user.getTenantId(), user.getUsername(), user.getNickname(), user.getPassword(), enabled,
                true, true, !CommonConstant.STATUS_LOCK.equals(user.getLockFlag()), authorities);
    }
}
