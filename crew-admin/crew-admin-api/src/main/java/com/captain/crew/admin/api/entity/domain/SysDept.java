package com.captain.crew.admin.api.entity.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * sys_dept
 *
 * @author
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysDept implements Serializable {
    /**
     * 部门ID
     */
    @TableId("dept_id")
    private Integer deptId;

    /**
     * 父级部门ID
     */
    private Integer parentId;

    /**
     * 部门名称
     */
    @NotBlank(message = "部门名称不能为空")
    private String deptName;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime modifyTime;

    /**
     * 删除标记
     */
    @TableLogic
    private Integer delFlag;

    private static final long serialVersionUID = 1L;
}
