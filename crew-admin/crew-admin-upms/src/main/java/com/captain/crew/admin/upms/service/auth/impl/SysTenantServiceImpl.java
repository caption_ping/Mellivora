package com.captain.crew.admin.upms.service.auth.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.captain.crew.admin.api.entity.domain.*;
import com.captain.crew.admin.api.service.auth.SysUserService;
import com.captain.crew.admin.upms.mapper.SysTenantMapper;
import com.captain.crew.admin.upms.service.auth.*;
import com.captain.crew.admin.upms.service.system.SysDictItemService;
import com.captain.crew.admin.upms.service.system.SysDictService;
import com.captain.crew.common.core.enums.DictTypeEnum;
import com.captain.crew.common.mybatis.tenant.TenantContextHolder;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2020-12-24
 **/
@Service
@AllArgsConstructor
public class SysTenantServiceImpl extends ServiceImpl<SysTenantMapper, SysTenant> implements SysTenantService {
    
    private final SysUserService userService;
    private final SysUserRoleService userRoleService;
    private final SysRoleService roleService;
    private final SysRoleMenuService roleMenuService;
    private final SysDictService dictService;
    private final SysDictItemService dictItemService;
    private final AuthDeptService deptService;
    private final SysDeptRelationService deptRelationService;
    private final SysMenuService menuService;
    private final BCryptPasswordEncoder passwordEncoder;

    /**
     * 保存租户
     * <p>
     * 1. 保存租户
     * 2. 初始化权限相关表
     * - sys_user
     * - sys_role
     * - sys_user_role
     * - sys_role_menu
     * - sys_dict
     * - sys_dict_item
     *
     * @param sysTenant 租户实体
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveTenant(SysTenant sysTenant) {
        this.save(sysTenant);

        // 系统内置字典
        List<SysDict> sysDictList = dictService.list(Wrappers.<SysDict>lambdaQuery()
                .eq(SysDict::getSystem, DictTypeEnum.SYSTEM.getType()))
                .stream()
                .map(dict -> {
                    SysDict sysDict = new SysDict();
                    BeanUtils.copyProperties(dict, sysDict, "tenantId");
                    return sysDict;
                }).collect(Collectors.toList());
        // 系统内置字典项
        List<Long> dictIdList = sysDictList.stream().map(SysDict::getId).collect(Collectors.toList());
        List<SysDictItem> dictItemList = dictItemService.list(Wrappers.<SysDictItem>lambdaQuery()
                .in(SysDictItem::getDictId, dictIdList))
                .stream()
                .map(dictItem -> {
                    SysDictItem sysDictItem = new SysDictItem();
                    BeanUtils.copyProperties(dictItem, sysDictItem, "tenantId");
                    return sysDictItem;
                }).collect(Collectors.toList());

        TenantContextHolder.setTenantId(sysTenant.getId());

        // 默认部门
        SysDept defaultDept = makeDefaultDept();
        deptService.save(defaultDept);
        // 维护部门关系
        deptRelationService.insertDeptRelation(defaultDept);

        // 默认角色
        SysRole defaultRole = makeDefaultRole();
        roleService.save(defaultRole);

        // 默认用户
        SysUser defaultUser = makeDefaultUser(defaultDept.getDeptId());
        userService.save(defaultUser);

        // 角色和用户对应关系
        SysUserRole userRole = new SysUserRole();
        userRole.setRoleId(defaultRole.getRoleId());
        userRole.setUserId(defaultUser.getUserId());
        userRoleService.save(userRole);

        // 角色与菜单的对应关系
        List<SysRoleMenu> roleMenuList = menuService.list()
                .stream()
                .map(menu -> {
                    SysRoleMenu roleMenu = new SysRoleMenu();
                    roleMenu.setMenuId(menu.getMenuId());
                    roleMenu.setRoleId(defaultRole.getRoleId());
                    return roleMenu;
                }).collect(Collectors.toList());
        roleMenuService.saveBatch(roleMenuList);

        // 植入系统字典
        dictService.saveBatch(sysDictList);
        // 处理字典项最新关联的字典ID
        List<SysDictItem> dictItems = sysDictList.stream().flatMap(dict -> dictItemList.stream()
                .filter(item -> item.getDictType().equals(dict.getDictType()))
                .peek(item -> item.setDictId(dict.getId())))
                .collect(Collectors.toList());
        dictItemService.saveBatch(dictItems);
    }

    private SysDept makeDefaultDept() {
        SysDept defaultDept = new SysDept();
        defaultDept.setDeptName("defaultDept");
        defaultDept.setParentId(-1);
        return defaultDept;
    }

    private SysUser makeDefaultUser(Integer deptId) {
        SysUser defaultUser = new SysUser();
        defaultUser.setUsername("admin");
        defaultUser.setPassword(passwordEncoder.encode("123456"));
        defaultUser.setDeptId(deptId);
        return defaultUser;
    }

    private SysRole makeDefaultRole() {
        SysRole defaultRole = new SysRole();
        defaultRole.setRoleName("defaultRoleName");
        defaultRole.setRoleCode("defaultRoleCode");
        return defaultRole;
    }
}
