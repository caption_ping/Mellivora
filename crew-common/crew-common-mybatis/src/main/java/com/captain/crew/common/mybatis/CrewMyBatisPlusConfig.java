package com.captain.crew.common.mybatis;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.TenantLineInnerInterceptor;
import com.captain.crew.common.mybatis.dataScope.CrewDataScopeInterceptor;
import com.captain.crew.common.mybatis.dataScope.CrewDefaultDataScopeHandle;
import com.captain.crew.common.mybatis.dataScope.DataScopeHandle;
import com.captain.crew.common.mybatis.property.CrewTenantTableProperty;
import com.captain.crew.common.mybatis.tenant.CrewTenantHandler;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/23 16:30
 */
@Configuration
@ConditionalOnBean(DataSource.class)
@AutoConfigureAfter(DataSourceAutoConfiguration.class)
@EnableConfigurationProperties(CrewTenantTableProperty.class)
public class CrewMyBatisPlusConfig {

    @Bean
    @ConditionalOnMissingBean
    public CrewTenantHandler crewTenantHandler() {
        return new CrewTenantHandler();
    }

    /**
     * MyBatis插件
     * 开启 Mybatis-plus 分页插件
     * 开启 MyBatis-plus 多租户插件
     *
     * @return
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(CrewTenantTableProperty tenantTableProperty, CrewTenantHandler handler) {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        // 按需加载多租户插件
        if (tenantTableProperty.getEnable()) {
            interceptor.addInnerInterceptor(new TenantLineInnerInterceptor(handler));
        }
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }

    @Bean
    public ConfigurationCustomizer configurationCustomizer() {
        return configuration -> configuration.setUseDeprecatedExecutor(false);
    }

    @Bean
    @ConditionalOnMissingBean
    public DataScopeHandle dataScopeHandle() {
        return new CrewDefaultDataScopeHandle();
    }

    /**
     * 开启自定义数据权限过滤插件
     *
     * @return CrewDataScopeInterceptor
     */
    @Bean
    @ConditionalOnMissingBean
    public CrewDataScopeInterceptor crewDataScopeInterceptor(DataScopeHandle dataScopeHandle) {
        return new CrewDataScopeInterceptor(dataScopeHandle);
    }
}
