package com.captain.crew.admin.api.entity.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author CAP_Crew
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sys_log")
public class SysLog {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "type")
    private Integer type;

    @TableField(value = "class_name")
    private String className;

    @TableField(value = "method_name")
    private String methodName;

    @TableField(value = "params")
    private String params;

    @TableField(value = "exec_time")
    private Long execTime;

    @TableField(value = "remark")
    private String remark;

    @TableField(value = "create_by")
    private String createBy;

    @TableField(value = "create_date")
    private LocalDateTime createDate;
}
