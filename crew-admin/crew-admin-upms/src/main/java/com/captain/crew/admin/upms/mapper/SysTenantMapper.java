package com.captain.crew.admin.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.captain.crew.admin.api.entity.domain.SysTenant;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/10/9 10:36
 */
public interface SysTenantMapper extends BaseMapper<SysTenant> {

}
