package com.captain.crew.common.swagger.annotation;

import com.captain.crew.common.swagger.config.SwaggerAutoConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-03-15
 **/
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({SwaggerAutoConfig.class})
public @interface EnableCrewSwaggerConfig {
}
