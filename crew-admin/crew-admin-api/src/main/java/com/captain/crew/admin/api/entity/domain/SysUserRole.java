package com.captain.crew.admin.api.entity.domain;

import lombok.Data;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2020-12-03
 **/
@Data
public class SysUserRole {

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 角色ID
     */
    private Integer roleId;
}
