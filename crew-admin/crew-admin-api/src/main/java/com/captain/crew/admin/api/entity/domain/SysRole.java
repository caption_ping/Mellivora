package com.captain.crew.admin.api.entity.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 系统角色表
 *
 * @author crewer
 */
@Data
public class SysRole {
    /**
     * 主键ID
     */
    @TableId("role_id")
    private Integer roleId;

    /**
     * 角色名
     */
    private String roleName;

    /**
     * 角色标识
     */
    private String roleCode;

    /**
     * 角色的中文描述
     */
    private String roleDesc;


    /**
     * 权限范围
     */
    private Integer dsScope;

    /**
     * 逻辑删除
     */
    private String delFlag;
}
