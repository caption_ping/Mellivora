package com.captain.crew.admin.api.entity;

import com.captain.crew.admin.api.entity.domain.SysUser;
import lombok.Data;

/**
 * @author: crewer
 * @Description: 用户信息
 * @Date: 2020/10/21 15:38
 */
@Data
public class UserInfo {

    /**
     * 用户基本信息
     */
    private SysUser user;

    /**
     * 角色集合
     */
    private Integer[] roles;

    /**
     * 权限列表
     */
    private String[] permissions;
}
