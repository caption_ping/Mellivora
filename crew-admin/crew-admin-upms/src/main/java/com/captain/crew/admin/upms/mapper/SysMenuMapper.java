package com.captain.crew.admin.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.captain.crew.admin.api.entity.domain.SysMenu;
import com.captain.crew.common.core.dto.MenuTreeNode;

import java.util.List;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/10/9 10:36
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 查询所有未被删除的菜单元素
     *
     * @return 菜单节点
     */
    List<MenuTreeNode> queryMenu();

    /**
     * 新增菜单操作
     *
     * @param menu 菜单实体类
     * @return 操作结果
     */
    int insertMenu(SysMenu menu);

    /**
     * 根据角色Id查询角色拥有的菜单信息集合
     *
     * @param roleId 角色Id
     * @return sysMenu 集合
     */
    List<SysMenu> findMenuByUserId(Integer roleId);
}
