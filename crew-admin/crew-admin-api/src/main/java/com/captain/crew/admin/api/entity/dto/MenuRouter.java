package com.captain.crew.admin.api.entity.dto;

import com.captain.crew.common.core.dto.TreeNode;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-02-04
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class MenuRouter extends TreeNode {

    /**
     * 路径
     */
    private String path;

    /**
     * 组件名
     */
    private String name;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 元数据
     */
    private Meta meta;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 类型 按钮、菜单
     */
    private Integer type;

   }
