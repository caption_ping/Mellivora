package com.captain.crew.common.quartz.component;

import com.captain.crew.admin.api.service.system.SysJobService;
import com.captain.crew.common.quartz.constant.CrewQuartzConstant;
import com.captain.crew.common.quartz.util.SysJobManage;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

/**
 * @author CAP_Crew
 * @date 2021-10-20
 **/
@Slf4j
@AllArgsConstructor
public class CrewQuartzInitJobs {
    private final SysJobManage sysJobManage;
    private final SysJobService sysJobService;

    @Bean
    public void customize() {
        sysJobService.list().forEach(sysJob -> {
            if (CrewQuartzConstant.JOB_STATUS_RELEASE.getType().equals(sysJob.getJobStatus())) {
                sysJobManage.removeJob(sysJob);
            } else if (CrewQuartzConstant.JOB_STATUS_RUNNING.getType().equals(sysJob.getJobStatus())) {
                sysJobManage.resumeJob(sysJob);
            } else if (CrewQuartzConstant.JOB_STATUS_NOT_RUNNING.getType().equals(sysJob.getJobStatus())) {
                sysJobManage.pauseJob(sysJob);
            } else {
                sysJobManage.removeJob(sysJob);
            }
        });
    }
}
