package com.captain.crew.admin.upms.service.auth;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-02-01
 **/
public interface AuthUerService {

    /**
     * 用户修改头像
     *
     * @param file     头像
     * @param username 用户名
     * @return 头像的 url 地址
     */
    String editAvatar(MultipartFile file, String username);
}
