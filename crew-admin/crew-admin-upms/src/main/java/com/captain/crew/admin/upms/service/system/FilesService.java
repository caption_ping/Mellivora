package com.captain.crew.admin.upms.service.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.captain.crew.admin.api.entity.domain.SysFiles;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2020-12-10
 **/
public interface FilesService extends IService<SysFiles> {

    /**
     * 图片文件上传
     *
     * @param file
     * @return
     */
    String uploadPicture(MultipartFile file);

    /**
     * 分页查询
     *
     * @param page
     * @return
     */
    IPage<SysFiles> queryPage(Page<?> page);

    /**
     * 删除文件，通过ID
     *
     * @param id 文件ID
     */
    void remove(Integer id);

    /**
     * 下载文件，返回 input 流
     *
     * @param id 文件ID
     * @return inputStream
     */
    InputStream downLoad(Integer id);
}
