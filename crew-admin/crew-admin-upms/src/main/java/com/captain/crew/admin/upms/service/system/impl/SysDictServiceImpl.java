package com.captain.crew.admin.upms.service.system.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.captain.crew.admin.api.entity.domain.SysDict;
import com.captain.crew.admin.upms.mapper.SysDictMapper;
import com.captain.crew.admin.upms.service.system.SysDictService;
import com.captain.crew.common.core.enums.ResponseCode;
import com.captain.crew.common.security.handle.BusinessException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author crewer
 */
@Service
@AllArgsConstructor
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements SysDictService {

    private final SysDictMapper dictMapper;

    @Override
    public Boolean edit(SysDict sysDict) {
        if (sysDict.getId() == null) {
            throw new BusinessException(ResponseCode.ILLEGAL_PARAMETER);
        }
        SysDict dict = this.getById(sysDict.getId());
        if (dict == null) {
            throw new BusinessException("查询不到字典信息");
        }
        return this.update(Wrappers.<SysDict>lambdaUpdate()
                .set(SysDict::getDescription, sysDict.getDescription())
                .set(SysDict::getRemark, sysDict.getRemark())
                .set(SysDict::getSystem, sysDict.getSystem())
                .eq(SysDict::getId, sysDict.getId()));
    }

    @Override
    public IPage<SysDict> queryList(Page<SysDict> page) {
        return dictMapper.queryList(page);
    }
}
