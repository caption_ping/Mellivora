package com.captain.crew.admin.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.captain.crew.admin.api.entity.domain.SysDictItem;

/**
 * @author crewer
 */
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {
}
