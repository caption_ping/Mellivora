package com.captain.crew.admin.upms.service.auth.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.captain.crew.admin.api.entity.domain.SysMenu;
import com.captain.crew.admin.api.entity.domain.SysRoleMenu;
import com.captain.crew.admin.upms.mapper.SysMenuMapper;
import com.captain.crew.admin.upms.mapper.SysRoleMenuMapper;
import com.captain.crew.admin.upms.service.auth.SysMenuService;
import com.captain.crew.common.core.constant.CommonConstant;
import com.captain.crew.common.core.enums.ResponseCode;
import com.captain.crew.common.core.dto.MenuTreeNode;
import com.captain.crew.common.core.dto.R;
import com.captain.crew.common.core.dto.TreeNodeUtil;
import com.captain.crew.common.security.handle.BusinessException;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/25 18:34
 */
@Service
@AllArgsConstructor
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

    private final SysMenuMapper menuMapper;
    private final SysRoleMenuMapper roleMenuMapper;
    private final RedisTemplate<String, Object> redisTemplate;

    @Override
    public List<MenuTreeNode> getAllMenuNode() {
        List<MenuTreeNode> menu = menuMapper.queryMenu();
        return TreeNodeUtil.build(menu,
                CommonConstant.MENU_TREE_ROOT_ID);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addMenu(SysMenu sysMenu) {
        return SqlHelper.retBool(menuMapper.insertMenu(sysMenu));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean editMenu(SysMenu sysMenu) {
        if (sysMenu.getMenuId() == null) {
            throw new BusinessException(ResponseCode.ILLEGAL_PARAMETER);
        }
        SysMenu menu = this.getById(sysMenu.getMenuId());
        if (menu == null) {
            throw new BusinessException("查询不到菜单信息，修改失败");
        }
        return this.update(Wrappers.<SysMenu>lambdaUpdate()
                .eq(SysMenu::getMenuId, sysMenu.getMenuId())
                .set(SysMenu::getParentId, sysMenu.getParentId())
                .set(SysMenu::getIcon, sysMenu.getIcon())
                .set(SysMenu::getName, sysMenu.getName())
                .set(SysMenu::getPath, sysMenu.getPath())
                .set(SysMenu::getPermission, sysMenu.getPermission())
                .set(SysMenu::getType, sysMenu.getType())
                .set(SysMenu::getSort, sysMenu.getSort()));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R<Boolean> removeMenuById(Integer id) {
        // 查询父节点为当前节点的节点
        List<SysMenu> menuList = this.list(Wrappers.<SysMenu>query()
                .lambda().eq(SysMenu::getParentId, id));
        if (!(menuList == null || menuList.isEmpty())) {
            return R.failed("菜单含有下级不能删除");
        }
        // 清除redis中的菜单信息
        redisTemplate.delete(CommonConstant.REDIS_ROLE_MENU);
        // 删除角色和这个菜单的关系
        roleMenuMapper.delete(Wrappers.<SysRoleMenu>lambdaQuery()
                .eq(SysRoleMenu::getMenuId, id));

        //删除当前菜单及其子菜单
        return R.ok(this.removeById(id));
    }

    @Override
    public List<SysMenu> findMenusByRoleId(Integer roleId) {
        return baseMapper.findMenuByUserId(roleId);
    }
}
