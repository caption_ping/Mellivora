package com.captain.crew.admin.upms.service.auth;

import com.baomidou.mybatisplus.extension.service.IService;
import com.captain.crew.admin.api.entity.domain.SysDept;
import com.captain.crew.common.core.vo.DeptTreeNode;
import com.captain.crew.common.core.vo.SelectTreeView;
import com.captain.crew.common.core.vo.TreeNodeView;

import java.util.List;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2020-11-30
 **/
public interface AuthDeptService extends IService<SysDept> {

    /**
     * 返回一个部门的树状列表视图
     *
     * @return 树状列表
     */
    List<DeptTreeNode> getDeptTree();



    /**
     * 添加信息部门
     *
     * @param sysDept
     * @return
     */
    Boolean saveDept(SysDept sysDept);

    /**
     * 删除部门
     *
     * @param id 部门 ID
     * @return 成功、失败
     */
    Boolean removeDeptById(Integer id);

    /**
     * 更新部门
     *
     * @param sysDept 部门信息
     * @return 成功、失败
     */
    Boolean updateDeptById(SysDept sysDept);
}
