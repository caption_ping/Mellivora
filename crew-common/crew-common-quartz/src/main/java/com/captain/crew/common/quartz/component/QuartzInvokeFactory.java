package com.captain.crew.common.quartz.component;

import com.captain.crew.admin.api.entity.domain.SysJob;
import com.captain.crew.common.quartz.event.SysJobEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Trigger;
import org.springframework.context.ApplicationEventPublisher;

/**
 * @author CAP_Crew
 * @date 2021-10-13
 **/
@AllArgsConstructor
@Slf4j
public class QuartzInvokeFactory {

    private final ApplicationEventPublisher eventPublisher;

    public void init(SysJob sysJob, Trigger trigger) {
        log.info("反射任务初始化，发布 SysJobEvent");
        this.eventPublisher.publishEvent(new SysJobEvent(sysJob, trigger));
    }
}
