package com.captain.crew.common.quartz.event;

import com.captain.crew.admin.api.entity.domain.SysJobLog;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author PC
 * @date 2021/10/16
 */
@Getter
@AllArgsConstructor
public class SysJobLogEvent {

    private final SysJobLog sysJobLog;
}
