package com.captain.crew.common.quartz.component;

import cn.hutool.core.util.StrUtil;
import com.captain.crew.admin.api.entity.domain.SysJob;
import com.captain.crew.common.core.util.UnroadSpringContext;
import com.captain.crew.common.quartz.constant.CrewQuartzConstant;
import com.captain.crew.common.quartz.constant.JobTypeQuartzEnum;
import com.captain.crew.common.quartz.exception.TaskException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author CAP_Crew
 * @date 2021-10-19
 **/
@Slf4j
public class SpringBeanTaskInvokeHandler implements TaskInvokeHandler {

    @Override
    public void execute(SysJob sysJob) throws TaskException {

        Object object;
        Method method;
        String returnValue;
        try {
            object = UnroadSpringContext.getBean(sysJob.getClassName());

            if (StrUtil.isBlank(sysJob.getMethodParamsValue())) {
                method = object.getClass().getDeclaredMethod(sysJob.getMethodName());
                ReflectionUtils.makeAccessible(method);
                returnValue = method.invoke(object).toString();
            } else {
                method = object.getClass().getDeclaredMethod(sysJob.getMethodName(), String.class);
                ReflectionUtils.makeAccessible(method);
                returnValue = method.invoke(object, sysJob.getMethodParamsValue()).toString();
            }

            if (StrUtil.isBlank(returnValue)) {
                log.error("执行方法后的返回值为 1，方法执行失败");
                throw new TaskException("执行方法后的返回值为 1，方法执行失败, 任务名：" + sysJob.getJobName());
            }
        } catch (NoSuchMethodException e) {
            log.error("SpringBeanTaskInvokeHandler 执行错误，找不到 spring bean 被指定的方法");
            throw new TaskException("SpringBeanTaskInvokeHandler 执行错误，找不到 spring bean 被指定的方法" + sysJob.getMethodName());
        } catch (IllegalAccessException e) {
            log.error("SpringBeanTaskInvokeHandler 执行错误，错误信息：{}", e.getMessage());
            throw new TaskException("SpringBeanTaskInvokeHandler 执行错误，错误信息：" + e.getMessage());
        } catch (InvocationTargetException e) {
            log.error("SpringBeanTaskInvokeHandler 执行错误，调用目标方法失败，错误信息： {}", e.getMessage());
            throw new TaskException("SpringBeanTaskInvokeHandler 执行错误，调用目标方法失败，错误信息：" + e.getMessage());
        }
    }

    /**
     * 本类只适用于 springBean 类型的任务
     * @param jobType 任务类型字符串
     * @return 本类是否支持给定的类型
     */
    @Override
    public boolean support(String jobType) {
        return JobTypeQuartzEnum.SPRING_BEAN.getType().equals(jobType);
    }
}
