package com.captain.crew.admin.upms.service.auth;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.captain.crew.admin.api.entity.domain.SysRole;

import java.util.List;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/22 11:54
 */
public interface SysRoleService extends IService<SysRole> {

    /**
     * 查询所有的角色信息
     *
     * @param page 分页
     * @return 所有的用户信息
     */
    IPage<SysRole> queryList(Page<SysRole> page);

    /**
     * 添加或者修改角色信息
     *
     * @param role 角色信息
     * @return Http响应消息
     */
    boolean add(SysRole role);

    /**
     * 修改角色信息
     *
     * @param role
     * @return
     */
    boolean edit(SysRole role);

    /**
     * 通过用户ID查询所拥有的所有角色信息
     *
     * @param userId 用户ID
     * @return sysRole 角色集合
     */
    List<SysRole> findRolesByUserId(Integer userId);
}
