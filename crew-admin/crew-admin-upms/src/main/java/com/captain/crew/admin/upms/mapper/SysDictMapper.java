package com.captain.crew.admin.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.captain.crew.admin.api.entity.domain.SysDict;

/**
 * @author crewer
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

    /**
     * 分页查询所有角色信息
     *
     * @param page 分页信息
     * @return 分页信息
     */
    IPage<SysDict> queryList(Page<?> page);
}
