package com.captain.crew.common.log.event;

import com.captain.crew.common.log.core.CrewLog;
import org.springframework.context.ApplicationEvent;

/**
 * @author CAP_Crew
 * @Description: 系统日志事件
 * @date 2021-01-20
 **/
public class SysLogEvent extends ApplicationEvent {

    public SysLogEvent(CrewLog source) {
        super(source);
    }
}
