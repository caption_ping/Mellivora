package com.captain.crew.admin.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.captain.crew.admin.api.entity.domain.SysRole;
import com.captain.crew.common.mybatis.dataScope.DataScope;

import java.util.List;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/8/28 11:23
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 根据角色ID查询角色信息
     *
     * @param roleId 角色ID
     * @return 角色实体类
     */
    SysRole getByRoleId(Integer roleId);

    /**
     * 根据 角色ID 查询角色信息
     *
     * @param roleId 角色ID
     * @return RoleEntity 角色实体类
     */
    SysRole queryRoleByRoleId(Integer roleId);

    /**
     * 添加角色信息，主键自增不需要携带roleID
     *
     * @param entity 角色实体类
     * @return 布尔
     */
    boolean insertInfo(SysRole entity);

    /**
     * 分页查询所有角色信息
     *
     * @param page 分页信息
     * @return 分页信息
     */
    IPage<SysRole> queryList(Page<?> page);

    /**
     * 根据用户ID查询用户所拥有的所有角色信息
     *
     * @param userId
     * @return
     */
    List<SysRole> findRoleByUserId(Integer userId);
}
