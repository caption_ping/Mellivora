package com.captain.crew.admin.upms.service.auth;

import com.baomidou.mybatisplus.extension.service.IService;
import com.captain.crew.admin.api.entity.domain.SysMenu;
import com.captain.crew.common.core.dto.MenuTreeNode;
import com.captain.crew.common.core.dto.R;

import java.util.List;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/25 18:32
 */
public interface SysMenuService extends IService<SysMenu> {

    /**
     * 获取所有的菜单节点，以 treeNode 的形式返回
     *
     * @return List<TreeNodeParent> treeNode集合
     */
    List<MenuTreeNode> getAllMenuNode();

    /**
     * 添加菜单信息
     *
     * @param sysMenu
     * @return 操作结果
     */
    Boolean addMenu(SysMenu sysMenu);


    /**
     * 修改菜单信息
     *
     * @param sysMenu
     * @return
     */
    Boolean editMenu(SysMenu sysMenu);


    /**
     * 删除菜单，有下级菜单的菜单不能删除
     * 同时删除角色和要删除菜单的关系
     *
     * @param id 菜单ID
     * @return 执行结果
     */
    R<Boolean> removeMenuById(Integer id);

    /**
     * 根据角色ID查询所拥有的菜单列表
     *
     * @param roleId 角色ID
     * @return sysMenu 菜单集合
     */
    List<SysMenu> findMenusByRoleId(Integer roleId);
}
