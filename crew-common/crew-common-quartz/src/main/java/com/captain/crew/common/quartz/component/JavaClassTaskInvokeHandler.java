package com.captain.crew.common.quartz.component;

import cn.hutool.core.util.StrUtil;
import com.captain.crew.admin.api.entity.domain.SysJob;
import com.captain.crew.common.quartz.constant.CrewQuartzConstant;
import com.captain.crew.common.quartz.constant.JobTypeQuartzEnum;
import com.captain.crew.common.quartz.exception.TaskException;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author CAP_Crew
 * @date 2021-10-19
 **/
@Slf4j
public class JavaClassTaskInvokeHandler implements TaskInvokeHandler {

    @Override
    public void execute(SysJob sysJob) throws TaskException {
        Class<?> clazz;
        Object obj;
        Method method;
        String returnValue;
        try {
            clazz = Class.forName(sysJob.getClassName());
            obj = clazz.getDeclaredConstructor().newInstance();
            if (StrUtil.isBlank(sysJob.getMethodParamsValue())) {
                method = clazz.getDeclaredMethod(sysJob.getMethodName());
                returnValue = method.invoke(obj).toString();
            } else {
                method = clazz.getDeclaredMethod(sysJob.getMethodName(), String.class);
                returnValue = method.invoke(obj, sysJob.getMethodParamsValue()).toString();
            }

            if (StrUtil.isBlank(returnValue)) {
                log.error("执行方法后的返回值为 1，方法执行失败");
                throw new TaskException("执行方法后的返回值为 1，方法执行失败, 任务名：" + sysJob.getJobName());
            }
        } catch (ClassNotFoundException e) {
            log.error("找不到指定类信息，错误信息：{}", e.getMessage());
            throw new TaskException("找不到制定类信息，错误信息:" + e.getMessage());
        } catch (InstantiationException e) {
            log.error("JavaClassTaskInvokeHandler 执行异常，无法实例化对象，错误信息：{}", e.getMessage());
            throw new TaskException("JavaClassTaskInvokeHandler 执行异常，无法实例化对象，错误信息" + e.getMessage());
        } catch (InvocationTargetException e) {
            log.error("调用构造器或方法时出现异常，错误信息：{}", e.getMessage());
            throw new TaskException("调用构造器或方法时出现异常，错误信息:" + e.getMessage());
        } catch (NoSuchMethodException e) {
            log.error("找不到该方法，错误信息：{}", e.getMessage());
            throw new TaskException("找不到该方法，错误信息：" + e.getMessage());
        } catch (IllegalAccessException e) {
            log.error("无权访问该方法，错误信息：{}", e.getMessage());
            throw new TaskException("无权访问该方法，错误信息：" + e.getMessage());
        }
    }

    /**
     * 只适用于 javaclass 类型的任务处理
     *
     * @param jobType 标识任务类型的字符
     * @return 本类是否支持指定的任务类型
     */
    @Override
    public boolean support(String jobType) {
        return JobTypeQuartzEnum.JAVA.getType().equals(jobType);
    }
}
