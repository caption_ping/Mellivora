package com.captain.crew.common.security.component;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReUtil;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.captain.crew.common.security.annotation.PermitUrl;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.*;
import java.util.regex.Pattern;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-05-22
 **/
@Slf4j
@Configuration
@RequiredArgsConstructor
public class PermitUrlProperties implements InitializingBean {

    private static final Pattern PATTERN = Pattern.compile("\\{(.*?)\\}");
    private final WebApplicationContext applicationContext;

    @Getter
    @Setter
    private List<String> ignoreUrls = new ArrayList<>();

    @Override
    public void afterPropertiesSet() {
        RequestMappingHandlerMapping mapping = applicationContext.getBean(RequestMappingHandlerMapping.class);
        Map<RequestMappingInfo, HandlerMethod> methods = mapping.getHandlerMethods();
        Set<String> urls = new HashSet<>();
        methods.keySet().forEach(info -> {
            HandlerMethod method = methods.get(info);
            PermitUrl annotation = AnnotationUtils.findAnnotation(method.getMethod(), PermitUrl.class);
            if (ObjectUtil.isNotEmpty(annotation)) {
                log.info("{} is permit url: {}", method, info.getPatternsCondition().getPatterns());
                info.getPatternsCondition().getPatterns()
                        .forEach(url -> urls.add(ReUtil.replaceAll(url, PATTERN, StringPool.ASTERISK)));
            }

            PermitUrl controller = AnnotationUtils.findAnnotation(method.getBeanType(), PermitUrl.class);
            if (ObjectUtil.isNotEmpty(controller)) {
                log.info("controller {} is permit url: {}", method, info.getPatternsCondition().getPatterns());
                info.getPatternsCondition().getPatterns()
                        .forEach(url -> urls.add(ReUtil.replaceAll(url, PATTERN, StringPool.ASTERISK)));
            }
        });
        ignoreUrls.addAll(urls);
    }
}
