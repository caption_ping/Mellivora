package com.captain.crew.common.core.dto;

import com.captain.crew.common.core.vo.TreeNodeView;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/25 18:13
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class MenuTreeNode extends TreeNodeView implements Serializable {

    private static final long serialVersionUID = 9124902012178260240L;

    /**
     * 图标
     */
    private String icon;

    /**
     * 组件 Url
     */
    private String url;

    /**
     * 菜单类型（0菜单、1按钮）
     */
    private Integer type;

    /**
     * 权限字符串
     */
    private String permission;
}
