package com.captain.crew.admin.api.entity.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.util.List;

/**
 * 用户表
 *
 * @author crewer
 */
@Data
public class SysUser {
    /**
     * 主键ID
     */
    @TableId
    private Integer userId;

    /**
     * 角色ID
     */
    @TableField(exist = false)
    private List<Integer> roleId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 昵称
     */
    private String nickname;

    @TableField(exist = false)
    private String roleName;

    /**
     * 密码
     */
    private String password;

    /**
     * 密码加盐
     */
    private String salt;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 部门ID
     */
    private Integer deptId;

    /**
     * 部门名称
     */
    @TableField(exist = false)
    private String deptName;

    /**
     * 备注
     */
    private String remark;


    private String lockFlag;

    /**
     * 逻辑删除
     */
    @TableLogic
    private String delFlag;

    private Integer tenantId;
}
