package com.captain.crew.common.log;

import com.captain.crew.common.core.enums.LogTypeEnum;
import com.captain.crew.common.log.annotation.SysLogger;
import com.captain.crew.common.log.core.CrewLog;
import com.captain.crew.common.log.event.SysLogEvent;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-01-20
 **/
@Aspect
@Slf4j
@Component
@AllArgsConstructor
public class CrewLogHandler {

    private final ApplicationEventPublisher publishEvent;

    @Around("@annotation(sysLogger)")
    @SneakyThrows
    public Object around(ProceedingJoinPoint point, SysLogger sysLogger) {
        long beginTime = System.currentTimeMillis();
        CrewLog log = getLog(point);
        log.setRemark(sysLogger.value());

        Object result;
        try {
            //实现保存日志逻辑
            result = point.proceed();
        } catch (Exception e) {
            log.setType(LogTypeEnum.ERROR.getType());
            log.setRemark(e.getMessage());
            throw e;
        } finally {
            long time = System.currentTimeMillis() - beginTime;
            log.setExecTime(time);
            publishEvent.publishEvent(new SysLogEvent(log));
        }
        return result;
    }

    private CrewLog getLog(ProceedingJoinPoint joinPoint) {
        // 获取方法的关键信息，类，包
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        CrewLog sysLogEntity = new CrewLog();
        sysLogEntity.setCreateDate(LocalDateTime.now());

        //请求的 类名、方法名
        String className = joinPoint.getTarget().getClass().getName();
        String methodName = signature.getName();
        sysLogEntity.setCreateBy(getUsername());
        sysLogEntity.setClassName(className);
        sysLogEntity.setMethodName(methodName);
        //请求的参数
        Object[] args = joinPoint.getArgs();
        try {
            List<String> list = new ArrayList<>();
            for (Object o : args) {
                list.add(o.toString());
            }
            sysLogEntity.setType(LogTypeEnum.NORMAL.getType());
            sysLogEntity.setParams(list.toString());
        } catch (Exception e) {
            sysLogEntity.setType(LogTypeEnum.ERROR.getType());
            sysLogEntity.setRemark(e.getMessage());
        }
        return sysLogEntity;
    }

    /**
     * 获取用户名称
     *
     * @return username
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }

}
