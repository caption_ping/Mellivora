package com.captain.crew.admin.upms.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.captain.crew.admin.api.entity.EchartsVo;
import com.captain.crew.admin.api.entity.domain.SysLog;

import java.util.List;

/**
 * @author CAP_Crew
 */
public interface SysLogService extends IService<SysLog> {

    /**
     * 各种操作次数的统计
     *
     * @return
     */
    List<EchartsVo> operateCount();

    /**
     * 近一个月各种操作的执行耗时平均数
     * @return
     */
    List<EchartsVo> averageOfEachOpConsuming();
}
