package com.captain.crew.common.quartz.component;

import cn.hutool.http.HttpUtil;
import com.captain.crew.admin.api.entity.domain.SysJob;
import com.captain.crew.common.quartz.constant.JobTypeQuartzEnum;
import com.captain.crew.common.quartz.exception.TaskException;
import lombok.extern.slf4j.Slf4j;

/**
 * @author CAP_Crew
 * @date 2021-10-19
 **/
@Slf4j
public class RestTaskInvokeHandler implements TaskInvokeHandler {

    @Override
    public void execute(SysJob sysJob) throws TaskException {
        try {
            HttpUtil.createGet(sysJob.getExecutePath()).execute();
        } catch (Exception e) {
            log.error("RestTaskInvokeHandler 执行失败，请求路径：{}", sysJob.getExecutePath());
            throw new TaskException("RestTaskInvokeHandler 执行失败, 请求路径：" + sysJob.getExecutePath());
        }
    }

    /**
     * 本类只适用于 rest 类型的任务
     *
     * @param jobType 任务类型字符
     * @return 是否支持给定的任务类型
     */
    @Override
    public boolean support(String jobType) {
        return JobTypeQuartzEnum.REST.getType().equals(jobType);
    }
}
