package com.captain.crew.admin.upms.controller.auth;

import com.captain.crew.admin.api.entity.domain.SysDept;
import com.captain.crew.admin.upms.service.auth.AuthDeptService;
import com.captain.crew.common.core.dto.R;
import com.captain.crew.common.core.vo.DeptTreeNode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2020-11-30
 **/
@RestController
@Slf4j
@RequestMapping("/dept")
@AllArgsConstructor
@Api(value = "dept", tags = "部门管理模块")
public class AuthDeptController {

    private final AuthDeptService deptService;

    /**
     * 返回部门树
     *
     * @return
     */
    @ApiOperation(value = "部门信息下拉树")
    @GetMapping("/tree")
    public R<List<DeptTreeNode>> getTree() {
        return R.ok(deptService.getDeptTree());
    }

    /**
     * 部门新增
     *
     * @param dept
     * @param result
     * @return
     */
    @ApiOperation(value = "新增部门信息")
    @PostMapping
    @PreAuthorize("@ums.hasPermission('sys_dept_add')")
    public R<Boolean> save(@RequestBody @Valid SysDept dept, BindingResult result) {
        if (result.hasErrors()) {
            return R.failed(Objects.requireNonNull(result.getFieldError()).getDefaultMessage());
        }
        return R.ok(deptService.saveDept(dept));
    }

    /**
     * 部门删除
     *
     * @param deptId
     * @return
     */
    @ApiOperation(value = "删除部门信息")
    @ApiImplicitParam(value = "部门ID", name = "deptId")
    @DeleteMapping("/{deptId}")
    @PreAuthorize("@ums.hasPermission('sys_dept_remove')")
    public R<Boolean> remove(@PathVariable("deptId") Integer deptId) {
        return R.ok(deptService.removeDeptById(deptId));
    }

    /**
     * 部门修改
     *
     * @param dept
     * @param result
     * @return
     */
    @ApiOperation(value = "修改部门信息")
    @PutMapping
    @PreAuthorize("@ums.hasPermission('sys_dept_edit')")
    public R<Boolean> modify(@RequestBody @Valid SysDept dept, BindingResult result) {
        if (result.hasErrors()) {
            return R.failed(Objects.requireNonNull(result.getFieldError()).getDefaultMessage());
        }
        return R.ok(deptService.updateDeptById(dept));
    }
}
