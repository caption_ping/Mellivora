package com.captain.crew.admin.upms.service.auth.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.captain.crew.admin.api.entity.domain.SysDept;
import com.captain.crew.admin.api.entity.domain.SysDeptRelation;
import com.captain.crew.admin.upms.mapper.SysDeptMapper;
import com.captain.crew.admin.upms.service.auth.AuthDeptService;
import com.captain.crew.admin.upms.service.auth.SysDeptRelationService;
import com.captain.crew.common.core.constant.CommonConstant;
import com.captain.crew.common.core.dto.TreeNodeUtil;
import com.captain.crew.common.core.vo.DeptTreeNode;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2020-11-30
 **/
@Service
@AllArgsConstructor
public class AuthDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements AuthDeptService {

    private final SysDeptMapper deptMapper;
    private final SysDeptRelationService sysDeptRelationService;

    @Override
    public List<DeptTreeNode> getDeptTree() {
        List<DeptTreeNode> menuTreeNodes = deptMapper.selectForTree();
        return TreeNodeUtil.buildByRecursive(menuTreeNodes,
                CommonConstant.MENU_TREE_ROOT_ID);
    }

    /**
     * 添加信息部门
     *
     * @param dept 部门
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveDept(SysDept dept) {
        SysDept sysDept = new SysDept();
        BeanUtils.copyProperties(dept, sysDept);
        this.save(sysDept);
        sysDeptRelationService.insertDeptRelation(sysDept);
        return Boolean.TRUE;
    }


    /**
     * 删除部门
     *
     * @param id 部门 ID
     * @return 成功、失败
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean removeDeptById(Integer id) {
        //级联删除部门
        List<Integer> idList = sysDeptRelationService
                .list(Wrappers.<SysDeptRelation>query().lambda()
                        .eq(SysDeptRelation::getAncestor, id))
                .stream()
                .map(SysDeptRelation::getDescendant)
                .collect(Collectors.toList());

        if (CollUtil.isNotEmpty(idList)) {
            this.removeByIds(idList);
        }

        //删除部门级联关系
        sysDeptRelationService.deleteAllDeptRelation(id);
        return Boolean.TRUE;
    }

    /**
     * 更新部门
     *
     * @param sysDept 部门信息
     * @return 成功、失败
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateDeptById(SysDept sysDept) {
        //更新部门状态
        this.updateById(sysDept);
        //更新部门关系
        SysDeptRelation relation = new SysDeptRelation();
        relation.setAncestor(sysDept.getParentId());
        relation.setDescendant(sysDept.getDeptId());
        sysDeptRelationService.updateDeptRelation(relation);
        return Boolean.TRUE;
    }
}
