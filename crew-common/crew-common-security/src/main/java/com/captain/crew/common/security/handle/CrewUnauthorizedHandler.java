package com.captain.crew.common.security.handle;

import com.alibaba.fastjson.JSON;
import com.captain.crew.common.core.enums.ResponseCode;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-01-22
 **/
@Component
public class CrewUnauthorizedHandler implements AuthenticationEntryPoint, Serializable {

    private static final long serialVersionUID = -8669253366153608780L;

    /**
     * 认证失败处理器，没有令牌，令牌错误等各种原因都会导致认证失败
     *
     * @param request
     * @param response
     * @param authException
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setStatus(HttpStatus.OK.value());
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");

        //输入响应内容
        PrintWriter writer = response.getWriter();
        Map<String, Object> res = new HashMap<>(2);
        res.put("code", ResponseCode.AUTHENTICATION_ERROR.getCode());
        res.put("msg", ResponseCode.AUTHENTICATION_ERROR.getMsg());
        String json = JSON.toJSONString(res);
        writer.write(json);
        writer.flush();
    }
}
