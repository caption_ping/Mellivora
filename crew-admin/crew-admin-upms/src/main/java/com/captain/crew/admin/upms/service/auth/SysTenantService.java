package com.captain.crew.admin.upms.service.auth;

import com.baomidou.mybatisplus.extension.service.IService;
import com.captain.crew.admin.api.entity.domain.SysTenant;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2020-12-24
 **/
public interface SysTenantService extends IService<SysTenant> {

    /**
     * 新增租户，初始化系统表
     *
     * @param sysTenant
     */
    void saveTenant(SysTenant sysTenant);
}
