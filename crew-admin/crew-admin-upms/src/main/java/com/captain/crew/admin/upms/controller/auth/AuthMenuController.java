package com.captain.crew.admin.upms.controller.auth;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.captain.crew.admin.api.entity.domain.SysMenu;
import com.captain.crew.admin.upms.service.auth.SysMenuService;
import com.captain.crew.admin.upms.service.auth.SysRoleMenuService;
import com.captain.crew.common.core.constant.CommonConstant;
import com.captain.crew.common.core.dto.MenuTreeNode;
import com.captain.crew.common.core.dto.R;
import com.captain.crew.common.log.annotation.SysLogger;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/25 19:36
 */
@Slf4j
@RestController
@RequestMapping("/menu")
@AllArgsConstructor
@Api(value = "menu", tags = "菜单管理模块")
public class AuthMenuController {

    private final SysMenuService menuService;
    private final SysRoleMenuService roleMenuService;

    /**
     * 返回树形菜单集合
     *
     * @return 树形菜单
     */
    @ApiOperation(value = "树形菜单集合")
    @GetMapping("/tree")
    public R<List<MenuTreeNode>> fetchMenu() {
        return R.ok(menuService.getAllMenuNode());
    }

    /**
     * 返回角色的菜单集合
     *
     * @param roleId 角色ID
     * @return 属性集合
     */
    @ApiOperation(value = "获得角色的菜单集合")
    @ApiImplicitParam(name = "roleId", value = "角色ID")
    @GetMapping("/tree/{roleId}")
    public R<List<Integer>> getRoleTree(@PathVariable("roleId") Integer roleId) {
        return R.ok(roleMenuService.queryMenuByRoleId(roleId));
    }

    /**
     * 删除菜单信息
     *
     * @param id 菜单节点ID
     * @return 处理结果
     */
    @ApiOperation(value = "删除菜单信息")
    @ApiImplicitParam(name = "id", value = "菜单ID")
    @SysLogger("删除菜单信息")
    @DeleteMapping("/{id}")
    @PreAuthorize("@ums.hasPermission('sys_menu_remove')")
    public R<Boolean> delMenu(@PathVariable("id") Integer id) {
        return menuService.removeMenuById(id);
    }

    /**
     * 处理提交菜单信息
     *
     * @param sysMenu 菜单信息
     * @return 处理结果
     */
    @ApiOperation(value = "修改菜单信息")
    @SysLogger("修改菜单信息")
    @PutMapping
    @PreAuthorize("@ums.hasPermission('sys_menu_edit')")
    public R<Boolean> edit(@Valid @RequestBody SysMenu sysMenu) {
        return R.ok(menuService.editMenu(sysMenu));
    }

    /**
     * 新添加菜单信息
     *
     * @param sysMenu
     * @return
     */
    @ApiOperation(value = "新增加菜单")
    @SysLogger("添加菜单信息")
    @PostMapping
    @PreAuthorize("@ums.hasPermission('sys_menu_add')")
    public R<Boolean> save(@Valid @RequestBody SysMenu sysMenu) {
        return R.ok(menuService.addMenu(sysMenu));
    }

    /**
     * 查看所有菜单信息，不包括按钮
     *
     * @return
     */
    @ApiOperation(value = "查看菜单信息不包括按钮")
    @GetMapping
    public R<List<SysMenu>> list() {
        return R.ok(menuService.list(Wrappers.<SysMenu>lambdaQuery()
                .eq(SysMenu::getType, CommonConstant.PERMISSION_MENU)));
    }
}
