package com.captain.crew.common.quartz.component;

import cn.hutool.core.util.StrUtil;
import com.captain.crew.common.core.util.UnroadSpringContext;
import com.captain.crew.common.quartz.exception.TaskException;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author CAP_Crew
 * @date 2021-10-20
 **/
@Slf4j
public class TaskInvokeFactory {

    public static TaskInvokeHandler getInvoker(String jobType) throws TaskException {
        if (StrUtil.isBlank(jobType)) {
            log.info("获取 TaskInvoke 传递参数有误，jobType:{}", jobType);
            throw new TaskException();
        }

        AtomicReference<TaskInvokeHandler> iTaskInvoke = new AtomicReference<>();
        Map<String, TaskInvokeHandler> taskInvokeHandlers = UnroadSpringContext.applicationContext().getBeansOfType(TaskInvokeHandler.class);
        taskInvokeHandlers.keySet()
                .stream()
                .filter(beanName -> taskInvokeHandlers.get(beanName).support(jobType))
                .findFirst()
                .ifPresent(bean -> iTaskInvoke.set(taskInvokeHandlers.get(bean)));
        if (iTaskInvoke.get() == null) {
            throw new TaskException("没有合适的任务处理器: " + jobType);
        }
        return iTaskInvoke.get();
    }
}
