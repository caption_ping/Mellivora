package com.captain.crew.admin.upms.controller.system;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.captain.crew.admin.api.entity.domain.SysJob;
import com.captain.crew.admin.api.service.system.SysJobService;
import com.captain.crew.common.core.dto.R;
import com.captain.crew.common.quartz.constant.CrewQuartzConstant;
import com.captain.crew.common.quartz.util.SysJobManage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @author CAP_Crew
 * @date 2021-10-20
 **/
@Api(value = "sys-job", tags = "定时任务模块")
@Slf4j
@RestController
@RequestMapping("sys-job")
@AllArgsConstructor
public class SysJobController {

    private final SysJobManage sysJobManage;
    private final SysJobService sysJobService;

    /**
     * 分页查询
     *
     * @param page
     * @param sysJob
     * @return
     */
    @ApiOperation("分页查询任务")
    @GetMapping
    public R<?> page(Page<SysJob> page, SysJob sysJob) {
        return R.ok(sysJobService.page(page, Wrappers.query(sysJob)));
    }

    /**
     * 发布定时任务
     *
     *
     * @param sysJob
     * @return
     */
    @PreAuthorize("@ums.hasPermission('sys_job_save')")
    @ApiOperation("发布定时任务")
    @PostMapping
    public R<?> save(@RequestBody SysJob sysJob) {
        sysJobService.save(sysJob);
        return R.ok();
    }

    /**
     * 启动定时任务或从暂停中恢复定时任务
     *
     * @param id
     * @return
     */
    @PreAuthorize("@ums.hasPermission('sys_job_start')")
    @ApiOperation("启动定时任务或从暂停中恢复定时任务")
    @PostMapping("/start-job/{id}")
    public R<?> bootstrap(@PathVariable Integer id) {
        SysJob sysJob = sysJobService.getById(id);

        if (sysJob != null && CrewQuartzConstant.JOB_STATUS_RELEASE.getType().equals(sysJob.getJobStatus())) {
            sysJobManage.addOrUpdate(sysJob);
        } else {
            sysJobManage.resumeJob(sysJob);
        }
        //更新定时任务状态条件，暂停状态3更新为运行状态2
        SysJob job = new SysJob();
        job.setJobId(id);
        job.setJobStatus(CrewQuartzConstant.JOB_STATUS_RUNNING.getType());
        this.sysJobService.updateById(job);
        return R.ok();
    }

    /**
     * 修改定时任务
     *
     * @param sysJob
     * @return
     */
    @PreAuthorize("@ums.hasPermission('sys_job_update')")
    @ApiOperation("修改定时任务")
    @PutMapping
    public R<?> updateById(@RequestBody SysJob sysJob) {
        SysJob oldJob = this.sysJobService.getById(sysJob.getJobId());
        if (CrewQuartzConstant.JOB_STATUS_RUNNING.getType().equals(oldJob.getJobStatus())) {
            sysJobManage.addOrUpdate(sysJob);
        }
        this.sysJobService.updateById(sysJob);
        return R.ok();
    }

    /**
     * 暂停任务
     *
     * @param id
     * @return
     */
    @PreAuthorize("@ums.hasPermission('sys_job_pause')")
    @ApiOperation("暂停任务")
    @PostMapping("/pause-job/{id}")
    public R<?> pauseJob(@PathVariable Integer id) {
        SysJob sysJob = this.sysJobService.getById(id);
        //更新定时任务状态条件，运行状态2更新为暂停状态3
        this.sysJobService.updateById(SysJob.builder().jobId(sysJob.getJobId())
                .jobStatus(CrewQuartzConstant.JOB_STATUS_NOT_RUNNING.getType()).build());
        sysJobManage.pauseJob(sysJob);
        return R.ok();
    }

    /**
     * 通过 ID 删除定时任务
     *
     * @param id
     * @return
     */
    @PreAuthorize("@ums.hasPermission('sys_job_delete')")
    @ApiOperation("通过 ID 删除定时任务")
    @DeleteMapping("/{id}")
    public R<?> deleteJob(@PathVariable Integer id) {
        SysJob sysJob = this.sysJobService.getById(id);
        if (CrewQuartzConstant.JOB_STATUS_NOT_RUNNING.getType().equals(sysJob.getJobStatus())) {
            this.sysJobManage.removeJob(sysJob);
        }
        this.sysJobService.removeById(id);
        log.info("delete job !");
        return R.ok();
    }

    /**
     * 暂停全部定时任务
     *
     * @return
     */
    @PreAuthorize("@ums.hasPermission('sys_jobs_shutdown')")
    @ApiOperation("暂停全部定时任务")
    @PostMapping("/shutdown-jobs")
    public R<?> shutdownJobs() {
        sysJobManage.pauseJobs();
        int count = this.sysJobService.count(new LambdaQueryWrapper<SysJob>()
                .eq(SysJob::getJobStatus, CrewQuartzConstant.JOB_STATUS_RUNNING.getType()));
        if (count <= 0) {
            return R.failed("没有正在运行中的任务");
        } else {
            //更新定时任务状态条件，运行状态2更新为暂停状态2
            this.sysJobService.update(SysJob.builder()
                    .jobStatus(CrewQuartzConstant.JOB_STATUS_NOT_RUNNING.getType()).build(), new UpdateWrapper<SysJob>()
                    .lambda().eq(SysJob::getJobStatus, CrewQuartzConstant.JOB_STATUS_RUNNING.getType()));
            return R.ok();
        }
    }

    /**
     * 启动全部定时任务
     *
     * @return
     */
    @PreAuthorize("@ums.hasPermission('sys_jobs_start')")
    @ApiOperation("启动全部定时任务")
    @PostMapping("/start-jobs")
    public R<?> startJobs() {
        //更新定时任务状态条件，暂停状态3更新为运行状态2
        this.sysJobService.update(SysJob.builder().jobStatus(CrewQuartzConstant.JOB_STATUS_RUNNING
                .getType()).build(), new UpdateWrapper<SysJob>().lambda()
                .eq(SysJob::getJobStatus, CrewQuartzConstant.JOB_STATUS_NOT_RUNNING.getType()));
        sysJobManage.startJobs();
        return R.ok();
    }
}
