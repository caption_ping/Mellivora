package com.captain.crew.common.quartz.util;

import com.captain.crew.admin.api.entity.domain.SysJob;
import com.captain.crew.common.quartz.constant.CrewQuartzConstant;
import com.captain.crew.common.quartz.job.CrewTaskFactory;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;

/**
 * @author CAP_Crew
 * @date 2021-10-13
 **/
@AllArgsConstructor
@Slf4j
public class SysJobManage {

    private final Scheduler scheduler;

    /**
     * 获取定时任务唯一的 Key
     *
     * @param sysJob
     * @return
     */
    public JobKey getJobKey(SysJob sysJob) {
        return JobKey.jobKey(sysJob.getJobName(), sysJob.getJobGroup());
    }

    /**
     * 获取 Trigger 唯一的 key
     *
     * @param sysJob
     * @return
     */
    public TriggerKey getTriggerKey(SysJob sysJob) {
        return TriggerKey.triggerKey(sysJob.getJobName(), sysJob.getJobGroup());
    }

    /**
     * 添加或者更新任务
     *
     * @param sysJob
     */
    public void addOrUpdate(SysJob sysJob) {
        try {
            if (isNotExistJob(sysJob)) {
                this.newJob(sysJob);
            } else {
                this.updateJob(sysJob);
            }
            // 如果任务的状态为暂停状态，对应的调度任务也暂停
            if (CrewQuartzConstant.JOB_STATUS_NOT_RUNNING.getType().equals(sysJob.getJobStatus())) {
                this.pauseJob(sysJob);
            }
        } catch (SchedulerException e) {
            log.error("更新或添加定时任务失败，失败信息：{}", e.getMessage());
        }
    }

    /**
     * 立即执行一次任务
     */
    public boolean runOnce(Scheduler scheduler, SysJob sysJob) {
        try {
            JobDataMap dataMap = new JobDataMap();
            dataMap.put(CrewQuartzConstant.TASK_PARAM_KEY.getType().toString(), sysJob);

            scheduler.triggerJob(getJobKey(sysJob), dataMap);
        } catch (SchedulerException e) {
            log.error("立刻执行定时任务，失败信息：{}", e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * 暂停定时任务
     *
     * @param sysJob
     */
    public void pauseJob(SysJob sysJob) {
        try {
            if (scheduler != null) {
                scheduler.pauseJob(getJobKey(sysJob));
            }
        } catch (SchedulerException e) {
            log.error("暂停任务失败，失败信息：{}", e.getMessage());
        }

    }

    public void resumeJob(SysJob sysJob) {
        try {
            scheduler.resumeJob(getJobKey(sysJob));
        } catch (SchedulerException e) {
            log.error("恢复任务失败，失败信息 {}", e.getMessage());
        }
    }

    public void removeJob(SysJob sysJob) {
        try {
            TriggerKey triggerKey = getTriggerKey(sysJob);
            // 暂停任务
            scheduler.pauseTrigger(triggerKey);
            // 移除触发器
            scheduler.unscheduleJob(triggerKey);
            scheduler.deleteJob(getJobKey(sysJob));
        } catch (SchedulerException e) {
            log.error("删除定时任务失败，失败信息：{}", e.getMessage());
        }
    }


    /**
     * 启动所有运行定时任务
     */
    public void startJobs() {
        try {
            if (scheduler != null) {
                scheduler.resumeAll();
            }
        } catch (SchedulerException e) {
            log.error("启动所有运行定时任务失败，失败信息：{}", e.getMessage());
        }
    }

    /**
     * 停止所有运行定时任务
     */
    public void pauseJobs() {
        try {
            if (scheduler != null) {
                scheduler.pauseAll();
            }
        } catch (Exception e) {
            log.error("暂停所有运行定时任务失败，失败信息：{}", e.getMessage());
        }
    }

    /**
     * 获取错失执行策略方法
     *
     * @param sysJob
     * @param cronScheduleBuilder
     * @return
     */
    public CronScheduleBuilder handleCronScheduleMisfirePolicy(SysJob sysJob, CronScheduleBuilder cronScheduleBuilder) {
        if (CrewQuartzConstant.MISFIRE_DEFAULT.getType().equals(sysJob.getMisfirePolicy())) {
            return cronScheduleBuilder;
        } else if (CrewQuartzConstant.MISFIRE_IGNORE_MISFIRES.getType().equals(sysJob.getMisfirePolicy())) {
            return cronScheduleBuilder.withMisfireHandlingInstructionIgnoreMisfires();
        } else if (CrewQuartzConstant.MISFIRE_FIRE_AND_PROCEED.getType().equals(sysJob.getMisfirePolicy())) {
            return cronScheduleBuilder.withMisfireHandlingInstructionFireAndProceed();
        } else if (CrewQuartzConstant.MISFIRE_DO_NOTHING.getType().equals(sysJob.getMisfirePolicy())) {
            return cronScheduleBuilder.withMisfireHandlingInstructionDoNothing();
        } else {
            return cronScheduleBuilder;
        }
    }

    /**
     * scheduler 中是否已经存在这个任务
     *
     * @param sysJob
     * @return
     * @throws SchedulerException
     */
    private boolean isNotExistJob(SysJob sysJob) throws SchedulerException {
        TriggerKey triggerKey = getTriggerKey(sysJob);
        Trigger trigger = scheduler.getTrigger(triggerKey);
        return trigger != null;
    }

    /**
     * 创建新任务
     *
     * @param sysJob
     * @throws SchedulerException
     */
    private void newJob(SysJob sysJob) throws SchedulerException {
        JobKey jobKey = getJobKey(sysJob);
        TriggerKey triggerKey = getTriggerKey(sysJob);
        JobDetail jobDetail = JobBuilder.newJob(CrewTaskFactory.class).withIdentity(jobKey).build();
        jobDetail.getJobDataMap().put(CrewQuartzConstant.TASK_PARAM_KEY.getType().toString(), sysJob);

        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(sysJob.getCronExpression());
        cronScheduleBuilder = this.handleCronScheduleMisfirePolicy(sysJob, cronScheduleBuilder);

        CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity(triggerKey).withSchedule(cronScheduleBuilder).build();
        scheduler.scheduleJob(jobDetail, cronTrigger);
    }

    /**
     * 修改已经存在的任务
     *
     * @param sysJob
     * @throws SchedulerException
     */
    private void updateJob(SysJob sysJob) throws SchedulerException {
        TriggerKey triggerKey = getTriggerKey(sysJob);
        CronTrigger cronTrigger = (CronTrigger) scheduler.getTrigger(triggerKey);
        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(sysJob.getCronExpression());
        scheduleBuilder = this.handleCronScheduleMisfirePolicy(sysJob, scheduleBuilder);

        cronTrigger = cronTrigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();
        cronTrigger.getJobDataMap().put(CrewQuartzConstant.TASK_PARAM_KEY.getType().toString(), sysJob);
        scheduler.rescheduleJob(triggerKey, cronTrigger);
    }
}
