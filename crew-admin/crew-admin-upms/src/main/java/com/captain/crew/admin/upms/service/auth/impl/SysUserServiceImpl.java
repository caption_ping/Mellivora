package com.captain.crew.admin.upms.service.auth.impl;

import cn.hutool.core.util.ArrayUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.captain.crew.admin.api.entity.UserInfo;
import com.captain.crew.admin.api.entity.domain.SysMenu;
import com.captain.crew.admin.api.entity.domain.SysRole;
import com.captain.crew.admin.api.entity.domain.SysUser;
import com.captain.crew.admin.api.entity.domain.SysUserRole;
import com.captain.crew.admin.api.entity.dto.EditPasswordInfo;
import com.captain.crew.admin.api.service.auth.SysUserService;
import com.captain.crew.admin.upms.mapper.SysUserMapper;
import com.captain.crew.admin.upms.mapper.SysUserRoleMapper;
import com.captain.crew.admin.upms.service.auth.AuthUerService;
import com.captain.crew.admin.upms.service.auth.SysMenuService;
import com.captain.crew.admin.upms.service.auth.SysRoleService;
import com.captain.crew.admin.upms.service.system.FilesService;
import com.captain.crew.common.core.constant.CommonConstant;
import com.captain.crew.common.core.dto.R;
import com.captain.crew.common.core.enums.ResponseCode;
import com.captain.crew.common.mybatis.dataScope.DataScope;
import com.captain.crew.common.security.handle.BusinessException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @Author: crewer
 * @Description:
 * @Date: 2020/8/28 10:40
 */
@Slf4j
@Service(value = "userService")
@AllArgsConstructor
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService, AuthUerService {

    private final SysUserMapper userDao;
    private final SysUserRoleMapper userRoleMapper;
    private final SysRoleService roleService;
    private final SysMenuService menuService;
    private final FilesService filesService;
    private final BCryptPasswordEncoder passwordEncoder;

    @Override
    public R<Void> editPassword(EditPasswordInfo userInfo) {

        // 验证输入的旧密码和新密码是否相等，相等则返回错误信息：新密码和旧密码相同
        if (userInfo.getOldPwd().equals(userInfo.getNewPwd())) {
            return R.failed("新密码与旧密码相同", null);
        }

        // 验证输入的新密码和再次输入的密码是否一致，不一致返回错误信息
        if (!userInfo.getNewPwd().equals(userInfo.getConfirmPwd())) {
            return R.failed("请检查两次密码输入是否一致", null);
        }

        // 1. 根据用户名查询数据库中该用户的密码
        String password = userDao.getByUsername(userInfo.getUsername()).getPassword();
        // 2. 对比表单的旧密码和数据库密码
        if (passwordEncoder.matches(userInfo.getOldPwd(), password)) {

            // 对比成功， 用表单的新密码覆盖数据库旧密码
            this.update(Wrappers.<SysUser>lambdaUpdate()
                    .set(SysUser::getPassword, passwordEncoder.encode(userInfo.getNewPwd()))
                    .eq(SysUser::getUsername, userInfo.getUsername()));
            return R.ok("密码修改成功", null);
        }
        // 对比失败， 返回错误信息，旧密码输入错误
        else {
            throw new BusinessException(ResponseCode.UNKNOWN);
        }
    }

    @Override
    public UserInfo findUserInfo(String username) {
        SysUser user = super.getOne(Wrappers.<SysUser>lambdaQuery()
                .eq(SysUser::getUsername, username));
        if (ObjectUtils.isEmpty(user)) {
            return null;
        }
        UserInfo userInfo = new UserInfo();
        userInfo.setUser(user);

        // 设置角色列表 roleId
        List<Integer> roleList = roleService.findRolesByUserId(user.getUserId())
                .stream()
                .map(SysRole::getRoleId)
                .collect(Collectors.toList());
        userInfo.setRoles(ArrayUtil.toArray(roleList, Integer.class));

        // 设置权限列表 menu.permission
        Set<String> permissionSet = new HashSet<>();
        roleList.forEach(roleId -> {
            List<String> permissionList = menuService.findMenusByRoleId(roleId)
                    .stream()
                    .filter(menu -> StringUtils.isNotBlank(menu.getPermission()))
                    .map(SysMenu::getPermission)
                    .collect(Collectors.toList());
            permissionSet.addAll(permissionList);
        });
        userInfo.setPermissions(ArrayUtil.toArray(permissionSet, String.class));
        return userInfo;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveUser(SysUser user) {
        this.save(user);
        user.getRoleId().forEach(roleId -> {
            SysUserRole userRole = new SysUserRole();
            userRole.setUserId(user.getUserId());
            userRole.setRoleId(roleId);
            userRoleMapper.insert(userRole);
        });
        return Boolean.TRUE;
    }

    @Override
    public IPage<SysUser> queryList(Page<SysUser> page, SysUser sysUser) {
        IPage<SysUser> list = userDao.queryList(page, sysUser, new DataScope());
        list.getRecords().forEach(user -> {
            List<SysRole> roles = roleService.findRolesByUserId(user.getUserId());
            user.setPassword(CommonConstant.SENSITIVE);
            List<Integer> roleList = new ArrayList<>();
            StringBuffer buffer = new StringBuffer();
            roles.forEach(role -> {
                buffer.append(role.getRoleName());
                buffer.append(CommonConstant.COMMA);
                roleList.add(role.getRoleId());
            });
            user.setRoleName(StringUtils.removeEnd(buffer.toString(), CommonConstant.COMMA));
            user.setRoleId(roleList);
        });
        return list;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean editUserInfo(SysUser sysUser) {
        // 删除当前用户角色关系
        userRoleMapper.delete(Wrappers.<SysUserRole>lambdaUpdate()
                .eq(SysUserRole::getUserId, sysUser.getUserId()));

        // 添加新的用户角色关系
        sysUser.getRoleId().forEach(roleId -> {
            SysUserRole userRole = new SysUserRole();
            userRole.setUserId(sysUser.getUserId());
            userRole.setRoleId(roleId);
            userRoleMapper.insert(userRole);
        });
        // 更新用户信息
        return this.update(Wrappers.<SysUser>lambdaUpdate()
                .set(StringUtils.isNotBlank(sysUser.getPassword()), SysUser::getPassword, passwordEncoder.encode(sysUser.getPassword()))
                .set(SysUser::getRemark, sysUser.getRemark())
                .set(SysUser::getNickname, sysUser.getNickname())
                .set(SysUser::getPhone, sysUser.getPhone())
                .set(SysUser::getDeptId, sysUser.getDeptId())
                .eq(SysUser::getUserId, sysUser.getUserId()));
    }

    @Override
    public String editAvatar(MultipartFile file, String username) {
        String picture = filesService.uploadPicture(file);
        if (StringUtils.isBlank(picture)) {
            return null;
        }
        this.update(Wrappers.<SysUser>lambdaUpdate()
                .set(SysUser::getAvatar, picture)
                .eq(SysUser::getUsername, username));
        return picture;
    }
}
