package com.captain.crew.admin.api.entity.dto;

import lombok.Data;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-05-11
 **/
@Data
public class Meta {
    /**
     * 标题
     */
    private String title;

    /**
     * icon
     */
    private String icon;
}
