package com.captain.crew.common.security.component;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-01-21
 **/
@Component
public class PasswordEncode {

    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
