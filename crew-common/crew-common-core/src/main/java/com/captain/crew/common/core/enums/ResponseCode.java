package com.captain.crew.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 请求响应状态码
 *
 * @Author: crewer
 * @Description:
 * @Date: 2020/8/25 15:58
 */
@AllArgsConstructor
@Getter
public enum ResponseCode {

    /**
     * 未知错误类型
     */
    UNKNOWN(-1, "未知错误"),

    /**
     * 成功
     */
    OK(20000, "操作成功"),

    /**
     * 失败
     */
    FAILED(1, "操作失败"),

    ACCESS_DENIED(4003, "抱歉，您没有权限这么做"),
    AUTHENTICATION_ERROR(4002, "认证失败"),
    UN_AUTHORIZE(4001, "授权失败"),
    BAD_CREDENTIALS(4004, "用户名或密码错误"),

    FAIL_UPLOAD_RESOURCE(51001, "图片上传失败"),

    FORM_INFO_INCOMPLETE(50001, "表单信息不完整"),
    ILLEGAL_PARAMETER(50002, "请求参数错误");


    private final Integer code;
    private final String msg;
}
