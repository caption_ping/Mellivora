package com.captain.crew.admin.upms.service.system.impl;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.captain.crew.admin.api.entity.domain.SysFiles;
import com.captain.crew.common.minio.service.MinioTemplate;
import com.captain.crew.common.security.handle.BusinessException;
import com.captain.crew.admin.upms.mapper.SysFilesMapper;
import com.captain.crew.admin.upms.service.system.FilesService;
import com.captain.crew.common.core.constant.CommonConstant;
import com.captain.crew.common.core.enums.ResponseCode;
import com.captain.crew.common.security.util.SecurityUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2020-12-10
 **/
@Service
@Slf4j
@AllArgsConstructor
public class FilesServiceImpl extends ServiceImpl<SysFilesMapper, SysFiles> implements FilesService {

    private final MinioTemplate minioHandler;

    private final SysFilesMapper filesMapper;

    @Override
    public String uploadPicture(MultipartFile file) {
        String fileUrl;
        String name = file.getOriginalFilename();
        assert name != null;
        int indexOf = name.lastIndexOf(".");
        String suffix = name.substring(indexOf);
        String randomName = RandomUtil.randomString(16) + suffix;
        try {
            fileUrl = minioHandler.putObject(CommonConstant.MINIO_BUCKET_PICTURE,
                    randomName,
                    file.getInputStream(),
                    file.getContentType());
        } catch (IOException e) {
            log.error("图片上传至 {} 失败，错误信息 {}", CommonConstant.MINIO_BUCKET_PICTURE, e.getMessage());
            e.printStackTrace();
            throw new BusinessException(ResponseCode.FAIL_UPLOAD_RESOURCE);
        }
        if (StringUtils.isNotBlank(fileUrl)) {
            SysFiles sysFiles = new SysFiles();
            sysFiles.setFileName(randomName);
            sysFiles.setSize(file.getSize());
            sysFiles.setModifier(SecurityUtils.getUser().getUsername());
            sysFiles.setBucketName(CommonConstant.MINIO_BUCKET_PICTURE);
            this.save(sysFiles);
        }
        return fileUrl;
    }

    @Override
    public IPage<SysFiles> queryPage(Page<?> page) {
        return filesMapper.getPage(page);
    }

    @Override
    public void remove(Integer id) {
        SysFiles one = this.getOne(Wrappers.<SysFiles>lambdaQuery().eq(SysFiles::getId, id));
        if (one == null) {
            throw new BusinessException("文件不存在");
        }
        this.removeById(id);
        minioHandler.removeObject(one.getBucketName(), one.getFileName());
    }

    @Override
    public InputStream downLoad(Integer id) {
        SysFiles one = this.getOne(Wrappers.<SysFiles>lambdaQuery().eq(SysFiles::getId, id));
        return minioHandler.getObject(one.getBucketName(), one.getFileName());
    }

}
