package com.captain.crew.common.mybatis.tenant;

import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import com.captain.crew.common.mybatis.property.CrewTenantTableProperty;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.NullValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-01-11
 **/
@Component
public class CrewTenantHandler implements TenantLineHandler {

    @Autowired
    private CrewTenantTableProperty property;

    @Override
    public Expression getTenantId() {
        Integer tenantId = TenantContextHolder.getTenantId();
        if (tenantId == null) {
            return new NullValue();
        }
        return new LongValue(tenantId);
    }

    @Override
    public String getTenantIdColumn() {
        return property.getFieldName();
    }

    @Override
    public boolean ignoreTable(String tableName) {
        Integer tenantId = TenantContextHolder.getTenantId();
        // 租户中ID 为空，查询全部，不进行过滤
        if (tenantId == null) {
            return Boolean.TRUE;
        }
        return !property.getTables().contains(tableName);
    }
}
