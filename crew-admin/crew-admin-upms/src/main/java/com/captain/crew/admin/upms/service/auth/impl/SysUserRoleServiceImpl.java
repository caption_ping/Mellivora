package com.captain.crew.admin.upms.service.auth.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.captain.crew.admin.api.entity.domain.SysUserRole;
import com.captain.crew.admin.upms.mapper.SysUserRoleMapper;
import com.captain.crew.admin.upms.service.auth.SysUserRoleService;
import org.springframework.stereotype.Service;

/**
 * @author CAP_Crew
 * @Description
 * @date 2021-09-10
 **/
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {
}
