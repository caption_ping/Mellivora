## 基于 SpringBoot 的后台管理项目
Spring Boot

Spring Security + Jwt 实现权限认证

MyBatis-Plus + MySql 实现数据库操作

完备的细粒度权限控制

基于 Mybatis 定制数据权限过滤插件，开发无感知实现数据权限过滤

严格遵从阿里巴巴开发规范

## 后期规划
- [x] Swagger 自动生成接口文档
- [ ] ELK 实现日志自动收集
- [ ] ElasticSearch 全文检索（需要数据，考虑要不要接入一些业务）
- [ ] redis 系统监控
- [x] Quartz 定时任务调度

工作流坚决不接入
