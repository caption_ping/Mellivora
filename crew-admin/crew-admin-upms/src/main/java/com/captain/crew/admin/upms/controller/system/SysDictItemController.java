package com.captain.crew.admin.upms.controller.system;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.captain.crew.admin.api.entity.domain.SysDictItem;
import com.captain.crew.admin.upms.service.system.SysDictItemService;
import com.captain.crew.common.core.constant.CacheConstants;
import com.captain.crew.common.core.dto.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author CAP_Crew
 * @date 2021-09-25
 **/
@Slf4j
@RequestMapping("/dict-item")
@RestController
@AllArgsConstructor
@Api(value = "dict-item", tags = "字典项管理模块")
public class SysDictItemController {

    private final SysDictItemService dictItemService;

    /**
     * 查询指定字典类型下的所有字典项
     *
     * @param index 字典类型ID
     * @return 字典项列表
     */
    @ApiOperation("查询指定字典类型下的所有字典项")
    @GetMapping("list/{index}")
    public R<List<SysDictItem>> itemList(@PathVariable("index") Integer index) {
        return R.ok(dictItemService.list(Wrappers.<SysDictItem>lambdaQuery()
                .eq(SysDictItem::getDictId, index)));
    }

    /**
     * 新添加字典项信息
     *
     * @param dictItem 字典项实体类
     * @return 处理结果成功与否
     */
    @ApiOperation("保存字典项信息")
    @PostMapping
    @PreAuthorize("@ums.hasPermission('sys_dict_item_save')")
    public R<Boolean> saveDictItem(@RequestBody SysDictItem dictItem) {
        return R.ok(dictItemService.save(dictItem));
    }

    /**
     * 修改字典项信息
     *
     * @param dictItem 字典项实体类
     * @return 处理结果成功与否
     */
    @ApiOperation("修改字典项信息")
    @PutMapping
    @PreAuthorize("@ums.hasPermission('sys_dict_item_edit')")
    public R<?> editDictItem(@RequestBody SysDictItem dictItem) {
        return R.ok(dictItemService.edit(dictItem));
    }

    /**
     * 删除字典项
     *
     * @param id 字典项ID
     * @return 处理结果
     */
    @ApiOperation("删除字典项")
    @DeleteMapping("/{id}")
    @PreAuthorize("@ums.hasPermission('sys_dict_item_remove')")
    public R<Void> removeDictItem(@PathVariable("id") Integer id) {
        if (id != null && dictItemService.removeById(id)) {
            return R.ok();
        }
        return R.failed();
    }

    @ApiOperation("根据字典类型查询字典项")
    @GetMapping("/type/{type}")
    @Cacheable(value = CacheConstants.DICT_DETAILS, key = "#type", unless = "#result == null")
    public R<List<SysDictItem>> getByDictType(@PathVariable String type) {
        return R.ok(dictItemService.list(Wrappers.<SysDictItem>lambdaQuery()
                .eq(SysDictItem::getDictType, type)));
    }
}
