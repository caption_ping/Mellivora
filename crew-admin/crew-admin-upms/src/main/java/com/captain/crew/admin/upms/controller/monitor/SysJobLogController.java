package com.captain.crew.admin.upms.controller.monitor;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.captain.crew.admin.api.entity.domain.SysJobLog;
import com.captain.crew.admin.api.service.system.SysJobLogService;
import com.captain.crew.common.core.dto.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CAP_Crew
 * @date 2021-10-21
 **/
@Api(value = "job-log", tags = "Quartz 日志")
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/job-log")
public class SysJobLogController {

    private final SysJobLogService sysJobLogService;

    /**
     * 分页查询 Quartz 执行日志
     *
     * @param page
     * @param sysJobLog
     * @return
     */
    @ApiOperation("分页查询 Quartz 执行日志")
    @GetMapping
    public R<?> page(Page<SysJobLog> page, SysJobLog sysJobLog) {
        page.addOrder(OrderItem.desc("job_log_id"));
        return R.ok(sysJobLogService.page(page, Wrappers.query(sysJobLog)));
    }
}
