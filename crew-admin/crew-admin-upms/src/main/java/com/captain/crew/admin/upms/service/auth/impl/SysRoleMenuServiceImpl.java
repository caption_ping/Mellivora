package com.captain.crew.admin.upms.service.auth.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.captain.crew.admin.api.entity.domain.SysRoleMenu;
import com.captain.crew.admin.api.entity.dto.MenuRouter;
import com.captain.crew.admin.upms.mapper.SysRoleMenuMapper;
import com.captain.crew.admin.upms.service.auth.SysRoleMenuService;
import com.captain.crew.common.core.constant.CommonConstant;
import com.captain.crew.common.core.enums.ResponseCode;
import com.captain.crew.common.core.dto.TreeNode;
import com.captain.crew.common.security.handle.BusinessException;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/28 17:53
 */
@Service
@AllArgsConstructor
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {

    private final SysRoleMenuMapper roleMenuMapper;
    private final RedisTemplate<String, Object> redisTemplate;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveRoleMenus(Integer roleId, String menuIds) {
        if (StringUtils.isBlank(menuIds)) {
            throw new BusinessException(ResponseCode.ILLEGAL_PARAMETER);
        }

        this.remove(Wrappers.<SysRoleMenu>lambdaQuery()
                .eq(SysRoleMenu::getRoleId, roleId));

        redisTemplate.opsForHash().delete(CommonConstant.REDIS_ROLE_MENU, String.valueOf(roleId));
        List<SysRoleMenu> roleMenus = Arrays
                .stream(menuIds.split(","))
                .map(menuId -> {
                    SysRoleMenu roleMenu = new SysRoleMenu();
                    roleMenu.setRoleId(roleId);
                    roleMenu.setMenuId(Integer.valueOf(menuId));
                    return roleMenu;
                })
                .collect(Collectors.toList());

        return this.saveBatch(roleMenus);
    }

    @Override
    public List<Integer> queryMenuByRoleId(Integer roleId) {
        List<MenuRouter> menuRouters = roleMenuMapper.queryMenuByRoleId(roleId);
        Set<Integer> parentsId = menuRouters.stream()
                .map(MenuRouter::getParentId)
                .collect(Collectors.toSet());
        return menuRouters.stream()
                .map(TreeNode::getId)
                .filter(id -> !parentsId.contains(id))
                .collect(Collectors.toList());
    }
}
