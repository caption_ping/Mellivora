package com.captain.crew.common.log.event;

import com.captain.crew.common.log.core.CrewLog;
import com.captain.crew.common.log.service.CrewLogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;

/**
 * @author CAP_Crew
 * @Description: 异步监听日志事件
 * @date 2021-01-20
 **/
@Slf4j
@RequiredArgsConstructor
public class SysLogEventListener {

    private final CrewLogService logService;

    @Async
    @Order
    @EventListener(SysLogEvent.class)
    public void saveSysLog(SysLogEvent event) {
        CrewLog sysLog = (CrewLog) event.getSource();
        logService.saveLog(sysLog);
    }
}
