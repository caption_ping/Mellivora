package com.captain.crew.common.quartz.util;

import cn.hutool.core.util.StrUtil;
import com.captain.crew.admin.api.entity.domain.SysJob;
import com.captain.crew.admin.api.entity.domain.SysJobLog;
import com.captain.crew.admin.api.service.system.SysJobService;
import com.captain.crew.common.quartz.component.TaskInvokeFactory;
import com.captain.crew.common.quartz.component.TaskInvokeHandler;
import com.captain.crew.common.quartz.constant.CrewQuartzConstant;
import com.captain.crew.common.quartz.event.SysJobLogEvent;
import com.captain.crew.common.quartz.exception.TaskException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.CronTrigger;
import org.quartz.Trigger;
import org.springframework.context.ApplicationEventPublisher;

import java.time.ZoneId;

/**
 * @author CAP_Crew
 * @date 2021-10-20
 **/
@Slf4j
@RequiredArgsConstructor
public class TaskInvokeUtil {

    private final SysJobService sysJobService;
    private final ApplicationEventPublisher applicationEventPublisher;

    public void invokeMethod(SysJob sysJob, Trigger trigger) {
        long begin = System.currentTimeMillis();
        long end;

        // 用于更新任务表内的状态、执行时间、下次执行时间、上次执行时间等
        SysJob updateJob = new SysJob();
        updateJob.setJobId(sysJob.getJobId());
        SysJobLog sysJobLog = getJobLog(sysJob);

        TaskInvokeHandler invoker;
        try {
            invoker = TaskInvokeFactory.getInvoker(sysJob.getJobType().toString());
            invoker.execute(sysJob);
            //任务表信息更新
            updateJob.setJobExecuteStatus(CrewQuartzConstant.JOB_LOG_STATUS_SUCCESS.getType());
            this.setSuccessResultLog(sysJobLog);
        } catch (TaskException taskException) {
            // 任务表信息更新
            updateJob.setJobExecuteStatus(CrewQuartzConstant.JOB_LOG_STATUS_FAIL.getType());
            this.setFailResultLog(sysJobLog, taskException);
        } finally {
            end = System.currentTimeMillis();

            // 更新执行信息
            this.recordExecuteInfo(updateJob, trigger);
            sysJobService.updateById(updateJob);

            sysJobLog.setExecuteTime(String.valueOf(end - begin));
            applicationEventPublisher.publishEvent(new SysJobLogEvent(sysJobLog));
        }

    }

    private SysJobLog getJobLog(SysJob sysJob) {
        SysJobLog sysJobLog = new SysJobLog();
        sysJobLog.setJobId(sysJob.getJobId());
        sysJobLog.setJobName(sysJob.getJobName());
        sysJobLog.setJobGroup(sysJob.getJobGroup());
        sysJobLog.setJobOrder(sysJob.getJobOrder());
        sysJobLog.setJobType(sysJob.getJobType());
        sysJobLog.setExecutePath(sysJob.getExecutePath());
        sysJobLog.setClassName(sysJob.getClassName());
        sysJobLog.setMethodName(sysJob.getMethodName());
        sysJobLog.setMethodParamsValue(sysJob.getMethodParamsValue());
        sysJobLog.setCronExpression(sysJob.getCronExpression());
        sysJobLog.setTenantId(sysJob.getTenantId());
        return sysJobLog;
    }

    private void setSuccessResultLog(SysJobLog sysJobLog) {
        sysJobLog.setJobMessage(CrewQuartzConstant.JOB_LOG_STATUS_SUCCESS.getDesc());
        sysJobLog.setJobLogStatus(CrewQuartzConstant.JOB_LOG_STATUS_SUCCESS.getType());
    }

    private void setFailResultLog(SysJobLog sysJobLog, TaskException taskException) {
        sysJobLog.setJobMessage(CrewQuartzConstant.JOB_LOG_STATUS_FAIL.getDesc());
        sysJobLog.setJobLogStatus(CrewQuartzConstant.JOB_LOG_STATUS_FAIL.getType());
        sysJobLog.setExceptionInfo(StrUtil.sub(taskException.getMessage(), 0, 2000));
    }

    private void recordExecuteInfo(SysJob updateJob, Trigger trigger){
        //记录执行时间 立刻执行使用的是simpleTrigger
        if (trigger instanceof CronTrigger) {
            updateJob.setStartTime(trigger.getStartTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
            updateJob.setPreviousTime(trigger.getPreviousFireTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
            updateJob.setNextTime(trigger.getNextFireTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
        }
    }
}
