package com.captain.crew.common.security.service;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/11 14:41
 */
@Data
@NoArgsConstructor
public class SysUserAdmin implements UserDetails {

    private static final long serialVersionUID = 1343966276212872623L;

    /**
     * 用户ID
     */
    private Integer id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 是否启用
     */
    private Boolean enable;
    /**
     * 部门ID
     */
    private Integer deptId;
    /**
     * 租户ID
     */
    private Integer tenantId;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * token
     */
    private String token;
    /**
     * 登陆时间
     */
    private long loginTime;
    /**
     * 超时时间
     */
    private long expireTime;
    /**
     *
     */
    private Boolean accountNonExpired;
    /**
     *
     */
    private Boolean credentialsNonExpired;
    /**
     *
     */
    private Boolean accountNonLocked;
    /**
     * 权限集合
     */
    private List<String> permissions;


    /**
     * 返回 null 不用 Spring Security 默认的权限集合了
     * 改为自己维护的 permissions
     * 因为 GrantedAuthority 总是存在序列化问题
     *
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enable;
    }

    /**
     * Construct the <code>User</code> with the details required by
     *
     * @param id                    用户ID
     * @param deptId                部门ID
     * @param tenantId              租户ID
     * @param username              the username presented to the
     *                              <code>DaoAuthenticationProvider</code>
     * @param password              the password that should be presented to the
     *                              <code>DaoAuthenticationProvider</code>
     * @param enabled               set to <code>true</code> if the user is enabled
     * @param accountNonExpired     set to <code>true</code> if the account has not expired
     * @param credentialsNonExpired set to <code>true</code> if the credentials have not
     *                              expired
     * @param accountNonLocked      set to <code>true</code> if the account is not locked
     * @param authorities           the authorities that should be granted to the caller if they
     *                              presented the correct username and password and the user is enabled. Not null.
     * @throws IllegalArgumentException if a <code>null</code> value was passed either as
     *                                  a parameter or as an element in the <code>GrantedAuthority</code> collection
     */
    public SysUserAdmin(Integer id, Integer deptId, Integer tenantId, String username, String nickName, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, List<String> authorities) {
        this.id = id;
        this.deptId = deptId;
        this.tenantId = tenantId;
        this.nickName = nickName;
        this.username = username;
        this.password = password;
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.enable = enabled;
        this.credentialsNonExpired = credentialsNonExpired;
        this.permissions = authorities;
    }
}
