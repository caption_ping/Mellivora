package com.captain.crew.common.mybatis.tenant;

import cn.hutool.core.util.StrUtil;
import com.captain.crew.common.core.constant.CommonConstant;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-02-01
 **/
@Slf4j
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class TenantContextHolderFilter extends GenericFilterBean {

    @Override
    @SneakyThrows
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) {
        HttpServletRequest servletRequest = (HttpServletRequest) request;

        // 从请求头中获取租户ID
        String tenantId = servletRequest.getHeader(CommonConstant.TENANT_ID);

        if (StrUtil.isNotBlank(tenantId)) {
            TenantContextHolder.setTenantId(Integer.parseInt(tenantId));
        }

        filterChain.doFilter(request, response);
        TenantContextHolder.clear();
    }
}
