package com.captain.crew.common.security.config;

import com.captain.crew.common.security.component.PermitUrlProperties;
import com.captain.crew.common.security.handle.CrewAuthenticationTokenFilter;
import com.captain.crew.common.security.handle.CrewLogoutSuccessHandler;
import com.captain.crew.common.security.handle.CrewUnauthorizedHandler;
import com.captain.crew.common.security.service.CrewUserDetailServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * @author: crewer
 * @Description:
 * @Date: 2020/9/11 14:50
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@RequiredArgsConstructor
public class UnroadWebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final CrewUserDetailServiceImpl unroadUserDetailServiceImpl;
    private final CrewUnauthorizedHandler crewUnauthorizedHandler;
    private final CrewLogoutSuccessHandler logoutSuccessHandler;
    private final CrewAuthenticationTokenFilter authenticationTokenFilter;
    private final PermitUrlProperties urlProperties;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                // 关闭 csrf 开启 iframe 嵌套网页
                .csrf().disable()
                // 认证失败处理类
                .exceptionHandling().authenticationEntryPoint(crewUnauthorizedHandler).and()
                // 基于token，所以不需要session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class)
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessHandler(logoutSuccessHandler);

        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry configurer = http.authorizeRequests();
        configurer.antMatchers("/login/**", "/v2/api-docs", "/swagger-resources/**").permitAll();
        configurer.antMatchers(
                HttpMethod.GET,
                "/**/*.html",
                "/**/*.css",
                "/**/*.js").permitAll();
        // 将标注有 PermitUrl 注解的接口设置允许外部访问
        urlProperties.getIgnoreUrls()
                .forEach(url -> configurer.antMatchers(url).permitAll());
        configurer.anyRequest().authenticated();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(unroadUserDetailServiceImpl);
    }

    /**
     * 解决 无法直接注入 AuthenticationManager
     *
     * @return
     * @throws Exception
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
