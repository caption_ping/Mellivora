package com.captain.crew.common.core.dto;

import com.captain.crew.common.core.enums.ResponseCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

/**
 * 通用的 Http 响应实体
 *
 * @Author: crewer
 * @Description:
 * @Date: 2020/8/25 15:49
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "响应信息主体")
public class R<T> implements Serializable {

    private static final long serialVersionUID = 6741514732696168017L;

    /**
     * 响应码
     */
    @ApiModelProperty(value = "返回标记: 成功 = 20000; 其他都算失败")
    private Integer code;

    /**
     * 响应信息
     */
    @ApiModelProperty(value = "返回信息说明，成功可为空，失败有失败信息")
    private String msg;

    /**
     * 响应数据报
     */
    @ApiModelProperty(value = "响应数据")
    private T data;

    /**
     * 数据条数
     */
    @ApiModelProperty(value = "数据条数")
    private Integer count;

    /**
     * 成功的响应体，默认 code 为 0
     *
     * @param msg   成功的响应信息
     * @param data  成功的响应数据
     * @param count 返回的数据条数
     * @param <T>   返回的响应数据类型，建议在返回值列表中填写完整
     * @return 统一的成功响应信息
     */
    public static <T> R<T> ok(String msg, T data, Integer count) {
        R<T> r = new R<>();
        r.setCode(ResponseCode.OK.getCode());
        r.setData(data);
        r.setMsg(msg);
        r.setCount(count);
        return r;
    }

    /**
     * 成功的响应体，默认 code 为 0
     *
     * @param msg  成功的响应信息
     * @param data 成功的响应数据
     * @param <T>  返回的响应数据类型，建议在返回值列表中填写完整
     * @return 统一的成功响应信息
     */
    public static <T> R<T> ok(String msg, T data) {
        return ok(msg, data, null);
    }

    /**
     * 成功的响应体，默认 code 为 0
     *
     * @param data  成功的响应数据
     * @param count 返回的数据条数
     * @param <T>   返回的响应数据类型，建议在返回值列表中填写完整
     * @return 统一的成功响应信息
     */
    public static <T> R<T> ok(Integer count, T data) {
        return ok(ResponseCode.OK.getMsg(), data, count);
    }

    public static <T> R<T> ok(T data) {
        return ok(ResponseCode.OK.getMsg(), data);
    }

    public static <T> R<T> ok() {
        return ok(null);
    }

    /**
     * 失败的统一结果相应体 code 为 1
     *
     * @param msg  失败的响应信息
     * @param data 失败的数据响应信息
     * @param <T>  失败的响应数据类型，建议在返回值列表中填写完整，没有数据返回值填写 Void
     * @return 失败的同意结果相应体
     */
    public static <T> R<T> failed(String msg, T data) {
        R<T> r = new R<>();
        r.setCode(ResponseCode.FAILED.getCode());
        r.setData(data);
        r.setMsg(msg);
        r.setCount(null);
        return r;
    }

    public static <T> R<T> failed(String msg, Integer code, T data) {
        R<T> r = new R<>();
        r.setCode(code);
        r.setMsg(msg);
        r.setData(data);
        return r;
    }

    public static <T> R<T> failed(String msg) {
        return failed(msg, null);
    }

    public static <T> R<T> failed() {
        return failed(ResponseCode.FAILED.getMsg());
    }

    @Override
    public String toString() {
        return "ResponseResult{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
