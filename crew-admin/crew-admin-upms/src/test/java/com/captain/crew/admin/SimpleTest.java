package com.captain.crew.admin;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-05-24
 **/
public class SimpleTest {

    private final List<String> tables = Lists.newArrayList("heavy_data", "a", "c");

    @Test
    public void test() {

        String sql = " SELECT a.A FROM (" +
                "SELECT b.B FROM " +
                "(SELECT c.C FROM cc c) as b Left JOIN " +
                "(SELECT * FROM F left join (select * from heavy_data) as x on x.id = F.id)d ON b.C = d.d" +
                ") as a";

        if (tables.stream().anyMatch(sql::contains)) {
            System.out.println("包含");
        }
    }

}
