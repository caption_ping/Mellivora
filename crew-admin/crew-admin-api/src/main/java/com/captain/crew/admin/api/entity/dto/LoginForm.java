package com.captain.crew.admin.api.entity.dto;

import lombok.Data;

/**
 * @author CAP_Crew
 * @Description: TODO
 * @date 2021-04-26
 **/
@Data
public class LoginForm {

    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 验证码 key
     */
    private String key;
    /**
     * 验证码
     */
    private String captcha;
}
