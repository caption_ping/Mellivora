package com.captain.crew.admin.api.entity.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author CAP_Crew
 * @date 2021-10-19
 **/
@Data
@TableName(value = "sys_job")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SysJob implements Serializable {
    private static final long serialVersionUID = -1916924459301487960L;
    /**
     * 任务id
     */
    @TableId(value = "job_id", type = IdType.AUTO)
    private Integer jobId;

    /**
     * 任务名称
     */
    @TableField(value = "job_name")
    private String jobName;

    /**
     * 任务组名
     */
    @TableField(value = "job_group")
    private String jobGroup;

    /**
     * 组内执行顺利，值越大执行优先级越高，最大值9，最小值1
     */
    @TableField(value = "job_order")
    private Integer jobOrder;

    /**
     * 1、java类;2、spring bean名称;3、rest调用;9其他
     */
    @TableField(value = "job_type")
    private Integer jobType;

    /**
     * job_type=3时，rest调用地址，仅支持rest get协议,需要增加String返回值，0成功，1失败
     */
    @TableField(value = "execute_path")
    private String executePath;

    /**
     * job_type=1时，类完整路径;job_type=2时，spring bean名称;其它值为空
     */
    @TableField(value = "class_name")
    private String className;

    /**
     * 任务方法
     */
    @TableField(value = "method_name")
    private String methodName;

    /**
     * 参数值
     */
    @TableField(value = "method_params_value")
    private String methodParamsValue;

    /**
     * cron执行表达式
     */
    @TableField(value = "cron_expression")
    private String cronExpression;

    /**
     * 错失执行策略（1错失周期立即执行 2错失周期执行一次 3下周期执行）
     */
    @TableField(value = "misfire_policy")
    private Integer misfirePolicy;

    /**
     * 1、多租户任务;2、非多租户任务
     */
    @TableField(value = "job_tenant_type")
    private Integer jobTenantType;

    /**
     * 状态（1、未发布;2、运行中;3、暂停;4、删除;）
     */
    @TableField(value = "job_status")
    private Integer jobStatus;

    /**
     * 状态（0正常 1异常）
     */
    @TableField(value = "job_execute_status")
    private Integer jobExecuteStatus;

    /**
     * 创建者
     */
    @TableField(value = "create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private LocalDateTime createTime;

    /**
     * 更新者
     */
    @TableField(value = "update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private LocalDateTime updateTime;

    /**
     * 初次执行时间
     */
    @TableField(value = "start_time")
    private LocalDateTime startTime;

    /**
     * 上次执行时间
     */
    @TableField(value = "previous_time")
    private LocalDateTime previousTime;

    /**
     * 下次执行时间
     */
    @TableField(value = "next_time")
    private LocalDateTime nextTime;

    /**
     * 租户
     */
    @TableField(value = "tenant_id")
    private Integer tenantId;

    /**
     * 备注信息
     */
    @TableField(value = "remark")
    private String remark;
}
